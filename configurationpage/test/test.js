var express = require('express');
var router = express.Router();
var expect = require('chai').expect;
var add = require('./routes/add.js');
var edit = require('./routes/edit.js');
var healthcheck = require('./routes/healthcheck.js');
var home = require('./routes/home.js');
var showDetailConnector = require('./routes/showDetailConnector.js');

var querystring = require('querystring');
var http = require('http');

//add post


describe('test add post', function () {
    it('success should be return scheduled', function () {

        var data = querystring.stringify({
            username: "admin",
            password: "admin",
            pname: "test",
            interval: 3 * 60000,
            port: "5657",
            server: "http://ec2-54-224-144-100.compute-1.amazonaws.com:8085"
        });

        var options = {
            host: 'localhost',
            port: 3000,
            path: '/add',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': Buffer.byteLength(data)
            }
        };


        var callback = http.request(options, function (response) {
            var str = '';

            response.setEncoding('utf8');
            response.on('data', function (chunk) {
                str += chunk;
            });


            response.on('end', function () {
                expect(str.to.be.equal("scheduled"));
            });

        });
        callback.write(data);
        callback.end();


    });
});


//add get
describe('test add get', function () {
    it('success should be return a html page', function () {

        var options = {
            host: 'localhost',
            port: 3000,
            path: '/add',
            method: 'GET',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'

            }
        };


        var callback = http.request(options, function (response) {
            var str = '';

            response.setEncoding('utf8');
            response.on('data', function (chunk) {
                str += chunk;
            });


            response.on('end', function () {
                expect(str.to.be.not.equal("scheduled"));
            });

        });
        callback.end();


    });
});


//edit post
describe('test edit post', function () {
    it('success should be return scheduled', function () {
        var data = querystring.stringify({
            operation: "Reschedule",
            username: "admin",
            password: "admin",
            pname: "test",
            interval: 5 * 60000,
            port: "5657",
            server: "http://ec2-54-224-144-100.compute-1.amazonaws.com:8085"
        });
        var options = {
            host: 'localhost',
            port: 3000,
            path: '/edit',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': Buffer.byteLength(data)
            }
        };


        var callback = http.request(options, function (response) {
            var str = '';

            response.setEncoding('utf8');
            response.on('data', function (chunk) {
                str += chunk;
            });


            response.on('end', function () {
                expect(str.to.be.equal("scheduled"));
            });

        });
        callback.write(data);
        callback.end();
    });
});

//edit get
describe('test edit get', function () {
    it('success should be return a html page', function () {
        var options = {
            host: 'localhost',
            port: 3000,
            path: '/edit',
            method: 'GET',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'

            }
        };


        var callback = http.request(options, function (response) {
            var str = '';

            response.setEncoding('utf8');
            response.on('data', function (chunk) {
                str += chunk;
            });


            response.on('end', function () {

                expect(str.to.be.not.equal("scheduled"));
            });
        });
        callback.end();

    });
});

//healthcheck get
describe('test healthcheck get', function () {
    it('success should be return success', function () {
        var options = {
            host: 'localhost',
            port: 3000,
            path: '/healthcheck?status=die&port=5656&name=jira',
            method: 'GET',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'

            }
        };


        var callback = http.request(options, function (response) {
            var str = '';

            response.setEncoding('utf8');
            response.on('data', function (chunk) {
                str += chunk;
            });


            response.on('end', function () {
                expect(str.to.be.equal("success"));
            });

        });
        callback.end();

    });
});

//home get
describe('test home  get', function () {
    it('success should be return a html page', function () {
        var options = {
            host: 'localhost',
            port: 3000,
            path: '/home',
            method: 'GET',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'

            }
        };


        var callback = http.request(options, function (response) {
            var str = '';

            response.setEncoding('utf8');
            response.on('data', function (chunk) {
                str += chunk;
            });


            response.on('end', function () {
                expect(str.to.be.not.equal("success"));
            });

        });
        callback.end();
    });
});

//showDetailConnector post
describe('test showDetailConnector post', function () {
    it('success should be return cancelled succssfully', function () {
        var data = querystring.stringify({
            operation: "Cancel",
            username: "admin",
            password: "admin",
            pname: "test",
            interval: 5 * 60000,
            port: "5657",
            server: "http://ec2-54-224-144-100.compute-1.amazonaws.com:8085"
        });
        var options = {
            host: 'localhost',
            port: 3000,
            path: '/showDetailConnector',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': Buffer.byteLength(data)
            }
        };


        var callback = http.request(options, function (response) {
            var str = '';

            response.setEncoding('utf8');
            response.on('data', function (chunk) {
                str += chunk;
            });


            response.on('end', function () {

                expect(str.to.be.equal("cancelled succssfully"));
            });

        });
        callback.write(data);
        callback.end();
    });
});

//showDetailConnector get
describe('test showDetailConnector get', function () {
    it('success should be return a html page', function () {
        var options = {
            host: 'localhost',
            port: 3000,
            path: '/showDetailConnector',
            method: 'GET',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'

            }
        };


        var callback = http.request(options, function (response) {
            var str = '';

            response.setEncoding('utf8');
            response.on('data', function (chunk) {
                str += chunk;
            });


            response.on('end', function () {
                expect(str.to.be.not.equal("cancelled succssfully"));
            });

        });
        callback.end();

    });
});


module.exports = router;
