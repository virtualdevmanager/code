
var http = require('http');
var bunyan = require('bunyan');
var log = bunyan.createLogger({name: 'configurationpage'});
function makeRequest(options,res) {
    return http.request(options, function (response) {
        var str = '';
        response.setEncoding('utf8');
        response.on('data', function (chunk) {
            log.info("body: " + chunk);
            str += chunk;
        });
        response.on('end', function () {
            if (str === 'scheduled') {
                res.redirect('/success');
            } else {
                res.render('error', {
                    message: str
                });
            }

        });
    });
}

module.exports = makeRequest;


