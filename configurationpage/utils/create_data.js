var _ = require('underscore');

/*
 * This function creates a dictionary with request body pass in
 */
function createData(requestBody) {
	_.each(requestBody, function(property) {
		if (property === '' || property === undefined) {
			return {};
		}
	});

	var data = {
		username: requestBody.username,
    	password: requestBody.password,
    	pname: encodeURI(requestBody.pname),
    	host: requestBody.server,
		timeInterval: requestBody.interval * 60000,
     	port: requestBody.port
	};
	
	return data;
}

module.exports = createData