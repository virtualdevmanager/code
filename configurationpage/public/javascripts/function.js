/*
VDM functions
 */

function checkForm(){
  
    document.getElementById('alert_required').style.display="none";
    document.getElementById('alert_interval').style.display="none";

    if(document.getElementById('username').value === ""||document.getElementById('pname').value === ""||document.getElementById('style').value === ""||document.getElementById('pw').value === ""||document.getElementById('server').value === ""){ //
        document.getElementById('alert_required').style.display="block";
        return false;
    }else if(isNaN(document.getElementById('interval').value)) {
        document.getElementById('alert_interval').style.display="block";
        return false;
    }else{

        var e = document.getElementById("style");
        var portNumber = e.options[e.selectedIndex].value;
        document.getElementById('port').value =portNumber;
        document.forms[0].submit();
        return true;
    }


}



function checkFormReschedule(){
    document.getElementById('alert_required_reschedule').style.display="none";
    document.getElementById('alert_interval_reschedule').style.display="none";
    if(document.getElementById('username').value === "" ||document.getElementById('style').value === ""||document.getElementById('password').value === ""||document.getElementById('server').value === ""){
        document.getElementById('alert_required_reschedule').style.display="block";

        return false;
    }else if(isNaN(document.getElementById('interval').value)) {
        document.getElementById('alert_interval_reschedule').style.display="block";
        return false;
    }else{
        document.getElementById('operation').value ="Reschedule";
        // alert("succeed!")
        var e = document.getElementById("style");
        var portNumber = e.options[e.selectedIndex].value;
        document.getElementById('port').value =portNumber;
        document.forms[0].submit();
        return true;
    }
}


function refresh(){
    setInterval(change,500);
    function change(){

        $("#content").load(location.href + ' #content>*');


    }
    if (location.href.indexOf("?refresh=")<0)
    {   window.location.reload();
        location.href=location.href+"?refresh="+Math.random();
    }




    return true;
}



function deleteConnection(){
    document.getElementById('operation').value ="Cancel";
    document.forms[0].submit();
    return true;

}
function editConnection(i){

    document.getElementById('operation').value ="Edit";
    document.getElementById('connector').value =i;

    document.forms[0].submit();
    return true;

}
