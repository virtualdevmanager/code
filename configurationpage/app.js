var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var HashMap = require('hashmap');
var os = require('os');
var bunyan = require('bunyan');

//The default log path in vagrant box is: /var/log/
var log = bunyan.createLogger({name: 'configurationpage'});
var interfaces = os.networkInterfaces();
var addresses = [];
global.projlink;

//This section records the VDM project IP address
for (var k in interfaces) {
  for (var k2 in interfaces[k]) {
    var address = interfaces[k][k2];
    if (address.family === 'IPv4' && !address.internal) {
      global.projlink=address.address+":8000";
      addresses.push(address.address);
      log.info("The address is: " + address.address);
    }
  }
}

var add = require('./routes/add');
var home = require('./routes/home');
var showDetailConnector = require('./routes/showDetailConnector');
var success = require('./routes/success');
var edit = require('./routes/edit');


var healthcheck = require('./routes/healthcheck');
var app = express();

global.statusMap=new HashMap();

global.connMap=new HashMap();

global.reverseconnMap=new HashMap();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/add', add);
app.use('/home', home);
app.use('/showDetailConnector', showDetailConnector);
app.use('/success', success);
app.use('/edit', edit);
app.use('/healthcheck', healthcheck);



app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: {}
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});





module.exports = app;
