var express = require('express');
var router = express.Router();
var http = require('http');
var HashMap = require('hashmap');

//Currently the nano module is used to read data from couch DB, the couch DB is hard coded at port 5984
var nano = require('nano')('http://localhost:5984');
var schedulardb = nano.db.use('schedular');
var bunyan = require('bunyan');

//The default log path in vagrant box is: /var/log/
var log = bunyan.createLogger({name: 'configurationpage'});

router.get('/', function (req, res, next) {

    var status = req.query.status;
    var port = req.query.port;
    var name = req.query.connector;
    log.info(port, name, status);

    if (port !== null && port !== undefined && port !== '') {

        //We update global variable if the current service is alive
        global.statusMap.set(port, status);
        global.connMap.set(port, name);
        global.reverseconnMap.set(name, port);
    }


    log.info("global.statusMap.count()", global.statusMap.count());


    res.send("success!")


});


module.exports = router;
