var express = require('express');
var router = express.Router();
var querystring = require('querystring');
var http = require('http');
var bunyan = require('bunyan');

//The default log path in vagrant box is: /var/log/
var log = bunyan.createLogger({name: 'configurationpage'});
var createData = require('../utils/create_data');
var makeRequest = require('../utils/make_request');
var _ = require('underscore');

/*
 * This file handles all the web calls which can edit adapter on the configuration page
 */
router.get('/', function (req, res, next) {
    var i = req.query.i;
    log.info("i:", i);
    res.render('edit', {i: edit_connector});
});
router.post('/', function (req, res, next) {
    var operation = req.body.operation;
    log.info("operation:" + req.body.operation)
    if (operation === "Cancel") {

        res.render('home', {title: 'home'});

    }
    //If user selected reschedule button on edit page, we reschedule adapter in DB
    else if (operation === "Reschedule") {
        var data = createData(req.body);
        log.info("username:" + data.username);
        log.info("pname:" + data.pname);
        log.info("server:" + data.server);
        log.info("port:" + data.port);
        log.info("time:" + req.body.time);

        if (_.isEmpty(data)) {
            res.send("Username,password, server and connector name is required!");
            log.warn( "edit.js", "Unknown", "Unknown", "Username,password, server and connector name is required!");
            return;
        } else {
            var request_data = querystring.stringify(data);

            //Reschedule the adapter in DB to do data update
            var options = {
                host: 'localhost',
                port: 5689,
                path: '/reschedule',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Content-Length': Buffer.byteLength(request_data)
                }
            };
            var callback = makeRequest(options, res);
            callback.write(request_data);
            callback.end();
        }
    }
});
module.exports = router;
