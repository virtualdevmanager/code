var express = require('express');
var router = express.Router();
var querystring = require('querystring');
var http = require('http');
var bunyan = require('bunyan');

//The default log path in vagrant box is: /var/log/
var log = bunyan.createLogger({name: 'configurationpage'});
var createData = require('../utils/create_data');
var makeRequest = require('../utils/make_request');
var _ = require('underscore');

/*
 * This file handles all the web calls which can add new adapter on the configuration page
 */
router.get('/', function (req, res, next) {
    log.info("alive connetor:", global.statusMap.count());
    res.render('add', {title: 'add'});
});

router.post('/', function (req, res, next) {
    log.info("Enter!!!!!" );

    //Create the request json
    var data = createData(req.body);
    log.info("username:" + data.username);
    log.info("password:" + data.password);
    log.info("pname:" + data.pname);
    log.info("server:" + data.host);
    log.info("port:" + data.port);
    log.info("time:" + req.body.time);

    if (_.isEmpty(data)) {
         res.send("Username,password, server and connector name is required!");
            return;
    } else {
        var request_data = querystring.stringify(data);

        //Making init web call to adapter script with data content
        var options = {
            host: 'localhost',
            port: 5689,
            path: '/init',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': Buffer.byteLength(request_data)
            }
        };
        var callback = makeRequest(options,res);
        callback.write(request_data);
        callback.end();
    }
});
module.exports = router;
