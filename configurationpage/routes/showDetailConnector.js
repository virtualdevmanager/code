var express = require('express');
var router = express.Router();
var bunyan = require('bunyan');

//The default log path in vagrant box is: /var/log/
var log = bunyan.createLogger({name: 'configurationpage'});

/* GET users listing. */
router.get('/', function (req, res, next) {
    var i = req.query.i;
    log.info("i:", i);

    res.render('showDetailConnector', {i: i});
});


router.post('/', function (req, res, next) {
    var operation = req.body.operation;
    log.info("operation:" + req.body.operation);

    //If user selected removing adapter, the program will initialize a delete call to adapter script
    if (operation === "Cancel") {


        var querystring = require('querystring');
        var http = require('http');

        log.info("pname:" + encodeURI(req.body.pname));

        //Building web call with adapter detail information
        var data = querystring.stringify({
            username: req.body.username,
            password: req.body.password,
            pname: encodeURI(req.body.pname),
            timeInterval: req.body.interval,
            port: req.body.port,
            host: req.body.server
        });




        var options = {
            host: 'localhost',
            port: 5689,
            path: '/cancel',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': Buffer.byteLength(data)
            }
        };

        log.info(data);

        var callback = http.request(options, function (response) {
            var str = '';

            response.setEncoding('utf8');
            response.on('data', function (chunk) {
                log.info("body: " + chunk);
                str += chunk
            });


            response.on('end', function () {

                if (str === 'cancelled successfully') {
                    res.redirect('/success');
                } else {
                    res.render('error', {
                        message: "cancel fail"
                    });
                    
                }
            });

        });
        callback.write(data);
        callback.end();
    } else if (operation === "Edit") {
        log.info("edit!!!")
        var connector = req.body.connector;
        global.edit_connector = connector;
        //If user choose to edit the adapter, redirect page to edit
        res.redirect('/edit');
    }


});
module.exports = router;
