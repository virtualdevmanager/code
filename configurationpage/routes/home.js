var express = require('express');
var router = express.Router();
var http = require('http');
var HashMap = require('hashmap');

//Currently the nano module is used to read data from couch DB, the couch DB is hard coded at port 5984
var nano = require('nano')('http://localhost:5984');
var schedulardb = nano.db.use('schedular');
var bunyan = require('bunyan');

//The default log path in vagrant box is: /var/log/
var log = bunyan.createLogger({name: 'configurationpage'});
router.get('/', function (req, res, next) {

    schedulardb.view("schedular", "by_pname", function (err, body) {
        if (!err) {
            global.connectors = [];
            var i = 0;

            //Reading all connector detail from database
            body.rows.forEach(function (doc) {
                var username = doc.value.username;
                var password = doc.value.password;
                var pname = decodeURI(doc.value.pname);
                var server = doc.value.host;
                var timeInterval = doc.value.timeInterval / 60000;
                var port = doc.value.port;
                log.info("home.js", doc.value.port);
                port = global.connMap.get(port);
                log.info("home.js.portname", port);
                var hm = new HashMap();
                hm.set("pname", pname);
                hm.set("port", port);
                hm.set("host", server);
                hm.set("username", username);
                hm.set("password", password);
                hm.set("timeInterval", timeInterval);
                log.info("timeInterval", timeInterval)


                hm.set("status", global.statusMap.get(port));//global.statusMap.get(port)
                global.connectors[i] = hm;
                i++;


            });

            //Update the total number of adapter
            global.connectors_count = i;
            log.info("global.connectors_count", i);

        } else {
            log.info(err)
        }
    });


    res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
    res.setHeader("Pragma", "no-cache"); // HTTP 1.0.
    res.setHeader("Expires", "0"); // Proxies.

    res.render('home', {title: 'home'});
});


router.post('/', function (req, res, next) {
    var status = req.body.status;
    var port = req.body.port;
    if (status === 'alive') {
        global.statusMap.set(port, "alive");
    } else if (status === 'dead') {
        global.statusMap.set(port, "dead");
    }
    log.info(port)
    log.info(global.statusMap.get(port))

    global.statusMap.forEach(function (value, key) {
        log.info(key + " : " + value);

    });
    res.send("success!")
});
module.exports = router;
