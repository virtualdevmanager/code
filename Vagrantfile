# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  # All Vagrant configuration is done here. The most common configuration
  # options are documented and commented below. For a complete reference,
  # please see the online documentation at vagrantup.com.

  # Every Vagrant virtual environment requires a box to build off of.
  config.vm.box = "ubuntu/trusty64"

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.

  # Add port-forward for Node-inspector
  #config.vm.network "forwarded_port", guest: 5858, host: 5858
  # Add port-forward Sails.js application
  config.vm.network "forwarded_port", guest: 8000, host: 8000
  config.vm.network "forwarded_port", guest: 3000, host: 3000

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  config.vm.network "private_network", ip: "192.168.33.10"


  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # VirtualBox:
  config.vm.provider "virtualbox" do |vb|
     # Boot with headless mode
     vb.gui = false

     # Use VBoxManage to customize the VM. For example to change memory:
     vb.customize ["modifyvm", :id, "--memory", "8096"]
  end

  # Enable provisioning with Puppet stand alone.  Puppet manifests
  # are contained in a directory path relative to this Vagrantfile.

  #config.vm.provision :chef_solo do |chef|
  #  chef.cookbooks_path = ['chef/cookbooks']
  #  chef.add_recipe 'recipe[vdm::default]'
  #end

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  config.vm.provision "shell", inline: <<-SHELL
    cd /vagrant

    #set timezone
    sudo timedatectl set-timezone "America/New_York"

    #CouchDB
    sudo add-apt-repository ppa:couchdb/stable -y
    sudo apt-get update
    sudo apt-get install software-properties-common -y
    sudo apt-get remove couchdb couchdb-bin couchdb-common -yf
    sudo apt-get install couchdb -y
    sudo rm -rf /etc/couchdb/local.ini
    sudo cp scripts/local.ini /etc/couchdb/
    sudo cp scripts/test-local.ini /tmp/
    sudo service couchdb stop
    sudo service couchdb start
    sudo cp /vagrant/scripts/test-couch /etc/init.d/test-couch
    sudo chmod 755 /etc/init.d/test-couch
    sudo update-rc.d test-couch defaults
    sudo service test-couch restart
    sudo apt-get install npm -y

    #VDM Main
    sudo apt-get install python-pip -y
    sudo apt-get install python-numpy -y
    sudo pip install -r vdm/requirements.txt #verisions
    sudo cp /vagrant/scripts/vdm_service /etc/init.d/vdm # vdm_service: define commands of vdm service
    sudo chmod 755 /etc/init.d/vdm 
    sudo update-rc.d vdm defaults #add vdm/ to boot
    sudo service vdm restart

    #Configuration Page
    cd /vagrant/configurationpage
    sudo apt-get install nodejs-legacy -y
    sudo apt-get install nodejs npm -y
    sudo npm install
    sudo cp /vagrant/scripts/configurationpage /etc/init.d/configurationpage
    sudo chmod 755 /etc/init.d/configurationpage #CouchDB configuration setting
    sudo update-rc.d configurationpage defaults
    sudo service configurationpage restart

    mkdir /vagrant/temp/ #folder for pid files

    #Connectors
    cd /vagrant/Adapters
    sudo pip install -r requirements.txt #verisions

    sudo cp setup/jira /etc/init.d/
    sudo chmod +x /etc/init.d/jira

    sudo cp setup/bamboo /etc/init.d/ 
    sudo chmod +x /etc/init.d/bamboo

    sudo cp setup/bitbucket /etc/init.d/
    sudo chmod +x /etc/init.d/bitbucket

    sudo cp setup/fogbugz /etc/init.d/
    sudo chmod +x /etc/init.d/fogbugz

    sudo apt-get install build-essential python-dev -y
    sudo apt-get install uwsgi -y
    sudo apt-get install uwsgi-plugin-python -y

    #Monitor/Scheduler
    cd /vagrant/Monitor
    sudo npm install
    cd setup/
    sudo sh setup.sh
    sudo cp VDMmonitor /etc/init.d/.
    sudo chmod +x /etc/init.d/VDMmonitor


    sudo update-rc.d VDMmonitor defaults
    sudo service VDMmonitor start

    #VDM Main
    sudo cp /vagrant/scripts/processingservice /etc/init.d/processingservice
    sudo chmod 755 /etc/init.d/processingservice
    sudo update-rc.d processingservice defaults
    sudo service processingservice restart
  SHELL


  config.vm.provision "shell", run: "always", inline: <<-SHELL
    sudo service vdm restart
    sudo service configurationpage restart
    sudo service VDMmonitor restart
    sudo service processingservice restart
    sudo service test-couch restart
  SHELL

end
