import sched, time
import couchdb
import json
from dateutil.parser import parse
from datetime import timedelta
import numpy as np
import datetime
import json
import sys, os

import logging
from .charts import *
from .models import *

formatter = logging.Formatter(fmt='%(asctime)s-%(levelname)s-%(module)s-%(message)s')
handler = logging.FileHandler('/vagrant/dataprocessing/commits/processing.log')
handler.setFormatter(formatter)
logger = logging.getLogger('root')
logger.setLevel(logging.INFO)
logger.addHandler(handler)
logger.info("level:" + str(logger.getEffectiveLevel()))


def date_handler(obj):
    """Formats an object into date into YYYY-MM-DD format"""
    logger.info('formatting date into isoformat')
    return obj.isoformat() if hasattr(obj, 'isoformat') else obj


def handle_update():
    """Refreshes the data required to produce the dashboard charts from the database
    rype: int
    Return Type: 0 if this is the first time we are updating, 1 if not
    """
    update_first_time = 0  # whether or not it is the first time we are loading the data
    commits = Commit()  # commit DAO
    commits.process_diff()  # process diff in commits
    save = Save()  # project data DAO
    docs = save.get()  # getting all the project data needed for charts
    projects = Project().get()  # getting all the configured project information

    logger.info('iterating through the sets of project information')
    for project in projects:
        # if the JIRA ID was provided in the project configurations then pull the project data related to that JIRA ID
        # from the database
        if "bitbucket_id" in project:
            logger.info("before updating project %s" % project["bitbucket_id"])
            data = commits.get_commits(project["bitbucket_id"])
            repo_activity = create_repo_activity(data)
            activity_per_user = create_activity_per_user(data)
            modification_activity = create_modification_activity(data)
            commit_case_activity = create_commit_case_activity(data, project["bitbucket_id"])
            modification_activity_per = create_modification_activity_per_member(data)

            # if the project data for charts includes documents then iterate through them and update them based on the
            # values we just pulled from the database above
            if len(docs) != 0:
                logger.info("iterating through existing documents for project %s" % project["bitbucket_id"])
                for doc in docs:
                    logger.debug('update.handle_update - looking at doc %s' % doc["_id"])
                    # update the data for the issue breakdown by priority chart
                    if str(doc["_id"]) == str(project["bitbucket_id"]) + "_repo_activity":
                        new_doc = json.loads(json.dumps(repo_activity, default=date_handler))
                        new_doc["_id"] = doc["_id"]
                        new_doc["_rev"] = doc["_rev"]
                        save.insert(new_doc)
                    elif str(doc["_id"]) == str(project["bitbucket_id"]) + "_activity_per":
                        new_doc = json.loads(json.dumps(activity_per_user, default=date_handler))
                        new_doc["_id"] = doc["_id"]
                        new_doc["_rev"] = doc["_rev"]
                        save.insert(new_doc)
                    elif str(doc["_id"]) == str(project["bitbucket_id"]) + "_modification_activity":
                        new_doc = json.loads(json.dumps(modification_activity, default=date_handler))
                        new_doc["_id"] = doc["_id"]
                        new_doc["_rev"] = doc["_rev"]
                        save.insert(new_doc)
                    elif str(doc["_id"]) == str(project["bitbucket_id"]) + "_commit_case_activity":
                        new_doc = json.loads(json.dumps(commit_case_activity, default=date_handler))
                        new_doc["_id"] = doc["_id"]
                        new_doc["_rev"] = doc["_rev"]
                        save.insert(new_doc)
                    elif str(doc["_id"]) == str(project["bitbucket_id"]) + "_modification_activity_per":
                        new_doc = json.loads(json.dumps(modification_activity_per, default=date_handler))
                        new_doc["_id"] = doc["_id"]
                        new_doc["_rev"] = doc["_rev"]
                        save.insert(new_doc)
                logger.info("done iterating through existing documents for project %s" % project["bitbucket_id"])

            # if the documents for this project do not already exist, then we will create a new document
            # in the database
            if not any(x["_id"] == (str(project["bitbucket_id"]) + "_repo_activity") for x in docs):
                doc = json.loads(json.dumps(repo_activity, default=date_handler))
                doc["_id"] = str(project["bitbucket_id"]) + "_repo_activity"
                save.insert(doc)
                docs.append(doc)
                update_first_time = 1
            if not any(x["_id"] == (str(project["bitbucket_id"]) + "_activity_per") for x in docs):
                doc = json.loads(json.dumps(activity_per_user, default=date_handler))
                doc["_id"] = str(project["bitbucket_id"]) + "_activity_per"
                save.insert(doc)
                docs.append(doc)
                update_first_time = 1
            if not any(x["_id"] == (str(project["bitbucket_id"]) + "_modification_activity") for x in docs):
                doc = json.loads(json.dumps(modification_activity, default=date_handler))
                doc["_id"] = str(project["bitbucket_id"]) + "_modification_activity"
                save.insert(doc)
                docs.append(doc)
                update_first_time = 1
            if not any(x["_id"] == (str(project["bitbucket_id"]) + "_commit_case_activity") for x in docs):
                doc = json.loads(json.dumps(commit_case_activity, default=date_handler))
                doc["_id"] = str(project["bitbucket_id"]) + "_commit_case_activity"
                save.insert(doc)
                docs.append(doc)
                update_first_time = 1
            if not any(x["_id"] == (str(project["bitbucket_id"]) + "_modification_activity_per") for x in docs):
                doc = json.loads(json.dumps(modification_activity_per, default=date_handler))
                doc["_id"] = str(project["bitbucket_id"]) + "_modification_activity_per"
                save.insert(doc)
                docs.append(doc)
                update_first_time = 1

    logger.info("returning %s to start.py: " % update_first_time)
    return update_first_time
