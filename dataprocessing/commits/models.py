import sched, time
import couchdb
import json
from dateutil.parser import parse
from datetime import timedelta
import numpy as np
import datetime
import json
import sys, os
import settings
import logging

formatter = logging.Formatter(fmt='%(asctime)s-%(levelname)s-%(module)s-%(message)s')
handler = logging.FileHandler('/vagrant/dataprocessing/commits/models.log')
handler.setFormatter(formatter)
logger = logging.getLogger('root')
logger.setLevel(logging.INFO)
logger.addHandler(handler)


class Commit:
    def __init__(self):
        """Initializes the issue DAO to get issue and sprint data from couchdb"""
        self.commits = []
        self.couch = couchdb.Server(settings.COUCH_DB_URL)
        try:
            logger.debug('models.Commit.__init__ - initializing Issue()')
            self.commits = self.couch['commits']
        except Exception as e:
            logger.error('models.Issue.__init__ - %s' % str(e))

    def get_commits(self, project_name):
        logger.info('getting commits for: ', project_name)
        return [self.commits[x] for x in self.commits if x.startswith(project_name) and project_name != ""]

    def get_all_commits(self):
        return [self.commits[x] for x in self.commits]

    def process_diff(self):
        try:
            for key in self.commits:
                commit = self.commits[key]
                if commit["diff"] is not None and ("added" not in commit or "deleted" not in commit):
                    added = 0
                    deleted = 0
                    diff = commit["diff"].encode('utf-8')
                    diff_lines = str(diff).splitlines()

                    for line in diff_lines:
                        if line.startswith('+') and not line.startswith('+++'):
                            added += 1
                        if line.startswith('-') and not line.startswith('---'):
                            deleted += 1
                    commit["added"] = added
                    commit["deleted"] = deleted

                    self.save_to_commits_db(commit, commit["_id"])
        except Exception as e:
            logger.error('commits.models process_diff - %s' % e)

    def save_to_commits_db(self, data, doc_key):
        try:
            db = self.couch['commits']
            if doc_key in db:
                doc = db.get(doc_key)
                new_doc = json.loads(json.dumps(data))
                new_doc["_id"] = doc["_id"]
                new_doc["_rev"] = doc["_rev"]
                db.save(new_doc)
            else:
                new_doc = json.loads(json.dumps(data))
                new_doc['_id'] = doc_key
                db.save(new_doc)
        except Exception as e:
            logger.error('commits.models save_to_commits_db - %s' % e)


class Save:
    def __init__(self):
        """Initializes the save DAO to save, remove, or return relevant project-related data from couchdb"""
        try:
            couch = couchdb.Server(settings.COUCH_DB_URL)
            logger.debug('models.Commit.__init__ - initializing Save()')
            self.db = couch['projects_data']
            logging.info('saving projects database')
        except Exception as e:
            logger.error('models.Save.__init__ - %s' % str(e))

    def insert(self, doc):
        logging.info('inserting into database doc: %s' % doc)
        self.db.save(doc)

    def delete(self, doc):
        logging.info('deleting from database doc: %s' % doc)
        self.db.delete(doc)

    def get(self):
        logging.info('getting all projects data documents')
        return [self.db[x] for x in self.db]


class Project:
    def __init__(self):
        """Initializes the save DAO to save, remove, or return relevant project-related data from couchdb"""
        try:
            couch = couchdb.Server(settings.COUCH_DB_URL)
            self.db = couch['project_info']
            logger.debug('models.Project.__init__ - initializing Project()')
        except Exception as e:
            logger.error('models.Project.__init__ - %s' % str(e))

    def get(self):
        logging.info('getting project info')
        return [self.db[x] for x in self.db]

    def get_db(self):
        logging.info('getting db')
        return self.db
