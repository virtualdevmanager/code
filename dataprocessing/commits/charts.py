import sched, time
import couchdb
import json
from dateutil.parser import parse
from datetime import timedelta
import numpy as np
import datetime
import json
import sys, os
from .models import Commit, Save, Project
import logging

formatter = logging.Formatter(fmt='%(asctime)s-%(levelname)s-%(module)s-%(message)s')
handler = logging.FileHandler('/vagrant/dataprocessing/logs/commit-charts.log')
handler.setFormatter(formatter)
logger = logging.getLogger('root')
logger.setLevel(logging.DEBUG)
logger.addHandler(handler)

DATE_RANGE = 14
NUM_MOST_RECENT_COMMITS = 5


def create_repo_activity(commits):
    """ create a dictionary which stores the numbers of commits in a project in a range of time
    :param commits: list, a list of Documents(couchdb.client.Document)
                 A Document example:
                 "_id":"<project_id>_<repo_id>_<commit_id>",
                  {
                      "date": "2016-05-02 22:32:27 need to be converted to local time",
                      "author": "",
                      "diff": "<diff text>",
                      "added": 100,
                      "deleted": 20
                  }
    :rtype: dict
    :return: a dict with keys: x_axis, x_axis_title, y_axis_title, categories, num (meaning number of commits)
    """
    try:
        end_date = parse(str(datetime.datetime.now()))
        start_date = end_date - timedelta(days=DATE_RANGE)
        delta = end_date - start_date
        # a list of dates in the range
        categories = [(str((start_date + timedelta(days=i)).month) + "/" + str(
            (start_date + timedelta(days=i)).day) + "/" + str((start_date + timedelta(days=i)).year)) for i in
                      range(delta.days + 1)]
        num_days = len(categories)

        num_of_commits_list = [0] * num_days
        if commits is not None:
            for commit in commits:
                if commit["date"] is not None:
                    commit_date = parse(str(commit["date"]))
                    commit_date = str(commit_date.month) + "/" + str(commit_date.day) + "/" + str(commit_date.year)
                    if commit_date in categories:
                        position = categories.index(commit_date)
                        num_of_commits_list[position] += 1

        np.set_printoptions(suppress=True)
        logger.info("num_of_commits:" + str(num_of_commits_list))

        data = {
            'x_axis_title': "Date",
            'y_axis_title': "Number of Commits",
            'categories': categories,
            'num': num_of_commits_list
        }
        return data

    except Exception as e:
        logger.error("dataprocessing/commits/charts.py create_repo_activity - %s" % e)
    return []


def create_activity_per_user(commits):
    """ create a dictionary which stores the numbers of commits by users in a project in a range of time
    :param commits: list, a list of Documents(couchdb.client.Document)
                 A Document example:
                 "_id":"<project_id>_<repo_id>_<commit_id>",
                  {
                      "date": "2016-05-02 22:32:27 need to be converted to local time",
                      "author": "",
                      "diff": "<diff text>",
                      "added": 100,
                      "deleted": 20
                  }
    :rtype: dict
    :return: a dict with keys: x_axis_title, y_axis_title, x_axis_values, y_axis_values
    """

    try:
        num_commits_by_team_member = {}  # stores the team member and commit number

        end_date = parse(str(datetime.datetime.now()))
        start_date = end_date - timedelta(days=DATE_RANGE)

        # iterate through the commits and only use the commits where the date is less than the start date
        if commits is not None:
            for commit in commits:

                if commit["date"] is not None and parse(str(commit["date"])) >= start_date:
                    logger.info("commit date: " + str(parse(str(commit["date"]))))
                    commit_author = commit["author"]

                    if commit_author is not None and commit_author not in num_commits_by_team_member:
                        num_commits_by_team_member[commit_author] = 1
                        logger.info('adding ' + commit_author + ' to the dictionary for the first time')
                    elif commit_author is not None and commit_author in num_commits_by_team_member:
                        num_commits_by_team_member[commit_author] += 1
                        logger.info('incrementing the commit number for ' + commit_author)
                else:
                    logger.info('date is outside of the range')
                    continue

        data = {
            'x_axis_title': "Team Members",
            'y_axis_title': "Number of Commits",
            'x_axis_values': num_commits_by_team_member.keys(),
            'y_axis_values': num_commits_by_team_member.values()
        }
        return data
    except Exception as e:
        logger.error("dataprocessing/commits/charts.py create_activity_per_user - %s" % e)


def create_modification_activity(commits):
    """ create a dictionary which stores the lines of added/deleted code in a project in a range of time
    :param commits: list, a list of Documents(couchdb.client.Document)
                 A Document example:
                 "_id":"<project_id>_<repo_id>_<commit_id>",
                  {
                      "date": "2016-05-02 22:32:27 need to be converted to local time",
                      "author": "",
                      "diff": "<diff text>",
                      "added": 100,
                      "deleted": 20
                  }
    :rtype: dict
    :return: a dict with keys: x_axis_title, y_axis_title, categories (dates in the time range),
                               added(lines of code added), deleted(lines of code deleted)
    """
    try:
        end_date = parse(str(datetime.datetime.now()))
        start_date = end_date - timedelta(days=DATE_RANGE)
        delta = end_date - start_date
        categories = [(str((start_date + timedelta(days=i)).month) + "/" + str(
            (start_date + timedelta(days=i)).day) + "/" + str((start_date + timedelta(days=i)).year)) for i in
                      range(delta.days + 1)]
        num_days = len(categories)

        added_list = [0] * num_days
        deleted_list = [0] * num_days

        if commits is not None:
            for commit in commits:
                if commit["date"] is not None:
                    date = parse(str(commit["date"]))
                    date = str(date.month) + "/" + str(date.day) + "/" + str(date.year)
                    if date in categories:
                        position = categories.index(date)
                        if "added" in commit and commit["added"] is not None:
                            added_list[position] += commit["added"]
                        if "deleted" in commit and commit["deleted"] is not None:
                            deleted_list[position] += commit["deleted"]

        np.set_printoptions(suppress=True)
        data = {
            'x_axis_title': "Date",
            'y_axis_title': "Lines of Code",
            'categories': categories,
            'added': added_list,
            'deleted': deleted_list
        }
        return data

    except Exception as e:
        logger.error("dataprocessing/commits/charts.py create_modification_activity - %s" % e)
        return []


def create_commit_case_activity(commits, project_name):
    """ create a dictionary which stores the most recent commits in the project
    :param commits: list, a list of Documents(couchdb.client.Document)
                 A Document example:
                 "_id":"<project_id>_<repo_id>_<commit_id>",
                  {
                      "date": "2016-05-02 22:32:27 need to be converted to local time",
                      "author": "",
                      "diff": "<diff text>",
                      "added": 100,
                      "deleted": 20
                  }
    :param project_name: the id of the project
    :rtype: dict
    :return: a dict with keys: last_commits (a list of most recent commits and each element contains the commit time,
             commit id and commit author), project_info_to_return (a dict with key 'bitbucket_id and its value is the
             project id in the repo server)
    """
    last_commits_by_date = {}

    value = project_name
    values = value.split("_")
    if len(values) < 2:
        return {}

    project_info_to_return = {
        "team_id": values[0],
        "repo_id": values[1]
    }

    list = []
    if commits is not None:
        for commit in commits:
            if commit["date"] is not None:
                date = str(commit["date"])
                commit_id = commit["_id"].split("_")
                if len(commit_id) < 3:
                    continue
                commit_name = commit_id[2]
                last_commits_by_date[date] = {
                    "id": commit_name,
                    "author": commit["author"],
                    "comment": commit["comment"]
                }
        list = sorted(last_commits_by_date.items(), key=lambda t: t[0], reverse=True)

    data = {
        'last_commits': list[:NUM_MOST_RECENT_COMMITS],
        'project_info_to_return': project_info_to_return
    }

    return data


def create_modification_activity_per_member(commits):
    """ create a dictionary which stores the lines of added/deleted code by users in a project in a range of time
    :param commits: list, a list of Documents(couchdb.client.Document)
                 A Document example:
                 "_id":"<project_id>_<repo_id>_<commit_id>",
                  {
                      "date": "2016-05-02 22:32:27 need to be converted to local time",
                      "author": "",
                      "diff": "<diff text>",
                      "added": 100,
                      "deleted": 20
                  }
    :rtype: dict
    :return: a dict with keys: x_axis_title, y_axis_title, categories (usernames),
                               added(lines of code added), deleted(lines of code deleted)
    """
    try:
        end_date = parse(str(datetime.datetime.now()))
        start_date = end_date - timedelta(days=DATE_RANGE)

        num_lines_added_by_member = {}
        num_lines_deleted_by_member = {}

        if commits is not None:
            for commit in commits:
                if commit["date"] is not None and parse(str(commit["date"])) >= start_date:
                    commit_author = commit["author"]

                    if commit_author is not None and commit_author not in num_lines_added_by_member:
                        lines_added = 0
                        if "added" in commit and commit["added"] is not None:
                            lines_added = commit["added"]
                        num_lines_added_by_member[commit_author] = lines_added

                        lines_deleted = 0
                        if "deleted" in commit and commit["deleted"] is not None:
                            lines_deleted = commit["deleted"]
                        num_lines_deleted_by_member[commit_author] = lines_deleted
                    elif commit_author is not None and commit_author in num_lines_added_by_member:
                        lines_added = 0
                        if "added" in commit and commit["added"] is not None:
                            lines_added = commit["added"]
                        num_lines_added_by_member[commit_author] += lines_added

                        lines_deleted = 0
                        if "deleted" in commit and commit["deleted"] is not None:
                            lines_deleted = commit["deleted"]
                        num_lines_deleted_by_member[commit_author] += lines_deleted

        data = {
            'x_axis_title': "Team Members",
            'y_axis_title': "Lines of Code",
            'categories': num_lines_added_by_member.keys(),
            'added': num_lines_added_by_member.values(),
            'deleted': num_lines_deleted_by_member.values()
        }
        return data

    except Exception as e:
        logger.error("dataprocessing/commits/charts.py create_modification_activity_per_member - %s" % e)
        return []
