import sched, time
import couchdb
import json
from dateutil.parser import parse
from datetime import timedelta
import numpy as np
import datetime
import json
import sys,os

from issues.update import handle_update as issue
from builds.update import handle_update as build
from commits.update import handle_update as commit
import logging
formatter = logging.Formatter(fmt='%(asctime)s-%(levelname)s-%(module)s-%(message)s')
handler = logging.FileHandler('/vagrant/dataprocessing/start.log')
handler.setFormatter(formatter)
logger = logging.getLogger('root')
logger.setLevel(logging.INFO)
logger.addHandler(handler)


def start(sc):
    try:
        first_time = issue()
        logger.info(">> issue() = %s" % first_time)
        # set this to first_time once we implement bamboo
        build()
        commit()
        if first_time == 1:
            logger.info(">> start: first time")
            sc.enter(5, 1, start, (sc,))
        else:
            logger.info(">> start: not first time")
            sc.enter(60, 1, start, (sc,))
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)

if __name__ == '__main__':
    try:
        s = sched.scheduler(time.time, time.sleep)
        s.enter(60, 1, start, (s,))
        s.run()
        logger.info(">> running start from main")
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
