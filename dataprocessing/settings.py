import os
import socket
import fcntl
import struct
import sys
import logging

formatter = logging.Formatter(fmt='%(asctime)s-%(levelname)s-%(module)s-%(message)s')
handler = logging.FileHandler('/vagrant/dataprocessing/logs/settings.log')
handler.setFormatter(formatter)
logger = logging.getLogger('root')
logger.setLevel(logging.WARNING)
logger.addHandler(handler)

def get_ip_address(ifname):
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        return socket.inet_ntoa(fcntl.ioctl(
            s.fileno(), 0x8915, struct.pack('256s', ifname[:15])
            )[20:24])
    except Exception as e:
        logger.error('dataprocessing.settings.get_ip_address - %s' % e)
        return "0.0.0.0"

IP_ADDRESS = get_ip_address('eth1')

COUCH_DB_URL = "http://" + get_ip_address('eth1') + ":5984/"