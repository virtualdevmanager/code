import sched, time
import couchdb
import json
from dateutil.parser import parse
from datetime import timedelta
import numpy as np
import datetime
import json
import sys, os
from .models import Issue, Save, Project
import logging

formatter = logging.Formatter(fmt='%(asctime)s-%(levelname)s-%(module)s-%(message)s')
handler = logging.FileHandler('/vagrant/dataprocessing/logs/charts.log')
handler.setFormatter(formatter)
logger = logging.getLogger('root')
logger.setLevel(logging.WARNING)
logger.addHandler(handler)

CLOSED_ISSUE_STATUS_CODE = "Closed"
OPEN_ISSUE_STATUS_CODE = "Open"
IN_PROGRESS_ISSUE_STATUS_CODE = "In Progress"


def accumulate(list):
    """ accumulate the values in a list, lis[i] = lis[0] + .. + lis[i]
    :param list: a list
    :return: a list, lis[1] = lis[0] + lis[1], lis[2] = lis[0] + lis[1] + lis[2], list[3] = lis[0] + lis[1] + lis[2] + lis[3]
    """
    try:
        total = 0
        list_accumulated = []
        for value in list:
            total = total + value
            list_accumulated.append(total)
        return list_accumulated
    except Exception as e:
        logger.error("dataprocessing/issues/charts.py accumulate - %s" % e)
        return []


def create_project_progress(status):
    """ return a dict which shows the progress(%) of the project (closed issues/total issues)
    :param status: a dict with keys: open/progress/closed, open: open issues
    :rtype: dict
    :return: a dict with keys: progress, shows the progress(%) of the project
    """
    try:
        if sum(status.values()) != 0:
            return {"progress": float(status["closed"]) / sum(status.values()) * 100}
        else:
            return {"progress": 0}
    except Exception as e:
        logger.error("dataprocessing/issues/charts.py create_project_progress - %s" % e)
        return {}


def create_issues_status(data):
    """ create a dictionary which stores the numbers of open/closed/in-progress issues in a project
    :param data: list, list of dicts of Documents(couchdb.client.Document)
                 A Document example:
                 {'status': 'Closed', 'resolve_date': '2016-02-18T00:54:58.694+0000',
                 'logged': 0, 'due': None, 'creation_date': '2016-01-25T15:49:00.000+0000',
                 'priority': 'Medium', 'assignee': 'graghav', 'sprint': 'VDMBOAR Sprint 1',
                 'summary': 'Review Top 2 risks and TOS', 'estimate': 1}
    :rtype: dict
    :return: a dict with keys: open/progress/closed
    """
    try:
        issues_open = 0
        issues_in_progress = 0
        issues_closed = 0
        for issue in data:
            # if the issue is open
            if issue["status"].startswith(OPEN_ISSUE_STATUS_CODE):
                issues_open += 1
            # if the issue is in progress
            elif issue["status"].startswith(IN_PROGRESS_ISSUE_STATUS_CODE):
                issues_in_progress += 1
            # if the issue is closed
            elif issue["status"].startswith(CLOSED_ISSUE_STATUS_CODE):
                issues_closed += 1
        data = {
            'open': issues_open,
            'progress': issues_in_progress,
            'closed': issues_closed
        }
        return data
    except Exception as e:
        logger.error("dataprocessing/issues/charts.py create_issues_status - %s" % e)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
    return {
        'open': 0,
        'progress': 0,
        'closed': 0
    }


def create_issues_count_by_priority(data):
    """ create a dictionary which stores the numbers of high(est)/medium/low(est) issues
        which are either open or in-progress in a project
    :param data: data: dict, a dict of Documents(couchdb.client.Document)
                 A Document example:
                 {'status': 'Closed', 'resolve_date': '2016-02-18T00:54:58.694+0000',
                 'logged': 0, 'due': None, 'creation_date': '2016-01-25T15:49:00.000+0000',
                 'priority': 'Medium', 'assignee': 'graghav', 'sprint': 'VDMBOAR Sprint 1',
                 'summary': 'Review Top 2 risks and TOS', 'estimate': 1}
    :rtype: dict
    :return: a dict with keys: high/low/medium
    """
    try:
        issues_high = 0
        issues_low = 0
        issues_medium = 0
        for issue in data:
            # the issue should be either open or in progress
            if issue["status"].startswith(OPEN_ISSUE_STATUS_CODE) \
                    or issue["status"].startswith(IN_PROGRESS_ISSUE_STATUS_CODE):
                priority = str(issue["priority"])
                if priority == "Medium":
                    issues_medium += 1
                # issues with highest or high priority
                elif priority == "High" or priority == "Highest":
                    issues_high += 1
                # issues with lowest or low priority
                elif priority == "Low" or priority == "Lowest":
                    issues_low += 1
        data = {
            'high': issues_high,
            'low': issues_low,
            'medium': issues_medium
        }
        return data
    except Exception as e:
        logger.error("dataprocessing/issues/charts.py create_issues_count_by_priority - %s" % e)
    return {}


def get_project_date(sprint):
    """ return the start date (the beginning of the earliest sprint) and
        the end date (the end of the latest sprint) of the project
    :param sprint: list of dicts, each element is a dict of Documents(couchdb.client.Document) including all the sprints
                 A Document example:
                 { "_id":"VDM_Sprint 05.24.2017 - 06.05.2017","_rev":"3-77b183630f330d28c3d3db43c1d09a39",
                 "start_date":"24/May/17 10:42 PM","end_date":"05/Jun/17 10:42 PM"}
    :rtype: start_date: str, the start date of of the earliest sprint (eg. "24/May/17 10:42 PM")
            end_date: str, the end date of the latest sprint (eg. "24/May/17 10:42 PM")
    """
    try:
        start_date = sprint[0]["start_date"]
        end_date = sprint[0]["end_date"]
        for item in sprint:
            if str(item["_id"]) == "VDM_Sprint 4":
                # ignore a sprint with wrong start date
                continue
            if parse(item["end_date"]) > parse(end_date):
                end_date = item["end_date"]
            if parse(item["start_date"]) < parse(start_date):
                start_date = item["start_date"]
        return start_date, end_date
    except Exception as e:
        print str(e)
        logger.error("dataprocessing/issues/charts.py get_project_date - %s" %e)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
    return []


def create_project_burndown(data, sprint, project_name):
    """ create a dictionary which is used to show the project burndown chart
    :param data: dict, a dict of Documents(couchdb.client.Document) including all the issues
                 A Document example:
                 {'status': 'Closed', 'resolve_date': '2016-02-18T00:54:58.694+0000',
                 'logged': 0, 'due': None, 'creation_date': '2016-01-25T15:49:00.000+0000',
                 'priority': 'Medium', 'assignee': 'graghav', 'sprint': 'VDMBOAR Sprint 1',
                 'summary': 'Review Top 2 risks and TOS', 'estimate': 1}
    :param sprint: dict, a dict of Documents(couchdb.client.Document) including all the sprints
                 A Document example:
                 { "_id":"VDM_Sprint 05.24.2017 - 06.05.2017","_rev":"3-77b183630f330d28c3d3db43c1d09a39",
                 "start_date":"24/May/17 10:42 PM","end_date":"05/Jun/17 10:42 PM"}
    :param project_name: the id of the project
    :rtype: dict
    :return: a dict with keys: 'x_axis', 'x_axis_title', 'y_axis_title','categories','estimated', 'remaining'
            the dict is to used to draw the project burndown chart
    """
    try:
        # start_date: str, the start date of the oldest sprint
        # end_data: str, the end date of the latest sprint
        start_date, end_date = get_project_date(sprint)
        start_date = parse(start_date)
        end_date = parse(end_date)
        delta = end_date - start_date
        categories = [(str((start_date + timedelta(days=i)).month) + "/" + str(
            (start_date + timedelta(days=i)).day) + "/" + str((start_date + timedelta(days=i)).year)) for i in
                      range(delta.days + 1)]
        num_days = len(categories)
        # the total estimated values in this project
        estimated_total = 0.0
        current_date = str(datetime.datetime.now().month) + "/" + str(datetime.datetime.now().day) + \
                       "/" + str(datetime.datetime.now().year)
        # a list of estimates, each element is the total estimates resolved in one day
        completed_list = [0] * num_days
        for issue in data:
            issue_estimate = 0.0
            if issue["estimate"] is not None:
                issue_estimate = float(issue["estimate"])
                estimated_total += issue_estimate
            if issue["resolve_date"] is not None:
                resolve_date = parse(issue["resolve_date"])
                resolve_date = str(resolve_date.month) + "/" + str(resolve_date.day) + "/" + str(resolve_date.year)
                if resolve_date in categories:
                    position = categories.index(resolve_date)
                    completed_list[position] += issue_estimate
        np.set_printoptions(suppress=True)
        # estimated_total:estimatedburndownchart

        if estimated_total != 0:
            estimated_list = np.arange(estimated_total, -1.0, -1.0 * estimated_total / float(num_days - 1))
        else:
            estimated_list = np.array([0] * num_days)
        completed_list = accumulate(completed_list)
        index_for_curr_date = num_days - 1
        if current_date in categories:
            index_for_curr_date = categories.index(current_date)
        # the remaining estimates till a day( eg,remaining_list[1] indicates how many estimates
        # left on the second day of the project)
        remaining_list = list(estimated_list.tolist()[:index_for_curr_date + 1])
        for index in xrange(0, index_for_curr_date + 1):
            remaining_list[index] = estimated_total - completed_list[index]
        data = {
            'x_axis': "value",
            'x_axis_title': "Date",
            'y_axis_title': "Story Points",
            'categories': categories,
            'estimated': estimated_list.tolist(),
            'remaining': remaining_list
        }
        return data
    except Exception as e:
        logger.error("dataprocessing/issues/charts.py create_project_burndown - %s" % e)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
    return {}


def create_burndown(data, sprint, project_name):
    """ create a dictionary which is used to show the latest sprint burndown chart
    :param data: dict, a dict of Documents(couchdb.client.Document) including all the issues
                 A Document example:
                 {'status': 'Closed', 'resolve_date': '2016-02-18T00:54:58.694+0000',
                 'logged': 0, 'due': None, 'creation_date': '2016-01-25T15:49:00.000+0000',
                 'priority': 'Medium', 'assignee': 'graghav', 'sprint': 'VDMBOAR Sprint 1',
                 'summary': 'Review Top 2 risks and TOS', 'estimate': 1}
    :param sprint: dict, a dict of Documents(couchdb.client.Document) including all the sprints
                 A Document example:
                 { "_id":"VDM_Sprint 05.24.2017 - 06.05.2017","_rev":"3-77b183630f330d28c3d3db43c1d09a39",
                 "start_date":"24/May/17 10:42 PM","end_date":"05/Jun/17 10:42 PM"}
    :param project_name: the id of the project
    :rtype: dict
    :return: a dict with keys: 'x_axis', 'x_axis_title', 'y_axis_title','categories','estimated', 'remaining'
            the dict is to used to draw the sprint burndown chart
    """
    try:
        if sprint:
            # categories: a list of the dates in the lastest sprint
            # sprint_id: the id of the latest sprint
            # end_date: the end date of the latest sprint
            categories, sprint_id, end_date = helper(sprint, project_name)
            num_days = len(categories)
            # the total estimated values in the latest sprint
            estimated_total = 0.0
            current_date = str(datetime.datetime.now().month) + "/" + str(datetime.datetime.now().day) + \
                           "/" + str(datetime.datetime.now().year)
            # a list of estimates, each element is the total estimates resolved in one day
            completed_list = [0] * num_days
            for issue in data:
                # if the issue belongs to the latest sprint
                if str(issue["sprint"]) == str(sprint_id):
                    issue_estimate = 0.0
                    if issue["estimate"] is not None:
                        issue_estimate = float(issue["estimate"])
                        estimated_total += issue_estimate
                    if issue["resolve_date"] is not None:
                        resolve_date = parse(issue["resolve_date"])
                        resolve_date = str(resolve_date.month) + "/" + str(resolve_date.day) \
                                       + "/" + str(resolve_date.year)
                        if resolve_date in categories:
                            position = categories.index(resolve_date)
                            completed_list[position] += issue_estimate
            np.set_printoptions(suppress=True)
            # estimated_total: estimated burndown chart
            if estimated_total != 0:
                estimated_list = np.arange(estimated_total, -1.0, -1.0 * estimated_total / float(num_days - 1))
            else:
                estimated_list = np.array([0] * num_days)
            # accumulate the total resolved estimates till each date in the latest sprint and store the result in a list
            completed_list = accumulate(completed_list)
            index_for_curr_date = num_days - 1
            if current_date in categories:
                index_for_curr_date = categories.index(current_date)
            # the remaining estimates till a day(eg, remaining_list[1] indicates how many estimates
            # left on the second day of the latest sprint)
            remaining_list = list(estimated_list.tolist()[:(index_for_curr_date + 1)])
            for index in xrange(0, index_for_curr_date + 1):
                remaining_list[index] = estimated_total - completed_list[index]

            data = {
                'x_axis': "value",
                'x_axis_title': "Date",
                'y_axis_title': "Story Points",
                'categories': categories,
                'estimated': estimated_list.tolist(),
                'remaining': remaining_list
            }
            return data
        else:
            return {
                'x_axis': "value",
                'x_axis_title': "Date",
                'y_axis_title': "Story Points",
                'categories': [],
                'estimated': [],
                'remaining': []
            }
    except Exception as e:
        logger.error("dataprocessing/issues/charts.py create_burndown - %s" % e)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
    return {}


def create_case_status(data, sprint=None, project_name=None):
    """ create a dictionary which will be used to show the status of
        issues assigned to different team members
    :param project_name:
    :param sprint: if sprint is None, it returns the chart for the whole project; if the sprint is not None,
           it returns the chart for the latest sprint
    :param data: dict, a dict of Documents(couchdb.client.Document)
                 A Document example:
                 {'status': 'Closed', 'resolve_date': '2016-02-18T00:54:58.694+0000',
                 'logged': 0, 'due': None, 'creation_date': '2016-01-25T15:49:00.000+0000',
                 'priority': 'Medium', 'assignee': 'graghav', 'sprint': 'VDMBOAR Sprint 1',
                 'summary': 'Review Top 2 risks and TOS', 'estimate': 1}
    :rtype: dict
    :return: a dict with keys: x_axis, x_axis_title, y_axis, y_axis_title, closed, in_progress,
                               open, estimated, not_estimated
            x_axis: the people
            y_axis: number of issues
            closed: each person's number of closed issues
            in_progress: each person's number of in-progress issues
            open: each person's number of open issues
            estimated: each person's number of estimated issues
            not_estimated: each person's number of unestimated issues
    """
    try:
        # users' names which have ever been assigned with any issue

        issues_closed_dict = {}
        issues_open_dict = {}
        issues_in_progress_dict = {}
        latest_sprint_id = ""

        if sprint is not None and project_name is not None:
            categories, latest_sprint_id, end_date = helper(sprint, project_name)
            usernames = list(set([person["assignee"] for person in data if
                                  person["assignee"] != "" and person["sprint"] == latest_sprint_id]))
        else:
            usernames = list(set([person["assignee"] for person in data if person["assignee"] != ""]))

        for item in data:
            if sprint is not None and str(item["sprint"]) != str(latest_sprint_id):
                continue
            if item["status"].startswith(CLOSED_ISSUE_STATUS_CODE):
                if item["assignee"] in issues_closed_dict.keys():
                    issues_closed_dict[item["assignee"]] += 1
                else:
                    issues_closed_dict[item["assignee"]] = 1
            elif item["status"].startswith(OPEN_ISSUE_STATUS_CODE):
                if item["assignee"] in issues_open_dict.keys():
                    issues_open_dict[item["assignee"]] += 1
                else:
                    issues_open_dict[item["assignee"]] = 1
            elif item["status"].startswith(IN_PROGRESS_ISSUE_STATUS_CODE):
                if item["assignee"] in issues_in_progress_dict.keys():
                    issues_in_progress_dict[item["assignee"]] += 1
                else:
                    issues_in_progress_dict[item["assignee"]] = 1

        # a list, each element is the number of closed issues assigned to a specific person
        # if the index of a person in `people` is i, then the number of closed issues
        # assigned to this person is closed[i].
        issues_closed_list = []
        # similar to `closed`
        issues_open_list = []
        # similar to `closed`
        issues_in_progress_list = []

        for user in usernames:
            if user in issues_closed_dict.keys():
                issues_closed_list.append(issues_closed_dict[user])
            else:
                issues_closed_list.append(0)
            if user in issues_open_dict.keys():
                issues_open_list.append(issues_open_dict[user])
            else:
                issues_open_list.append(0)
            if user in issues_in_progress_dict.keys():
                issues_in_progress_list.append(issues_in_progress_dict[user])
            else:
                issues_in_progress_list.append(0)

        # a list, each element is the number of issues which has 'estimate' value assigned to a specific person
        # if the index of a person in `people` is i, then the number of estimated issues
        # assigned to this person is estimated[i].
        issues_estimated = []
        issues_not_estimated = []
        for user in usernames:
            if sprint is not None and project_name is not None:
                issues_estimated.append(
                    sum([1 for item in data if item["sprint"] == latest_sprint_id and
                         item["estimate"] is not None and item["assignee"] == user]))
                issues_not_estimated.append(
                    sum([1 for item in data if item["sprint"] == latest_sprint_id and
                         item["estimate"] is None and item["assignee"] == user]))
            else:
                issues_estimated.append(
                    sum([1 for item in data if item["estimate"] is not None and item["assignee"] == user]))
                issues_not_estimated.append(
                    sum([1 for item in data if item["estimate"] is None and item["assignee"] == user]))

        data = {
            'x_axis': [str(x) for x in usernames],
            'x_axis_title': "Developers",
            'y_axis_title': "Number of Issues",
            'closed': issues_closed_list,
            'in_progress': issues_in_progress_list,
            'open': issues_open_list,
            'estimated': issues_estimated,
            'not_estimated': issues_not_estimated
        }

        return data
    except Exception as e:
        logger.error("dataprocessing/issues/charts.py create_case_status - %s" % e)
    return {}


def helper(sprint, project_name):
    """ a function used to get the dates, id, and the end date of the latest sprint
    :param sprint: dict, a dict of Documents(couchdb.client.Document) including all the sprints
                 A Document example:
                 { "_id":"VDM_Sprint 05.24.2017 - 06.05.2017","_rev":"3-77b183630f330d28c3d3db43c1d09a39",
                 "start_date":"24/May/17 10:42 PM","end_date":"05/Jun/17 10:42 PM"}
    :param project_name: str, the project id
    :return: categories: a list of the dates in the lastest sprint
             sprint id: str, the id of the latest sprint
             end_date: datetime.datetime, the end date of the latest sprint
                      (example: datetime.datetime(2017,6,5,22,42) -> 06/05/2017 22:42)
    """
    try:
        latest_sprint = ""
        cur_date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        # find the first sprint that starts before cur_date in the list of sprints
        for item in sprint:
            if parse(item["start_date"]).strftime("%Y-%m-%d %H:%M:%S") < cur_date:
                latest_sprint = item
                break

        for item in sprint:
            if parse(item["end_date"]) > parse(latest_sprint["end_date"]) and \
                            parse(item["start_date"]).strftime("%Y-%m-%d %H:%M:%S") < cur_date:
                latest_sprint = item
        start_date = parse(latest_sprint["start_date"])
        end_date = parse(latest_sprint["end_date"])
        logger.info("the most recent sprint start_date:" + str(start_date) + ", end_date:" + str(end_date))
        delta = end_date - start_date
        categories = [(str((start_date + timedelta(days=i)).month) + "/" + str((start_date + timedelta(days=i)).day) +
                       "/" + str((start_date + timedelta(days=i)).year)) for i in range(delta.days + 1)]
        sprint_id = latest_sprint["_id"][(len(project_name) + 1):]
        return categories, sprint_id, end_date
    except Exception as e:
        logger.error("dataprocessing/issues/charts.py helper - %s" % e)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
    return []


def create_workload(data, sprint, project_name):
    """ create a dictionary which will be used to show the total estimate (story points or hours)
        of not closed issues assigned to team members in the latest sprint of a project (id: project name)
    :param data: dict, a dict of Documents(couchdb.client.Document) including all the issues
                 A Document example:
                 {'status': 'Closed', 'resolve_date': '2016-02-18T00:54:58.694+0000',
                 'logged': 0, 'due': None, 'creation_date': '2016-01-25T15:49:00.000+0000',
                 'priority': 'Medium', 'assignee': 'graghav', 'sprint': 'VDMBOAR Sprint 1',
                 'summary': 'Review Top 2 risks and TOS', 'estimate': 1}
    :param sprint: dict, a dict of Documents(couchdb.client.Document) including all the sprints
                 A Document example:
                 { "_id":"VDM_Sprint 05.24.2017 - 06.05.2017","_rev":"3-77b183630f330d28c3d3db43c1d09a39",
                 "start_date":"24/May/17 10:42 PM","end_date":"05/Jun/17 10:42 PM"}
    :param project_name: the project id
    :rtype: dict
    :return: a dict with keys: categories, lists
             categories: the usernames in the team
             lists: used to draw project workload chart
    """
    try:
        if sprint:
            # categories: not used
            # sprint_id: the id of the latest sprint
            # end_date: not used
            categories, sprint_id, end_date = helper(sprint, project_name)

            # all the usernames which have ever been assigned with any issue
            # usernames = list(set([person["assignee"] for person in data if person["assignee"] != ""]))
            usernames = list(set([person["assignee"] for person in data if person["assignee"] != ""
                                  and str(person["sprint"]) == str(sprint_id)]))
            # initialization
            workload_dict = dict.fromkeys(usernames, 0.0)

            for issue in data:
                # if the issue belongs to the latest sprint
                if str(issue["sprint"]) == str(sprint_id):
                    issue_estimate = 0.0
                    # if the issue is estimated and not closed
                    if issue["estimate"] is not None and not issue["status"].startswith(CLOSED_ISSUE_STATUS_CODE):
                        issue_estimate = float(issue["estimate"])
                        if issue["assignee"] != "":
                            # update the workload of the assignee
                            workload_dict[issue["assignee"]] += issue_estimate

            # lists: to be used in the project workload chart
            # each element of lists represents the workload of a person in the lastest sprint
            lists = [[], [], []]
            for item in workload_dict.values():
                if item > 40:
                    lists[0].append(20.0)
                    lists[1].append(20.0)
                    lists[2].append(item - 40.0)
                elif item > 20:
                    lists[0].append(20.0)
                    lists[1].append(item - 20.0)
                    lists[2].append(0.0)
                else:
                    lists[0].append(item)
                    lists[1].append(0.0)
                    lists[2].append(0.0)
            return {
                'categories': [str(x) for x in workload_dict.keys()],
                'lists': lists
            }
    except Exception as e:
        logger.error("dataprocessing/issues/charts.py create_workload - %s" % e)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
    return {
        'categories': [],
        'lists': [[], [], []]
    }


def get_global_workload(data, sprint, project_name):
    """ create a dictionary which will be used to show the total estimate (story points or hours) per week
        of not closed issues assigned to team members in the latest sprint in a project (id: project name).
    :param data: dict, a dict of Documents(couchdb.client.Document) including all the issues
                 A Document example:
                 {'status': 'Closed', 'resolve_date': '2016-02-18T00:54:58.694+0000',
                 'logged': 0, 'due': None, 'creation_date': '2016-01-25T15:49:00.000+0000',
                 'priority': 'Medium', 'assignee': 'graghav', 'sprint': 'VDMBOAR Sprint 1',
                 'summary': 'Review Top 2 risks and TOS', 'estimate': 1}
    :param sprint: dict, a dict of Documents(couchdb.client.Document) including all the sprints
                 A Document example:
                 { "_id":"VDM_Sprint 05.24.2017 - 06.05.2017","_rev":"3-77b183630f330d28c3d3db43c1d09a39",
                 "start_date":"24/May/17 10:42 PM","end_date":"05/Jun/17 10:42 PM"}
    :param project_name: the project id
    :rtype: dict
    :return: a dict with keys: categories, lists
             categories: the usernames in the team
             lists: used to draw global workload chart
    """
    try:
        if sprint:
            categories, sprint_id, end_date = helper(sprint, project_name)
            current_date = str(datetime.datetime.now().month) + "/" + str(datetime.datetime.now().day)

            people = list(set([person["assignee"] for person in data if person["assignee"] != ""]))
            data_dict = dict.fromkeys(people, 0.0)

            for issue in data:
                if str(issue["sprint"]) == str(sprint_id):
                    issue_estimate = 0.0
                    if issue["estimate"] is not None and not issue["status"].startswith(CLOSED_ISSUE_STATUS_CODE):
                        issue_estimate = float(issue["estimate"])
                        if issue["assignee"] != "":
                            data_dict[issue["assignee"]] += issue_estimate

            current_date = datetime.datetime.now()
            num_days_remaining = np.busday_count(current_date.date(), end_date.date())
            np.set_printoptions(suppress=True)
            for item in data_dict.keys():
                # normalize the workload as different projects may have different length of sprints.
                # this chart only displays the workload of members for each week
                if data_dict[item] != 0.0 and num_days_remaining > 5:
                    # the length of a sprint is more than a week
                    data_dict[item] = float(data_dict[item] / num_days_remaining) * 5.0
            return data_dict
    except Exception as e:
        logger.error("dataprocessing/issues/charts.py get_global_workload - %s" % e)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
    return {}
