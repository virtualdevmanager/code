import sched, time
import couchdb
import json
from dateutil.parser import parse
from datetime import timedelta
import numpy as np
import datetime
import json
import sys, os
import settings
import logging
formatter = logging.Formatter(fmt='%(asctime)s-%(levelname)s-%(module)s-%(message)s')
handler = logging.FileHandler('/vagrant/dataprocessing/issues/models.log')
handler.setFormatter(formatter)
logger = logging.getLogger('root')
logger.setLevel(logging.WARNING)
logger.addHandler(handler)


class Issue:
    def __init__(self):
        """Initializes the issue DAO to get issue and sprint data from couchdb"""
        self.builds = []
        try:
            couch = couchdb.Server(settings.COUCH_DB_URL)
            logger.debug('models.Issue.__init__ - initializing Issue()')
            self.issues = couch['issues']
            self.sprint = couch['sprints']
        except Exception as e:
            logger.error('models.Issue.__init__ - %s' % str(e))

    def get_issues(self, project_name):
        logger.info('getting issues for: ', project_name)
        return [self.issues[x] for x in self.issues if x.startswith(project_name) and project_name != ""]

    def get_sprint_data(self, project_name):
        logger.info('getting sprint data for: ', project_name)
        return [self.sprint[x] for x in self.sprint if x.startswith(project_name) and project_name != ""]


class Save:
    def __init__(self):
        """Initializes the save DAO to save, remove, or return relevant project-related data from couchdb"""
        try:
            couch = couchdb.Server(settings.COUCH_DB_URL)
            logger.debug('models.Issue.__init__ - initializing Save()')
            self.db = couch['projects_data']
            logging.info('saving projects database')
        except Exception as e:
            logger.error('models.Save.__init__ - %s' % str(e))

    def insert(self, doc):
        logging.info('inserting into database doc: %s' % doc)
        self.db.save(doc)

    def delete(self, doc):
        logging.info('deleting from database doc: %s' % doc)
        self.db.delete(doc)

    def get(self):
        logging.info('getting all projects data documents')
        return [self.db[x] for x in self.db]


class Project:
    def __init__(self):
        """Initializes the save DAO to save, remove, or return relevant project-related data from couchdb"""
        try:
            couch = couchdb.Server(settings.COUCH_DB_URL)
            self.db = couch['project_info']
            logger.debug('models.Project.__init__ - initializing Project()')
        except Exception as e:
            logger.error('models.Project.__init__ - %s' % str(e))

    def get(self):
        logging.info('getting project info')
        return [self.db[x] for x in self.db]

    def get_db(self):
        logging.info('getting db')
        return self.db