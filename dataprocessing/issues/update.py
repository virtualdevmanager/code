import sched,time
import couchdb
import json
from dateutil.parser import parse
from datetime import timedelta
import numpy as np
import datetime
import json
import sys,os

import logging
from .charts import *
from .models import *

formatter = logging.Formatter(fmt='%(asctime)s-%(levelname)s-%(module)s-%(message)s')
handler = logging.FileHandler('/vagrant/dataprocessing/issues/processing.log')
handler.setFormatter(formatter)
logger = logging.getLogger('root')
logger.setLevel(logging.ERROR)
logger.addHandler(handler)
logger.info("level:"+ str(logger.getEffectiveLevel()))


def date_handler(obj):
    """Formats an object into date into YYYY-MM-DD format"""
    logger.info('formatting date into isoformat')
    return obj.isoformat() if hasattr(obj, 'isoformat') else obj


def handle_update():
    """Refreshes the data required to produce the dashboard charts from the database
    rype: int
    Return Type: 0 if this is the first time we are updating, 1 if not
    """
    update_first_time = 0       # whether or not it is the first time we are loading the data
    issues = Issue()            # issues and sprint DAO
    save = Save()               # project data DAO
    docs = save.get()           # getting all the project data needed for charts
    projects = Project().get()  # getting all the configured project information
    devload = []                # stores an array of workload dictionaries (by sprint)
    global_issue_status = {}    # maps project name to issue breakdown by status type

    logger.info('iterating through the sets of project information')
    for project in projects:
        # if the JIRA ID was provided in the project configurations then pull the project data related to that JIRA ID
        # from the database
        if "jira_id" in project or "fogbugz_id" in project:
            id_field = ""
            if "jira_id" in project:
                id_field = "jira_id"
            elif "fogbugz_id" in project:
                id_field = "fogbugz_id"
            logger.info("before updating project %s" % project[id_field])
            data = issues.get_issues(project[id_field])

            sprint = issues.get_sprint_data(project[id_field])
            issues_count_by_priority = create_issues_count_by_priority(data)
            case_status = create_case_status(data)
            sprint_case_status = create_case_status(data, sprint, project[id_field])
            burndown = create_burndown(data, sprint, project[id_field])
            issues_status = create_issues_status(data)
            project_progress = create_project_progress(issues_status)
            workload = create_workload(data, sprint, project[id_field])
            project_burndown = create_project_burndown(data, sprint, project[id_field])
            devload.append(get_global_workload(data, sprint, project[id_field]))
            global_issue_status[project["name"]] = issues_status
            logger.info("updated charts for project %s" % project[id_field])

            # if the project data for charts includes documents then iterate through them and update them based on the
            # values we just pulled from the database above
            if len(docs) != 0:
                logger.info("iterating through existing documents for project %s" % project[id_field])
                for doc in docs:
                    logger.debug('update.handle_update - looking at doc %s' % doc["_id"])
                    # update the data for the issue breakdown by priority chart
                    if str(doc["_id"]) == str(project[id_field]) + "_issues_count_by_priority":
                        new_doc = json.loads(json.dumps(issues_count_by_priority, default=date_handler))
                        new_doc["_id"] = doc["_id"]
                        new_doc["_rev"] = doc["_rev"]
                        save.insert(new_doc)
                    # update the data for the issue breakdown by status type and assignee chart
                    elif str(doc["_id"]) == str(project[id_field]) + "_case_status":
                        new_doc = json.loads(json.dumps(case_status, default=date_handler))
                        new_doc["_id"] = doc["_id"]
                        new_doc["_rev"] = doc["_rev"]
                        save.insert(new_doc)
                    # update the data for the current sprint burndown chart
                    elif str(doc["_id"]) == str(project[id_field]) + "_sprint_case_status":
                        new_doc = json.loads(json.dumps(sprint_case_status, default=date_handler))
                        new_doc["_id"] = doc["_id"]
                        new_doc["_rev"] = doc["_rev"]
                        save.insert(new_doc)
                    elif str(doc["_id"]) == str(project[id_field]) + "_burndown":
                        new_doc = json.loads(json.dumps(burndown, default=date_handler))
                        new_doc["_id"] = doc["_id"]
                        new_doc["_rev"] = doc["_rev"]
                        save.insert(new_doc)
                    # update the data for the overall project issue status chart
                    elif str(doc["_id"]) == str(project[id_field]) + "_issues_status":
                        new_doc = json.loads(json.dumps(issues_status, default=date_handler))
                        new_doc["_id"] = doc["_id"]
                        new_doc["_rev"] = doc["_rev"]
                        save.insert(new_doc)
                    # update the data for the project progress bar
                    elif str(doc["_id"]) == str(project[id_field]) + "_project_progress":
                        new_doc = json.loads(json.dumps(project_progress, default=date_handler))
                        new_doc["_id"] = doc["_id"]
                        new_doc["_rev"] = doc["_rev"]
                        save.insert(new_doc)
                    # update the data for the workloads chart (estimated story points by assignee)
                    elif str(doc["_id"]) == str(project[id_field]) + "_workload":
                        new_doc = json.loads(json.dumps(workload, default=date_handler))
                        new_doc["_id"] = doc["_id"]
                        new_doc["_rev"] = doc["_rev"]
                        save.insert(new_doc)
                    # update the data for the overall project burndown
                    elif str(doc["_id"]) == str(project[id_field]) + "_project_burndown":
                        new_doc = json.loads(json.dumps(project_burndown, default=date_handler))
                        new_doc["_id"] = doc["_id"]
                        new_doc["_rev"] = doc["_rev"]
                        save.insert(new_doc)
                    logger.info("added new doc, new doc id: %s" % str(doc["_id"]))
                logger.info("done iterating through existing documents for project %s" % project[id_field])

            # if the documents for this project do not already exist, then we will create a new document
            # in the database
            if not any(x["_id"] == (str(project[id_field]) + "_issues_count_by_priority") for x in docs):
                doc = json.loads(json.dumps(issues_count_by_priority, default=date_handler))
                doc["_id"] = str(project[id_field]) + "_issues_count_by_priority"
                save.insert(doc)
                docs.append(doc)
                logger.info("doc doesn't exist. adding: %s" % str(project[id_field]) + "_issues_count_by_priority")
                update_first_time = 1
            if not any(x["_id"] == (str(project[id_field]) + "_case_status") for x in docs):
                doc = json.loads(json.dumps(case_status, default=date_handler))
                doc["_id"] = str(project[id_field]) + "_case_status"
                save.insert(doc)
                docs.append(doc)
                logger.info("doc doesn't exist. adding: %s" % str(project[id_field]) + "_case_status")
                update_first_time = 1
            if not any(x["_id"] == (str(project[id_field]) + "_sprint_case_status") for x in docs):
                doc = json.loads(json.dumps(sprint_case_status, default=date_handler))
                doc["_id"] = str(project[id_field]) + "_sprint_case_status"
                save.insert(doc)
                docs.append(doc)
                logger.info("doc doesn't exist. adding: %s" % str(project[id_field]) + "_sprint_case_status")
                update_first_time = 1
            if not any(x["_id"] == (str(project[id_field]) + "_burndown") for x in docs):
                doc = json.loads(json.dumps(burndown, default=date_handler))
                doc["_id"] = str(project[id_field]) + "_burndown"
                save.insert(doc)
                docs.append(doc)
                logger.info("doc doesn't exist. adding: %s" % str(project[id_field]) + "_burndown")
                update_first_time = 1
            if not any(x["_id"] == (str(project[id_field]) + "_issues_status") for x in docs):
                doc = json.loads(json.dumps(issues_status, default=date_handler))
                doc["_id"] = str(project[id_field]) + "_issues_status"
                save.insert(doc)
                docs.append(doc)
                logger.info("doc doesn't exist. adding: %s" % str(project[id_field]) + "_issues_status")
                update_first_time = 1
            if not any(x["_id"] == (str(project[id_field]) + "_project_progress") for x in docs):
                doc = json.loads(json.dumps(project_progress, default=date_handler))
                doc["_id"] = str(project[id_field]) + "_project_progress"
                save.insert(doc)
                docs.append(doc)
                logger.info("doc doesn't exist. adding: %s" % str(project[id_field]) + "_project_progress")
                update_first_time = 1
            if not any(x["_id"] == (str(project[id_field]) + "_workload") for x in docs):
                doc = json.loads(json.dumps(workload, default=date_handler))
                doc["_id"] = str(project[id_field]) + "_workload"
                save.insert(doc)
                docs.append(doc)
                logger.info("doc doesn't exist. adding: %s" % str(project[id_field]) + "_workload")
                update_first_time = 1
            if not any(x["_id"] == (str(project[id_field]) + "_project_burndown") for x in docs):
                doc = json.loads(json.dumps(project_burndown, default=date_handler))
                doc["_id"] = str(project[id_field]) + "_project_burndown"
                save.insert(doc)
                docs.append(doc)
                logger.info("doc doesn't exist. adding: %s" % str(project[id_field]) + "_project_burndown")
                update_first_time = 1
            logger.info("after updating project %s" % project[id_field])

    workload1 = {}
    # for each set of workloads that exist, process and clean up data and store in workload1
    logger.info('begin processing and storing each set of workload data.')
    for item in devload:
        for key in item.keys():
            if str(key) in workload1.keys():
                workload1[str(key)] = workload1[str(key)] + item[key]
            else:
                workload1[str(key)] = item[key]
    logger.info('finished storing cleaned workloads in the workload dict.')

    lists = [[], []]  # stores assignees and their respective estimated issue workloads
    logger.debug('update.handle_update - workload1.keys: % s' % workload1.keys())
    for item in workload1.values():
        if item > 40:
            lists[0].append(40)
            lists[1].append(item - 40)
        else:
            lists[0].append(item)
            lists[1].append(0)
    # stores workloads and their respective sprints
    global_workload = {'categories': workload1.keys(), 'lists': lists}

    logger.info("before setting the global_workload doc")
    if not any(x["_id"] == ("global_workload") for x in docs):
        doc = json.loads(json.dumps(global_workload, default=date_handler))
        doc["_id"] = "global_workload"
        save.insert(doc)
        docs.append(doc)
        logger.info("add new global_workload, new doc id: %s" % str(doc["_id"]))
        update_first_time = 1
    else:
        logger.info("global_workload exists-- iterating through docs")
        for doc in docs:
            if str(doc["_id"]) == "global_workload":
                new_doc = json.loads(json.dumps(global_workload, default=date_handler))
                new_doc["_id"] = doc["_id"]
                new_doc["_rev"] = doc["_rev"]
                save.insert(new_doc)
    logger.info("after setting the global_workload doc")

    # lists to store separated lists of issues by their status
    open_list = []
    progress = []
    closed = []
    logger.info("start separating global_issues_status values into respective status lists")
    for key in global_issue_status.keys():
        open_list.append(global_issue_status[key]["open"])
        closed.append(global_issue_status[key]["closed"])
        progress.append(global_issue_status[key]["progress"])
    logger.info("done updating global_issues_status into respective status lists")

    # stores each team member's open/closed/in progress breakdown
    global_issue_status_project = {
        'categories': [str(x) for x in global_issue_status.keys()],
        'open': open_list,
        'closed': closed,
        'progress': progress
    }

    logger.info("begin saving global_issues_status")
    if not any(x["_id"] == ("global_issue_status_project") for x in docs):
        doc = json.loads(json.dumps(global_issue_status_project, default=date_handler))
        doc["_id"] = "global_issue_status_project"
        save.insert(doc)
        docs.append(doc)
        logger.info("global_issue_status_project does not exist yet: %s" % doc)
        update_first_time = 1
    else:
        logger.info("global_issue_status_project exists-- iterating through docs")
        for doc in docs:
            logger.info("doc: %s" % doc)
            if str(doc["_id"]) == "global_issue_status_project":
                new_doc = json.loads(json.dumps(global_issue_status_project, default=date_handler))
                new_doc["_id"] = doc["_id"]
                new_doc["_rev"] = doc["_rev"]
                save.insert(new_doc)
    logger.info("done saving global_issues_status")
    logger.info("returning %s to start.py: " % update_first_time)
    return update_first_time
