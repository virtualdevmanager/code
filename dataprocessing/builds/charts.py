import sched, time
import couchdb
import json
from dateutil.parser import parse
from datetime import timedelta
import numpy as np
import datetime
import json
import logging

formatter = logging.Formatter(fmt='%(asctime)s-%(levelname)s-%(module)s-%(message)s')
handler = logging.FileHandler('/vagrant/dataprocessing/logs/builds-charts.log')
handler.setFormatter(formatter)
logger = logging.getLogger('buildsCharts')
logger.setLevel(logging.INFO)
logger.addHandler(handler)

DATE_RANGE = 7


def create_today_builds(data):
    sorted_builds = []
    builds = []
    try:
        for item in data:
            build = dict()
            build["projectName"] = item["projectName"]
            build["completed"] = parse(item["buildCompletedTime"])
            build["status"] = item["buildState"]
            build["number"] = item["buildNumber"]
            build["date"] = parse(item["buildCompletedTime"])
            builds.append(dict(build))
        sorted_builds = sorted(builds, key=lambda k: k['completed'])
        sorted_builds = sorted_builds[:10][::-1]
    except Exception as e:
        logger.error('builds.charts.create_today_builds - %s' % e)
    return sorted_builds


def create_builds_status(builds):
    try:
        logger.info('builds.charts.create_builds_status start')
        end_date = parse(str(datetime.datetime.now()))
        start_date = end_date - timedelta(days=DATE_RANGE)
        delta = end_date - start_date
        # a list of dates in the range
        categories = [(str((start_date + timedelta(days=i)).month) + "/" + str(
            (start_date + timedelta(days=i)).day) + "/" + str((start_date + timedelta(days=i)).year)) for i in
                      range(delta.days + 1)]
        num_days = len(categories)

        num_builds_succeeded_by_day = [0] * num_days
        num_builds_failed_by_day = [0] * num_days

        logger.info("builds.charts.create_builds_status the number of builds:%s" % str(len(builds)))
        for build in builds:
            if build["buildCompletedTime"] is not None:
                logger.info("check one build: %s" % build["key"])
                build_date = parse(str(build["buildCompletedTime"]))
                build_date = str(build_date.month) + "/" + str(build_date.day) + "/" + str(build_date.year)
                if build_date in categories:
                    logger.info("build: %s is in the range" % build["key"])
                    position = categories.index(build_date)
                    if "successful" in build and build["successful"]:
                        num_builds_succeeded_by_day[position] += 1
                    elif "successful" in build and not build["successful"]:
                        num_builds_failed_by_day[position] += 1

        np.set_printoptions(suppress=True)
        data = {
            'x_axis_title': "Date",
            'y_axis_title': "Number of Builds",
            'categories': categories,
            'successful': num_builds_succeeded_by_day,
            'failed': num_builds_failed_by_day
        }
        return data

    except Exception as e:
        logger.error("dataprocessing/builds/charts.py create_builds_status - %s" % e)
        return []
