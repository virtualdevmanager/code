import sched, time
import couchdb
import json
from dateutil.parser import parse
from datetime import timedelta
import numpy as np
import datetime
import json
import settings
import logging

formatter = logging.Formatter(fmt='%(asctime)s-%(levelname)s-%(module)s-%(message)s')
handler = logging.FileHandler('/vagrant/dataprocessing/builds/models.log')
handler.setFormatter(formatter)
logger = logging.getLogger('buildsModels')
logger.setLevel(logging.INFO)
logger.addHandler(handler)


class Build:
    def __init__(self):
        self.builds = []
        try:
            couch = couchdb.Server(settings.COUCH_DB_URL)
            self.builds = couch['builds']
        except Exception as e:
            logging.error(e)

    def get_builds(self, project_name):
        return [self.builds[x] for x in self.builds if x.startswith(project_name) and project_name != ""]

    def get_all_builds(self):
        return [self.builds[x] for x in self.builds]


class Save:
    def __init__(self):
        try:
            couch = couchdb.Server(settings.COUCH_DB_URL)
            self.db = couch['projects_data']
        except Exception as e:
            logging.error(e)

    def insert(self, doc):
        logging.info('inserting into database doc: %s' % doc)
        self.db.save(doc)

    def delete(self, doc):
        logging.info('deleting from database doc: %s' % doc)
        self.db.delete(doc)

    def get(self):
        logging.info('getting all projects data documents')
        return [self.db[x] for x in self.db]


class Project:
    def __init__(self):
        """Initializes the save DAO to save, remove, or return relevant project-related data from couchdb"""
        try:
            couch = couchdb.Server(settings.COUCH_DB_URL)
            self.db = couch['project_info']
            logger.debug('models.Project.__init__ - initializing Project()')
        except Exception as e:
            logger.error('models.Project.__init__ - %s' % str(e))

    def get(self):
        logging.info('getting project info')
        return [self.db[x] for x in self.db]

    def get_db(self):
        logging.info('getting db')
        return self.db
