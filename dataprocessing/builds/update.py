import sched, time
import couchdb
import json
from dateutil.parser import parse
from datetime import timedelta
import numpy as np
import datetime
import json
from .models import *
from .charts import *
import logging

formatter = logging.Formatter(fmt='%(asctime)s-%(levelname)s-%(module)s-%(message)s')
handler = logging.FileHandler('/vagrant/dataprocessing/builds/processing.log')
handler.setFormatter(formatter)
logger = logging.getLogger('buildsProcessing')
logger.setLevel(logging.DEBUG)
logger.addHandler(handler)
logger.info("level:" + str(logger.getEffectiveLevel()))

def date_handler(obj):
    return obj.isoformat() if hasattr(obj, 'isoformat') else obj


def handle_update():
    builds = Build()
    save = Save()
    docs = save.get()
    projects = Project().get()  # getting all the configured project information

    for project in projects:
        if "bamboo_id" in project:
            logger.info("update.handle_update - looking at project %s" % project['bamboo_id'])
            data = builds.get_builds(project['bamboo_id'])
            builds_status = create_builds_status(data)
            if len(docs) != 0:
                for doc in docs:
                    logger.debug('update.handle_update - looking at doc %s' % doc["_id"])
                    # update the data for the issue breakdown by priority chart
                    if str(doc["_id"]) == str(project["bamboo_id"]) + "_builds_status":
                        new_doc = json.loads(json.dumps(builds_status, default=date_handler))
                        new_doc["_id"] = doc["_id"]
                        new_doc["_rev"] = doc["_rev"]
                        save.insert(new_doc)

            if not any(x["_id"] == (str(project["bamboo_id"]) + "_builds_status") for x in docs):
                doc = json.loads(json.dumps(builds_status, default=date_handler))
                doc["_id"] = str(project["bamboo_id"]) + "_builds_status"
                save.insert(doc)
                docs.append(doc)
