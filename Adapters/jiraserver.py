import json
import logging
import time
import traceback
from threading import Thread

import couchdb
import requests
from flask import Flask
from flask_restful import Api, Resource, reqparse
from jira import JIRA
from jira.exceptions import JIRAError
from requests.exceptions import ConnectionError
from datetime import datetime
from dateutil import tz
from dateutil.parser import parse

formatter = logging.Formatter(fmt='%(asctime)s-%(levelname)s-%(module)s-%(message)s')
handler = logging.FileHandler('/var/tmp/jiraserver.log')
handler.setFormatter(formatter)
logger = logging.getLogger('jiraserver')
logger.setLevel(logging.DEBUG)
logger.addHandler(handler)

PORT = "5656"
SCHEDULAR_PORT = "5689"
CONNECTOR = "jira"

projkey_sprintkey = {}
projkey_issuekey = {}
couch = couchdb.Server()
app = Flask(__name__)
api = Api(app)
timeStamp = ""

from_zone = tz.tzutc()  # JIRA Server timezone: assume UTC
to_zone = tz.tzlocal()  # local timezone

to_do_statuses = []  # stores all statuses related to the TO DO status category
done_statuses = []  # stores all statuses related to the DONE status category
in_progress_statuses = []  # stores all statuses related to the IN PROGRESS status category


def heartbeat():
    """Periodically emits a heartbeat to indicate that this connector is still alive."""
    while True:
        try:
            payload = {'status': 'alive', 'port': PORT, 'connector': CONNECTOR}
            requests.post("http://localhost:" + SCHEDULAR_PORT + "/healthcheck", data=payload)
            logger.info('sending heartbeat')
            time.sleep(30)
        except Exception, e:
            logger.error('jiraserver.heartbeat - %s' % str(e))


t = Thread(target=heartbeat)
t.start()


def get_boards(jira, startdate, username, password, host):
    """Retrieves all boards and their respective spring data for a given JIRA project
    :param jira: JIRA object, object representing the JIRA connection.
    :param startdate: str, represents the date from which we start pulling data.
    :param username: str, the username of the JIRA login.
    :param password: str, the respective password of the JIRA user.
    :param host: str, the JIRA server host.
    """
    try:
        logger.info("getting all the boards...")
        boards = jira.boards()
        logger.debug("jiraserver.get_boards - all boards: %s" % boards)

        for board in boards:
            logger.info("pulling sprints for board_id: %s" % board.id)
            logger.debug("jiraserver.get_boards - sprints: %s" % jira.sprints(board.id))

            for sprint in jira.sprints(board.id):
                logger.debug("jiraserver.get_boards - getting data for sprint_id: %s" % sprint.id)
                spinfo = jira.sprint_info(board.id, sprint.id)
                sprintdict = {
                    "start_date": spinfo.get('startDate'),
                    "end_date": spinfo.get('endDate')
                }
                get_issues_in_sprint(sprint.id, sprint.name, sprintdict, startdate, username, password, host)
        logger.info("finished getting all the boards")
    except Exception as e:
        logger.error('jiraserver.get_boards - %s' % e)


def get_project_ids_in_JIRA(host, username, password):
    """ return a list of project ids that a user has under this account in JIRA server
    :param host: the host of JIRA server
    :param username: the username used to log in to the JIRA server
    :param password: the password used to log in to the JIRA server
    :return: a list of project ids that the user has under this account in JIRA server
    """
    project_ids = []
    try:
        all_projects_json_blob = requests.get(host + '/rest/api/2/project/', auth=(username, password))
        decoded_projects = json.loads(all_projects_json_blob.text)
        for project in decoded_projects:
            id = project.get('key')
            project_ids.append(id)
    except Exception as e:
        logger.error('jiraserver.get_all_project_keys - %s' % str(e))

    return project_ids


def load_all_statuses(host, username, password):
    """Retrieves all the statuses and appends them to their appropriate category list
    :param username: str, the username of the JIRA login.
    :param password: str, the respective password of the JIRA user.
    :param host: str, the JIRA server host.
    """
    try:
        logger.info('Getting all project statuses')
        # Sample JSON from the REST API:
        # {
        #    "self":"<host>/rest/api/2/status/10000",
        #    "description":"",
        #    "iconUrl":"<host>/images/icons/status_generic.gif",
        #    "name":"<status-name",
        #    "id":"<status-unique-identifier>",
        #    "statusCategory":{
        #       "self":"<host>/rest/api/2/statuscategory/2",
        #       "id":<status-category-id>,
        #       "key":"<status-category-key>",
        #       "colorName":"<status-category-label-color>",
        #       "name":"<status-category-name>"
        #    }
        #  },
        status_json_blob = requests.get(host + '/rest/api/2/status/', auth=(username, password))
        decoded_statuses = json.loads(status_json_blob.text)
        logger.info('Done getting all project statuses')
        logger.info('Normalizing statuses...')
        # Going through each status that we got back from the server
        for status in decoded_statuses:
            status_category_id = status.get('statusCategory').get("id")
            status_name = status.get('name')
            status_name += host
            # Add the status name to the correct status category list
            if status_category_id == 2:
                to_do_statuses.append(status_name)
            elif status_category_id == 4:
                in_progress_statuses.append(status_name)
            elif status_category_id == 3:
                done_statuses.append(status_name)
        logger.info('Done normalizing statuses...')
    except Exception as e:
        logger.error('jiraserver.load_all_statuses - %s' % str(e))


def get_status_category(issue_fields, host):
    """Gets the status category for a given issue's status based on the category lists we populated at setup
    :param issue_fields: dict, the fields associated with the issue.
    :rtype string
    :return the status category name
    """
    try:
        issue_status_name = issue_fields.get('status').get('name')
        issue_status_name += host
        if issue_fields.get('status') is not None and issue_status_name in to_do_statuses:
            status = "Open"
        elif issue_fields.get('status') is not None and issue_status_name in done_statuses:
            status = "Closed"
        elif issue_fields.get('status') is not None and issue_status_name in in_progress_statuses:
            status = "In Progress"
        return status
    except Exception as e:
        logger.error('jiraserver.get_status_category - %s' % str(e))


def get_issues_in_sprint(sprint_id, sprint_key, sprint_dict, startdate, username, password, host):
    """Retrieves all the issues in a given sprint
    :param sprint_id: int, unique identifier for a given sprint.
    :param sprint_key: str, the name of the sprint.
    :param sprint_dict: dict, dictionary including sprint start and end dates.
    :param startdate: str, represents the date from which we start pulling data.
    :param username: str, the username of the JIRA login.
    :param password: str, the respective password of the JIRA user.
    :param host: str, the JIRA server host.
    """
    # get project ids from couchdb['project_info']
    project_ids = get_added_project_ids()

    logger.info("getting all issues in sprint %s" % sprint_id)
    if startdate == '' or startdate is None or len(startdate) == 0:
        r = requests.get(host + '/rest/agile/1.0/sprint/' + str(sprint_id) + '/issue',
                         auth=(username, password))
    else:
        r = requests.get(host + '/rest/agile/1.0/sprint/' + str(sprint_id)
                         + '/issue?jql=updated >= "' + startdate + '"', auth=(username, password))
    data = json.loads(r.text)
    logger.debug('jiraserver.get_issues_in_sprint - got all sprint data')

    logger.info('iterating through issues')
    # Iterate through all issues and pull relevant fields to store in the issue dictionary
    for issue in data.get('issues'):
        assign = ''
        status = ''

        fields = issue.get('fields')
        # filter out issues that from the projects which do not exist in project_info database
        if fields.get('project') is None:
            # this issue does not belong to any project
            continue
        else:
            cur_project_id = fields.get('project').get('key')
            if cur_project_id not in project_ids:
                # this issue is not from the desired projects
                continue

        # Forcing the flow: To Do -> Open Done -> Closed by getting the relevant status category
        status = get_status_category(fields, host)

        if fields.get('assignee') is not None:
            assign = fields.get('assignee').get('name')

        if fields.get('timespent') is not None:
            logged_time = int(fields.get('timespent')) / 3600.0
        else:
            logged_time = 0

        if fields.get('customfield_10008') is not None:
            estimate = int(fields.get('customfield_10008'))
        elif fields.get('timeoriginalestimate') is not None:
            estimate = int(fields.get('timeoriginalestimate')) / 3600.0
        else:
            estimate = None

        # convert JIRA server time (UTC) to local timestamp in a string format
        if fields.get('resolutiondate') is not None:
            resolve_date = parse(fields.get('resolutiondate')).replace(tzinfo=from_zone).astimezone(to_zone).strftime(
                "%Y-%m-%d %H:%M:%S")
        else:
            resolve_date = None
        # convert JIRA server time (UTC) to local timestamp in a string format
        if fields.get('created') is not None:
            creation_date = parse(fields.get('created')).replace(tzinfo=from_zone).astimezone(to_zone).strftime(
                "%Y-%m-%d %H:%M:%S")
        else:
            creation_date = None
        # convert JIRA server time (UTC) to local timestamp in a string format
        if fields.get('duedate') is not None:
            due_date = parse(fields.get('duedate')).replace(tzinfo=from_zone).astimezone(to_zone).strftime(
                "%Y-%m-%d %H:%M:%S")
        else:
            due_date = None

        issue_dict = {
            "summary": fields.get('summary'),
            "creation_date": creation_date,
            "assignee": assign,
            "status": status,
            "sprint": sprint_key,
            "estimate": estimate,
            "logged": logged_time,
            "priority": fields.get('priority').get('name'),
            "due": due_date,
            "resolve_date": resolve_date
        }
        # Save the sprint and issue information to be saved to the database later
        if fields.get('project') is not None:
            projkey_sprintkey[fields.get('project').get('key') + "_" + sprint_key] = sprint_dict
            # save_dict_to_db('sprints', sprint_dict, fields.get('project').get('key') + "_" + sprint_key)
            projkey_issuekey[fields.get('project').get('key') + "_" + issue.get("key")] = issue_dict
            # save_dict_to_db('issues', issue_dict, fields.get('project').get('key') + "_" + issue.get("key"))
    logger.info('finished loading sprint issues')
    return


def save_dict_to_db(db_name, data, doc_key):
    """Saves data to the given database
    :param db_name: str, name of the database we want to save to.
    :param data: dict, the dictionary containing data we are saving to the db.
    :param doc_key: the key of the doc
    """
    try:
        db = couch[db_name]
        if doc_key in db:
            doc = db.get(doc_key)
            new_doc = json.loads(json.dumps(data))
            new_doc["_id"] = doc["_id"]
            new_doc["_rev"] = doc["_rev"]
            db.save(new_doc)
        else:
            new_doc = json.loads(json.dumps(data))
            new_doc['_id'] = doc_key
            db.save(new_doc)
    except Exception as e:
        logger.error('jiraserver.save_dict_to_db - %s' % e)


def save_to_db(db_name, data):
    """
    Saves data to the given database
    :param db_name: str, name of the database we want to save to.
    :param data: dict, the dictionary containing data we are saving to the db.
    """
    try:
        db = couch[db_name]
        # Iterating through the project data and saving them in their raw format in the proper couchdb
        for key, value in data.iteritems():
            if key in db:
                doc = db.get(key)
                new_doc = json.loads(json.dumps(value))
                new_doc["_id"] = doc["_id"]
                new_doc["_rev"] = doc["_rev"]
                db.save(new_doc)
            else:
                new_doc = json.loads(json.dumps(value))
                new_doc['_id'] = key
                db.save(new_doc)
    except Exception as e:
        logger.error('jiraserver.save_to_db - %s' % e)


def get_added_project_ids():
    """ Get all the project ids stored in the "project_info" database
    :rtype: a list
    :return: a list of project ids in the application
    """
    ids = []
    try:
        db = couch['project_info']
        for item in db:
            project_info = db[item]
            if project_info.has_key('jira_id'):
                ids.append(db[item]['jira_id'])
    except Exception as e:
        logger.error('jiraserver.get_project_ids - %s' % e)

    return ids


def get_update_date(host, username, password):
    """Gets the last update date for the JIRA server
    :param host: str, host of the JIRA server.
    :param username: dict, the dictionary containing data we are saving to the db.
    :param password:
    rype: str
    Return Type: the last updated date for a given JIRA server
    """
    try:
        logger.info('getting update date')
        url = host + "/rest/api/2/serverInfo";
        serve_info = requests.get(url, auth=(username, password))
        data = json.loads(serve_info.text)
        time = data['serverTime'].split("T");
        year = time[0]
        hour_min = ":".join(time[1].split(":")[:2])
        logger.info('done getting update date')
        return year + " " + hour_min
    except Exception as e:
        logger.error('jiraserver.get_update_date - %s ' % e)
    return ""


def cleanse_host(host):
    """This function cleans up and canonicalizes the raw host string returned from the external API
    :param host: str, the host the JIRA server is on.
    rype: str
    Return Type: the normalized host string
    """
    try:
        logger.debug('jiraserver.cleanse_host - host before: %s' % host)
        if (host[len(host) - 1]) == '/':  # Removes the trailing /
            host = host[:len(host) - 1]
        logger.debug('jiraserver.cleanse_host - host after: %s' % host)
    except Exception as e:
        logger.error('jiraserver.cleanse_host - %s' % e)

    return host


def get_all_data(jira, starttime, username, password, host):
    """Pull the data from the JIRA server and save to the appropriate database
    :param jira: JIRA object, the JIRA object.
    :param starttime: str, the start time that we want to start pulling data from.
    :param username: str, the username for the JIRA server we are pulling from.
    :param password: str, the password for the respective user.
    :param host: str, the host the JIRA server lives on.
    rype: str
    Return Type: the last date the
    """
    logger.info('getting all data...')
    load_all_statuses(host, username, password)
    last_update_time = get_update_date(host, username, password)
    get_boards(jira, starttime, username, password, host)
    # Pull all the sprints and issues for all boards living on the JIRA server and store the fields we care about
    # in the database.

    if bool(projkey_sprintkey):
        save_to_db("sprints", projkey_sprintkey)
    if bool(projkey_issuekey):
        save_to_db("issues", projkey_issuekey)
    logger.info('done getting all data')

    return last_update_time


class Auth(Resource):
    def __init__(self):
        """Used to authenticate the user."""
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('username', type=str, required=True, help='No username provided')
        self.reqparse.add_argument('password', type=str, required=True, help='No password provided')
        self.reqparse.add_argument('host', type=str, required=True, help='No server provided')
        self.reqparse.add_argument('pname', type=str, required=True, help='No name provided')
        super(Auth, self).__init__()

    def post(self):
        args = self.reqparse.parse_args()
        if (args['username'] == '' or args['password'] == '' or args['pname'] == '' or args['host'] == '' or
                    args['username'] is None or args['password'] is None or args['pname'] is None or
                    args['host'] is None):
            return {"message": {"status": "Username, password, connector name, host are required"}}, 200
        else:
            # Try to add a new project to the JIRA dictionary with the credentials provided
            try:
                host = cleanse_host(args['host'])
                JIRA(host, basic_auth=(args['username'], args['password']))
            except JIRAError:
                logger.error('jiraserver.Auth.post %s' % str(JIRAError))
                return {"message": {"status": "Unauthorized access"}}, 200
            except ConnectionError:
                logger.error('jiraserver.Auth.post %s' % str(ConnectionError))
                return {"message": {"status": "Host name does not exist"}}, 200
            return {"message": {"status": "success"}}, 200


class Cancel(Resource):
    def __init__(self):
        """Used to cancel a project that is currently being tracked."""
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('pname', type=str, required=True, help='No name provided')
        super(Cancel, self).__init__()

    def post(self):
        args = self.reqparse.parse_args()

        if args['pname'] == '' or args['pname'] is None:
            logger.error('jiraserver.Cancel.get - no Project name provided')
            return {"message": {"status": "No project name provided"}}, 200
        else:
            return {"message": {"status": "success"}}, 200


class UpdateData(Resource):
    def __init__(self):
        """Used to update the data for a given a project."""
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('pname', required=True, help="no name is provided", type=str)
        self.reqparse.add_argument('lastUpdate', type=str)
        self.reqparse.add_argument('username', type=str)
        self.reqparse.add_argument('password', type=str)
        self.reqparse.add_argument('host', type=str)
        super(UpdateData, self).__init__()

    def post(self):
        args = self.reqparse.parse_args()
        host = cleanse_host(args['host'])

        # get len of the db('project_info') first
        # if len == 0, do not pull the data
        try:
            doc_cnt = len(couch['project_info'])
            if doc_cnt == 0:
                logger.info("jiraserver.UpdateData.post - does not pull data as no project is added to the vdm")
                return {"message": {"status": "success"}}, 200
        except Exception as e:
            logger.error("jiraserver.UpdateData.get - read db error")

        try:
            logger.info("fetching data")
            jira_connection = JIRA(host, basic_auth=(args['username'], args['password']))
            time_got_all_data = get_all_data(jira_connection, args['lastUpdate'], args['username'],
                                             args['password'], host)
            return {"message": {"status": "success", "time": time_got_all_data}}, 200

        except Exception, e:
            logger.error("jiraserver.UpdateData.post - could not fetch data:%s" % e)
            traceback.print_exc()
            return {"message": {"status": "Unable to connect it:" + e}}, 200


class CheckIfExist(Resource):
    def __init__(self):
        """Used to update the data for a given a project."""
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('pname', required=True, help="no name is provided", type=str)
        self.reqparse.add_argument('key', required=True, help="no project key is provided", type=str)
        self.reqparse.add_argument('username', required=True, help="no username is provided", type=str)
        self.reqparse.add_argument('password', required=True, help="no password is provided", type=str)
        self.reqparse.add_argument('host', required=True, help="no host is provided", type=str)
        super(CheckIfExist, self).__init__()

    def post(self):
        args = self.reqparse.parse_args()
        host = cleanse_host(args['host'])
        project_key = args['key']
        logger.debug("made it into the post jiraserver request")

        try:
            jira_connection = JIRA(host, basic_auth=(args['username'], args['password']))
            projects = jira_connection.projects()

            for project in projects:
                if project_key == str(project):
                    return {"message": "True"}, 200

            return {"message": "False"}, 200


        except Exception as e:
            logger.error("jiraserver.CheckIfExist.post - %s" % e)
            traceback.print_exc()
            return {"message": {"status": "Unable to connect it:" + e}}, 200


api.add_resource(Auth, '/auth')
api.add_resource(UpdateData, '/updateData')
api.add_resource(Cancel, '/cancel')
api.add_resource(CheckIfExist, '/checkIfExist')

if __name__ == '__main__':
    app.run(debug=False, threaded=False, port=PORT, host='localhost')
