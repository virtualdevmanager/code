import json
import logging
import pytz
import time
import traceback
from threading import Thread
from requests.exceptions import ConnectionError
from datetime import datetime, timedelta
from dateutil import tz
from dateutil.parser import parse

import couchdb
import requests
from flask import Flask
from flask_restful import Api, Resource, reqparse

from fogbugz import FogBugz, FogBugzConnectionError
from uritemplate import expand

app = Flask(__name__)
api = Api(app)



formatter = logging.Formatter(fmt='%(asctime)s-%(levelname)s-%(module)s-%(message)s')
handler = logging.FileHandler('/var/tmp/fogbugz.log')
handler.setFormatter(formatter)
logger = logging.getLogger('fogbugz')
logger.setLevel(logging.DEBUG)
logger.addHandler(handler)

CONNECTOR = "fogbugz"
PORT = "5659"
SCHEDULAR_PORT = "5689"
MAX_PULLING_DAYS = 20
couch = couchdb.Server()
from_zone = tz.tzutc()  #
to_zone = tz.tzlocal()  # local timezone

def heartbeat():
    """Periodically emits a heartbeat to indicate that this connector is still alive."""
    while True:
        try:
            payload = {'status': 'alive', 'port': PORT, 'connector': CONNECTOR}
            requests.post("http://localhost:" + SCHEDULAR_PORT + "/healthcheck", data=payload)
            logger.info('sending heartbeat')
            time.sleep(30)
        except Exception, e:
            logger.error('fogbugz.heartbeat - %s' % str(e))

# Start the heartbeat process, the heartbeat is picked up by VDMmonitor
t = Thread(target=heartbeat)
t.start()



class FogBugzClient:
    fb = None
    person_cache = {}

    def __init__(self, url, username, password):

        try:
            self.fb = FogBugz(url)
            self.fb.logon(username, password)
        except (FogBugzConnectionError, AttributeError,) as fbce:
            logger.error(str(fbce))


    def test_connection(self):
        return self.fb

    def get_person(self, ixperson):
        if ixperson is None or int(ixperson) == 0:
            return None

        if ixperson not in self.person_cache:
            response = self.fb.viewPerson(ixperson=ixperson)
            self.person_cache[ixperson] = response.person

        return self.person_cache[ixperson]

    def get_projects(self):
        return self.fb.listProjects()

    def get_fixfors(self):
        return self.fb.listFixFors()

    def search(self, q="", cols=""):
        return self.fb.search(q=q, cols=cols)


def get_date_from_iso(iso_string):
    # -- 2013-10-03T04:00:00Z
    if iso_string:
        return datetime.strptime(iso_string, "%Y-%m-%dT%H:%M:%SZ")
    return None


def get_all_milestones(fb_connection, project_ids):
    """Retrieves all boards and their respective spring data for a given JIRA project
    :param jira: JIRA object, object representing the JIRA connection.
    :param startdate: str, represents the date from which we start pulling data.
    :param username: str, the username of the JIRA login.
    :param password: str, the respective password of the JIRA user.
    :param host: str, the JIRA server host.
    """

    projkey_sprintkey = {}

    try:
        #logger.info("getting all the boards...")
        milestones = fb_connection.get_fixfors()
        #logger.debug("jiraserver.get_boards - all boards: %s" % boards)
        logger.info("Got all milestones")
        for milestone in milestones.findAll("fixfor"):
            #print milestone.find('dt')
            if milestone.find('sproject').text.strip() in project_ids \
                    and milestone.find('dtstart') is not None\
                    and milestone.find('dt') is not None:
            #logger.debug("jiraserver.get_boards - getting data for sprint_id: %s" % sprint.id)
                start = get_date_from_iso(milestone.find('dtstart').text.strip())
                end = get_date_from_iso(milestone.find('dt').text.strip())


                if start is not None and end is not None:
                    sprintdict = {
                        "start_date": start.replace(tzinfo=from_zone).astimezone(to_zone).strftime("%Y-%m-%d %H:%M:%S"),
                        "end_date": end.replace(tzinfo=from_zone).astimezone(to_zone).strftime("%Y-%m-%d %H:%M:%S")
                    }
                    #print milestone.ixFixFor.string
                    projkey_sprintkey[milestone.find('sproject').text.strip() + "_" + milestone.find('ixfixfor').text.strip()] = sprintdict
                    #print projkey_sprintkey
            #get_issues_in_sprint(sprint.id, sprint.name, sprintdict, startdate, username, password, host)
        #print projkey_sprintkey
        if bool(projkey_sprintkey):
            logger.info("saving project sprint")
            save_to_db("sprints", projkey_sprintkey)

    except Exception as e:
        #logger.error('jiraserver.get_boards - %s' % e)
        print e


def get_all_issues(fb_connection, project_ids):
    """Retrieves all boards and their respective spring data for a given JIRA project
    :param jira: JIRA object, object representing the JIRA connection.
    :param startdate: str, represents the date from which we start pulling data.
    :param username: str, the username of the JIRA login.
    :param password: str, the respective password of the JIRA user.
    :param host: str, the JIRA server host.
    """

    projkey_issuekey = {}

    try:
        logger.info("getting all the issues...")
        caselist = fb_connection.search(cols="ixBug,ixPriority,sTitle,ixFixFor,ixProject,sStatus,sPersonAssignedTo,dtResolved,dtOpened,dtClosed,sPriority,hrsCurrEst,sProject")
        #logger.debug("jiraserver.get_boards - all boards: %s" % boards)
        logger.info("Got all cases")
        for case in caselist.findAll("case"):
            # print case.find('dtOpened').text.strip()
            # print not case.find('dtResolved').text.strip()
            if case.find('sproject').text.strip() in project_ids:
                if len(case.find('dtopened').text.strip()) > 0:
                    open_date = get_date_from_iso(case.find('dtopened').text.strip()).replace(tzinfo=from_zone).astimezone(to_zone).strftime(
                    "%Y-%m-%d %H:%M:%S")
                else:
                    open_date = None

                if len(case.find('dtresolved').text.strip()):
                    resolve_date = get_date_from_iso(case.find('dtresolved').text.strip()).replace(tzinfo=from_zone).astimezone(to_zone).strftime(
                    "%Y-%m-%d %H:%M:%S")
                elif len(case.find('dtclosed').text.strip()):
                    resolve_date = get_date_from_iso(case.find('dtclosed').text.strip()).replace(tzinfo=from_zone).astimezone(
                        to_zone).strftime("%Y-%m-%d %H:%M:%S")
                else:
                    resolve_date = None

                issue_dict = {
                    "summary": case.find('stitle').text.strip(),
                    "creation_date": open_date,
                    "assignee": case.find('spersonassignedto').text.strip(),
                    "status": case.find('sstatus').text.strip(),
                    "sprint": case.find('ixfixfor').text.strip(),
                    "estimate": float(case.find('hrscurrest').text.strip()),
                    "priority": case.find('spriority').text.strip(),
                    "resolve_date": resolve_date
                }
                logger.info("saved one case")
                projkey_issuekey[case.find('sproject').text.strip() + "_" + case.find('ixbug').text.strip()] = issue_dict
        #print projkey_issuekey
        if bool(projkey_issuekey):
            logger.info("saving project issues")
            save_to_db("issues", projkey_issuekey)
    except Exception as e:
        #logger.error('jiraserver.get_boards - %s' % e)
        print e


def get_added_project_ids():
    """ Get all the project ids stored in the "project_info" database
    :rtype: a list
    :return: a list of project ids in the application
    """
    ids = []
    try:
        db = couch['project_info']
        for item in db:
            project_info = db[item]
            if project_info.has_key('fogbugz_id'):
                ids.append(db[item]['fogbugz_id'])
        logger.info("Retrieved all project ids from database")
    except Exception as e:
        logger.error('fogbugz_id.get_project_ids - %s' % e)

    return ids


def check_project_exist(fogbugz_connection, project_name):
    """ Check if the project is exit in the remote bitbucket server
    :param project_name: Project to be checked (project slug).
    :param bitbucket_connection: Clinet,the bitbucket connection.
    :rtype: boolean
    :return: if project exist on remote, return true, else return false
    """

    try:
        logger.info("starting checking project")

        projects = fogbugz_connection.get_projects()
        for p in projects.findAll("project"):
            if p.find('sproject').text.strip() == project_name:
                return True
        logger.info("finished checking project")
        return False
    except Exception as e:
        flag = False
        logger.error('project doesnt exist - %s' % e)

    return flag


def save_to_db(db_name, data):
    """
    Saves data to the given database
    :param db_name: str, name of the database we want to save to.
    :param data: dict, the dictionary containing data we are saving to the db.
    """
    try:
        db = couch[db_name]
        # Iterating through the project data and saving them in their raw format in the proper couchdb
        for key, value in data.iteritems():
            if key in db:
                doc = db.get(key)
                new_doc = json.loads(json.dumps(value))
                new_doc["_id"] = doc["_id"]
                new_doc["_rev"] = doc["_rev"]
                db.save(new_doc)
            else:
                new_doc = json.loads(json.dumps(value))
                new_doc['_id'] = key
                db.save(new_doc)
    except Exception as e:
        logger.error('fogbugz.save_to_db - %s' % e)


class Auth(Resource):
    def __init__(self):
        """Used to authenticate the user."""
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('username', type=str, required=True, help='No username provided')
        self.reqparse.add_argument('password', type=str, required=True, help='No password provided')
        self.reqparse.add_argument('pname', type=str, required=True, help='No project name provided')
        self.reqparse.add_argument('host', type=str, required=True, help='No host name provided')
        super(Auth, self).__init__()

    def post(self):
        args = self.reqparse.parse_args()
        if (args['username'] == '' or args['password'] == '' or args['pname'] == '' or args['host'] == '' or
                    args['username'] is None or args['password'] is None
                    or args['pname'] is None or args['host'] is None):
            return {"message": {"status": "Username, password, project name, host are required"}}, 200
        else:
            # Try to add a new project to the bitbucket dictionary with the credentials provided
            try:
                FogBugzClient(args['host'], args['username'], args['password']).test_connection()
            except (FogBugzConnectionError, AttributeError,) as fbce:
                logger.info(fbce)
                return {"message": {"status": "Unauthorized access"}}, 200
            except ConnectionError:
                logger.info("error")
                return {"message": {"status": "Unauthorized access"}}, 200
            return {"message": {"status": "success"}}, 200


class UpdateData(Resource):
    def __init__(self):
        """Used to authenticate the user."""
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('username', type=str, required=True, help='No username provided')
        self.reqparse.add_argument('password', type=str, required=True, help='No password provided')
        self.reqparse.add_argument('pname', type=str, required=True, help='No project name provided')
        self.reqparse.add_argument('host', type=str, required=True, help='No host name provided')
        super(UpdateData, self).__init__()

    def post(self):
        args = self.reqparse.parse_args()
        try:
            doc_cnt = len(couch['project_info'])
            if doc_cnt == 0:
                logger.info("fogbugz.UpdateData.get - does not pull data as no project is added to the vdm")
                return {"message": {"status": "success"}}, 200
        except Exception as e:
            logger.error("fogbugz.UpdateData.get - read db error")

        logger.info("Kicking off update data")


        try:
            logger.info("fetching data")
            project_ids = get_added_project_ids()
            fogbugz_connection = FogBugzClient(args['host'], args['username'], args['password'])
            get_all_issues(fogbugz_connection, project_ids)
            get_all_milestones(fogbugz_connection, project_ids)
            return {"message": {"status": "success", "time": 0}}, 200

        except Exception, e:
            logger.error("fogbugz.UpdateData.get - could not fetch data:%s" % e)
            traceback.print_exc()
            return {"message": {"status": "Unable to connect it:" + e}}, 200


class Cancel(Resource):
    def __init__(self):
        """Used to cancel a project that is currently being tracked."""
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('pname', type=str)
        super(Cancel, self).__init__()

    def post(self):
        args = self.reqparse.parse_args()

        if args['pname'] == '' or args['pname'] is None:
            logger.error('fogbugz.Cancel.get - no Project name provided')
            return {"message": {"status": "No project name provided"}}, 200
        else:
            return {"message": {"status": "success"}}, 200


class CheckIfExist(Resource):
    def __init__(self):
        """Used to check the project trying to add is existing on server."""
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('username', type=str, required=True, help='No username provided')
        self.reqparse.add_argument('password', type=str, required=True, help='No password provided')
        self.reqparse.add_argument('key', type=str, required=True, help='No key name provided')
        self.reqparse.add_argument('host', type=str, required=True, help='No host name provided')
        super(CheckIfExist, self).__init__()

    def post(self):
        args = self.reqparse.parse_args()
        if (args['username'] == '' or args['password'] == '' or args['key'] == '' or args['host'] == '' or
                    args['username'] is None or args['password'] is None
                    or args['key'] is None or args['host'] is None):
            return {"message": {"status": "Username, password, project name, host are required"}}, 200
        else:
            # Try to check the new project is existing on bitbucket remote server
            try:
                check_result = check_project_exist(
                    FogBugzClient(args['host'], args['username'], args['password']), args['key'])
                if check_result is True:
                    return {"message": "True"}, 200
                else:
                    return {"message": "False"}, 200

            except (FogBugzConnectionError, AttributeError,) as fbce:
                return {"message": {"status": "Unauthorized access"}}, 200
            except ConnectionError:
                return {"message": {"status": "Unauthorized access"}}, 200


api.add_resource(Auth, '/auth')
api.add_resource(Cancel, '/cancel')
api.add_resource(UpdateData, '/updateData')
api.add_resource(CheckIfExist, '/checkIfExist')

if __name__ == '__main__':
    app.run(debug=False, threaded=False, port=PORT, host='localhost')
