import json
import time
import logging
from threading import Thread

import couchdb
import requests
from bamboo_api import BambooAPIClient
from flask import Flask
from flask_restful import Api, Resource, reqparse
from requests.exceptions import ConnectionError
from dateutil import tz
from dateutil.parser import parse


couch = couchdb.Server()
app = Flask(__name__)  # Initializing app variable to newly created flask instance object
api = Api(app)

formatter = logging.Formatter(fmt='%(asctime)s-%(levelname)s-%(module)s-%(message)s')
handler = logging.FileHandler('/var/tmp/bamboo.log')
handler.setFormatter(formatter)
logger = logging.getLogger('bamboo')
logger.setLevel(logging.DEBUG)
logger.addHandler(handler)

bambooDict = {}     # maps projects to their bamboo instance objects
PORT = "5657"
SCHEDULAR_PORT = "5689"
CONNECTOR = "bamboo"

from_zone = tz.tzutc()  # JIRA Server timezone: assume UTC
to_zone = tz.tzlocal()  # local timezone

def heartbeat():
    """Periodically emits a heartbeat to indicate that this connector is still alive."""
    while True:
        try:
            payload = {'status': 'alive', 'port': PORT, 'connector': CONNECTOR}
            requests.post("http://localhost:" + SCHEDULAR_PORT + "/healthcheck", data=payload)
            time.sleep(30)
        except Exception, e:
            logger.error('bamboo.heartbeat - %s' % str(e))


t = Thread(target=heartbeat)
t.start()


def get_data(bamboo_connector, host):
    """Retrieves all builds data from the Bamboo server and stores their details into the database
    :param bamboo_connector: Bamboo object, object representing the Bamboo connection.
    :param host: str, represents the host of the Bamboo server we want to pull data from.
    """
    # pulls raw data for all the latest builds from each plan from the Bamboo server
    # raw content have build details expanded in descending order. this is turned into a clean JSON blob

    if host.endswith('/'):
        host = host[:-1]

    ids = ["VDM-VB"] #get_added_project_ids()
    logger.info('project ids stored in db')
    logger.info(ids)
    logger.info('grabbing all builds from bamboo connector')
    raw_content = bamboo_connector._get_response(host + "/rest/api/latest/result.json?expand=results.result")._content
    content = json.loads(raw_content)
    logger.info('finished creating json from bamboo build data')
    projname_resultkey = {}  # stores and maps every build to their respective details
    # for each latest build detail, grab the raw content for its respective plan and store it in a json blob
    logger.info('iterating through all the latest builds')
    for latest_build in content.get("results").get('result'):
        if str(latest_build.get("plan").get("planKey").get("key")) in ids:
            logger.debug('bamboo.get_data - getting all builds for plan %s' % latest_build.get("plan").get("planKey"))
            raw_content = bamboo_connector._get_response(
                host + "/rest/api/latest/result/" + latest_build.get("plan").get("planKey").get(
                    "key") + ".json?expand=results.result")._content

            content = json.loads(raw_content)

            # go through each build in this plan and save the details into an element object (projname_resultkey value)
            for build in content.get("results").get('result'):
                element = {"link_href": build.get('link').get('href'),
                           "plan": {"shortName": build.get("plan").get("shortName"),
                                    "shortKey": build.get("plan").get("shortKey"),
                                    "link_href": build.get("plan").get("link").get('href'),
                                    "key": build.get("plan").get("key"),
                                    "name": build.get("plan").get("name"),
                                    "planKey": build.get("plan").get("planKey").get("key")
                                    },
                           "planName": build.get("planName"),
                           "projectName": build.get("projectName"),
                           "buildResultKey": build.get("buildResultKey"),
                           "lifeCycleState": build.get("lifeCycleState"),
                           "id": build.get("id"),
                           "buildStartedTime": parse(build.get("buildStartedTime")).replace(tzinfo=from_zone)
                               .astimezone(to_zone).strftime("%Y-%m-%d %H:%M:%S"),
                           "prettyBuildStartedTime": build.get("prettyBuildStartedTime"),
                           "buildCompletedTime": parse(build.get("buildCompletedTime")).replace(tzinfo=from_zone)
                               .astimezone(to_zone).strftime("%Y-%m-%d %H:%M:%S"),
                           "buildCompletedDate": parse(build.get("buildCompletedDate")).replace(tzinfo=from_zone)
                               .astimezone(to_zone).strftime("%Y-%m-%d %H:%M:%S"),
                           "prettyBuildCompletedTime": build.get("prettyBuildCompletedTime"),
                           "buildDurationInSeconds": build.get("buildDurationInSeconds"),
                           "buildDuration": build.get("buildDuration"),
                           "buildDurationDescription": build.get("buildDurationDescription"),
                           "buildRelativeTime": build.get("buildRelativeTime"),
                           "buildTestSummary": build.get("buildTestSummary"),
                           "successfulTestCount": build.get("successfulTestCount"),
                           "failedTestCount": build.get("failedTestCount"),
                           "quarantinedTestCount": build.get("quarantinedTestCount"),
                           "skippedTestCount": build.get("skippedTestCount"),
                           "continuable": build.get("continuable"),
                           "restartable": build.get("restartable"),
                           "notRunYet": build.get("notRunYet"),
                           "finished": build.get("finished"),
                           "successful": build.get("successful"),
                           "buildReason": build.get("buildReason"),
                           "reasonSummary": build.get("reasonSummary"),
                           "comments_size": build.get("comments").get("size"),
                           "planResultKey": build.get("planResultKey").get("key"),
                           "key": build.get("key"),
                           "state": build.get("state"),
                           "buildState": build.get("buildState"),
                           "number": build.get("number"),
                           "buildNumber": build.get("buildNumber"),
                           }
                # save build name to its details object

                logging.info('saving build: %s' % build.get("buildResultKey"))
                projname_resultkey[build.get("buildResultKey")] = element
            # print projname_resultkey
    # saving all the builds and their data to the "builds" database
    if bool(projname_resultkey):
        save_to_db("builds", projname_resultkey)


def authenticate(host, user, passwd, baseurl='/rest/api/latest/'):
    if host.endswith('/'):
        host = host[:-1]
    request_url = host + baseurl
    request = requests.get(request_url, auth=(user, passwd))
    return request.status_code


def save_to_db(db_name, data):
    """
    Saves data to the given database
    :param db_name: str, name of the database we want to save to.
    :param data: dict, the dictionary containing data we are saving to the db.
    """
    try:
        db = couch[db_name]
        # Iterating through the project data and saving them in their raw format in the proper couchdb
        for key, value in data.iteritems():
            if key in db:
                # Since commits don't change once its committed, we skip the ones that is already stored into DB
                continue
            else:
                new_doc = json.loads(json.dumps(value))
                new_doc['_id'] = key
                db.save(new_doc)
    except Exception as e:
        logger.error('bamboo.save_to_db - %s' % e)


def get_added_project_ids():
    """ Get all the project ids stored in the "project_info" database
    :rtype: a list
    :return: a list of project ids in the application
    """
    ids = []
    try:
        db = couch['project_info']
        for item in db:
            project_info = db[item]
            if project_info.has_key('bamboo_id'):
                ids.append(db[item]['bamboo_id'])
        logger.info("Retrieved all project ids from database")
    except Exception as e:
        logger.error('bamboo.get_project_ids - %s' % e)

    return ids


def check_project_exist(username, password, host, key):
    """ Check if the project is exit in the remote bamboo server
    :param username: username.
    :param password: password.
    :param host: remote server url.
    :param key: project key.
    :rtype: boolean
    :return: if project exist on remote, return true, else return false
    """

    flag = False
    try:
        logger.info("starting checking project")
        if host.endswith('/'):
            host = host[:-1]
        bamboo_client = BambooAPIClient(host=host, user=username, password=password)
        raw_content = bamboo_client._get_response(host + "/rest/api/latest/plan")._content
        content = json.loads(raw_content)
        for plan in content.get("plans").get('plan'):
            if str(plan.get("planKey").get("key")) == key:
                return True
        logger.info("finished checking project")
    except Exception as e:
        flag = False
        logger.error('project doesnt exist - %s' % e)

    return flag


class Auth(Resource):
    def __init__(self):
        """Used to authenticate the user."""
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('username', type=str, required=True, help='No username provided')
        self.reqparse.add_argument('password', type=str, required=True, help='No password provided')
        self.reqparse.add_argument('host', type=str, required=True, help='No server provided')
        self.reqparse.add_argument('pname', type=str, required=True, help='No name provided')
        super(Auth, self).__init__()

    def post(self):
        args = self.reqparse.parse_args()
        if (args['username'] == '' or args['password'] == '' or args['pname'] == '' or args['host'] == '' or args[
            'username'] is None or args['password'] is None or args['pname'] is None or args['host'] is None):
            return {"message": {"status": "Username, password, connector name, host are required"}}, 200
        else:
            # Try to add a new project to the Bamboo dictionary with the credentials provided
            try:
                status_code = authenticate(args['host'], args['username'], args['password'])
                if status_code is not 200:
                    return {"message": {"status": "Unauthorized access"}}, 200
            except ConnectionError:
                logger.error('bamboo.Auth.get %s' % str(ConnectionError))
                return {"message": {"status": "Host name does not exist"}}, 200
            except Exception, e:
                logger.error('bamboo.Auth.get %s' % str(e))
                return {"message": {"status": "Unauthorized access"}}, 200
            return {"message": {"status": "success"}}, 200


class Cancel(Resource):
    def __init__(self):
        """Used to cancel a project that is currently being tracked."""
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('pname', type=str)
        super(Cancel, self).__init__()

    def post(self):
        args = self.reqparse.parse_args()

        if args['pname'] == '' or args['pname'] is None:
            logger.error('bamboo.Cancel.get - no project name provided')
            return {"message": {"status": "No project name provided"}}, 200
        else:
            return {"message": {"status": "success"}}, 200


class UpdateData(Resource):
    def __init__(self):
        """Used to update the data for a given a project."""
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('pname', required=True, help="no name is provided", type=str)
        self.reqparse.add_argument('username', type=str)
        self.reqparse.add_argument('password', type=str)
        self.reqparse.add_argument('host', type=str)

        super(UpdateData, self).__init__()

    def post(self):
        args = self.reqparse.parse_args()

        try:
            doc_cnt = len(couch['project_info'])
            if doc_cnt == 0:
                logger.info("bamboo.UpdateData.post - does not pull data as no project is added to the vdm")
                return {"message": {"status": "success"}}, 200
        except Exception as e:
            logger.error("bamboo.UpdateData.post - read db error")

        try:
            if args['host'].endswith('/'):
                args['host'] = args['host'][:-1]
            get_data(BambooAPIClient(host=args['host'], user=args['username'], password=args['password']),
                     args['host'])
            return {"message": {"status": "update bamboo project data"}}, 200
        except Exception, e:
            logger.error("bamboo.UpdateData.get - connection error. %s" % str(e))
            return {"message": {"status": "Unable to connect to bamboo server"}}, 200


class CheckIfExist(Resource):
    def __init__(self):
        """Used to check the project trying to add is existing on server."""
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('username', type=str, required=True, help='No username provided')
        self.reqparse.add_argument('password', type=str, required=True, help='No password provided')
        self.reqparse.add_argument('key', type=str, required=True, help='No key name provided')
        self.reqparse.add_argument('host', type=str, required=True, help='No host name provided')
        super(CheckIfExist, self).__init__()

    def post(self):
        args = self.reqparse.parse_args()
        if (args['username'] == '' or args['password'] == '' or args['key'] == '' or args['host'] == '' or
                    args['username'] is None or args['password'] is None
                    or args['key'] is None or args['host'] is None):
            return {"message": {"status": "Username, password, project name, host are required"}}, 200
        else:
            # Try to check the new project is existing on bitbucket remote server
            try:
                check_result = check_project_exist(args['username'], args['password'], args['host'], args['key'])
                if (check_result is True):
                    return {"message": "True"}, 200
                else:
                    return {"message": "False"}, 200
            except ConnectionError:
                return {"message": {"status": "Unauthorized access"}}, 200


api.add_resource(Auth, '/auth')                 # route /auth method to Auth resource
api.add_resource(UpdateData, '/updateData')     # route /updateData method to UpdateData resource
api.add_resource(Cancel, '/cancel')             # route /cancel method to Cancel resource
api.add_resource(CheckIfExist, '/checkIfExist')

if __name__ == '__main__':
    app.run(debug=False, threaded=True, port=PORT, host='localhost')
