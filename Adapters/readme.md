All connectors must contain:
------------------------------
- POST /auth
	- required inputs: 
		- username
    	- password
    	- host
       	- pname

- POST /cancel
	- required inputs:
      	- pname

- POST /updateData
	- required inputs: 
		- username
    	- password
   		- host
       	- pname

    - optional inputs:
        - lastUpdate

- POST /checkIfExist
    - required inputs:
		- username
    	- password
   		- host
       	- pname
       	- key



Integration
------------------------------------------
After fetching data, the connect must save into couchdb(port 5984). If it is for issue tracker, it should save into issues. If it is build servers, int should save to builds.
 

- All connectors must send heartbeat to monitor every 30 seconds 
	- post request should send to localhost:5689/healthcheck
	- body should be json format which contains following information: 
		'status': (alive or die), 
		'port': PORT (the port the connector is running), 
		'connector': name (the connector name which should be same as service name)

- Move new connector in  /Vagrant/Adapters/ 
- Add new connector in /Vagrant/Monitor/setup/connectors.json, providing connector name(same as service name which i will mention later), port which is running on, and type (issue trackers, build servers or code repository)

- Use the service template in /Adapters/Vagrant/setup folder and update $DAEMON, $ARGS and $PID

- Modify vagrant file in the main folder
  - go to connectors section ##connectors
  - add following lines:

         sudo cp setup/YourConnector /etc/init.d/
     
         sudo chmod +x /etc/init.d/YourConnector

    you need to replace YourConnecnot with the connector name
