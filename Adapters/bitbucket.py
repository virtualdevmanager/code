import json
import logging
import pytz
import time
import traceback
from threading import Thread
from requests.exceptions import ConnectionError
from datetime import datetime, timedelta
from dateutil import tz
from dateutil.parser import parse

import couchdb
import requests
from flask import Flask
from flask_restful import Api, Resource, reqparse

from pybitbucket import bitbucket

from pybitbucket.auth import BasicAuthenticator
from pybitbucket.team import Team, TeamRole
from pybitbucket.repository import Repository
from pybitbucket.commit import Commit
from uritemplate import expand

app = Flask(__name__)
api = Api(app)



formatter = logging.Formatter(fmt='%(asctime)s-%(levelname)s-%(module)s-%(message)s')
handler = logging.FileHandler('/var/tmp/bitbucket.log')
handler.setFormatter(formatter)
logger = logging.getLogger('bitbucket')
logger.setLevel(logging.DEBUG)
logger.addHandler(handler)

CONNECTOR = "bitbucket"
PORT = "5658"
SCHEDULAR_PORT = "5689"
MAX_PULLING_DAYS = 20
couch = couchdb.Server()
from_zone = tz.tzutc()  #
to_zone = tz.tzlocal()  # local timezone

def heartbeat():
    """Periodically emits a heartbeat to indicate that this connector is still alive."""
    while True:
        try:
            payload = {'status': 'alive', 'port': PORT, 'connector': CONNECTOR}
            requests.post("http://localhost:" + SCHEDULAR_PORT + "/healthcheck", data=payload)
            logger.info('sending heartbeat')
            time.sleep(30)
        except Exception, e:
            logger.error('bitbucket.heartbeat - %s' % str(e))

# Start the heartbeat process, the heartbeat is picked up by VDMmonitor
t = Thread(target=heartbeat)
t.start()

def save_to_db(db_name, data):
    """
    Saves data to the given database
    :param db_name: str, name of the database we want to save to.
    :param data: dict, the dictionary containing data we are saving to the db.
    """
    try:
        db = couch[db_name]
        # Iterating through the project data and saving them in their raw format in the proper couchdb
        for key, value in data.iteritems():
            if key in db:
                # Since commits don't change once its committed, we skip the ones that is already stored into DB
                continue
            else:
                new_doc = json.loads(json.dumps(value))
                new_doc['_id'] = key
                db.save(new_doc)
    except Exception as e:
        logger.error('bitbucket.save_to_db - %s' % e)


class Bitbucket():
    def __init__(self, username, password, host):
        """
            Initialize Bitbucket connection with given parameter
            :param username: str, username of bitbucket account
            :param password: str, password of bitbucket account
            :param host: str, host of the bitbucket server
        """
        self.client = bitbucket.Client(
            BasicAuthenticator(username, password, "", server_base_uri=host)
        )
        try:
            response = self.client.session.get(self.client.get_bitbucket_url())

            # If we get response other than 200, it means either the server is bad or credential is failing
            if not response.status_code == 200:
                raise ConnectionError
        except ConnectionError:
            raise ConnectionError

    def get(self):
        """
            Returns Bitbucket connection client back to the caller
        """
        return self.client


def fetch_diff(bitbucket_connection, commit):
    """
        Fetch the diff of a specific commit in the bitbucket repository
        :param bitbucket_connection: client, bitbucket client connection
        :param commit: str, the hash of the commit
    """
    try:
        response = bitbucket_connection.session.get(commit.links["diff"]["href"]).content
        return response
    except ConnectionError:
        raise ConnectionError


def get_all_data(bitbucket_connection):
    """Retrieves all the issues in a given sprint
        :param bitbucket_connection: Clinet,the bitbucket connection.
        """
    # get project ids from couchdb['project_info']
    project_ids = get_added_project_ids()

    project_commits = {}
    logger.info("Getting all project related data")

    for fullname in project_ids:
        team_full_name = fullname.split('_')

        logger.info("Making rest api calls for commits")
        # Making a rest api call to pull all commits related to given repository
        commits = Commit.find_commits_in_repository(
            team_full_name[0], team_full_name[1],
            branch=None,
            include=None,
            exclude=None,
            client=bitbucket_connection)

        for commit in commits:

            # Calculating the days difference between current date and commit date
            diff = fetch_diff(bitbucket_connection, commit)
            today = datetime.utcnow().replace(tzinfo=pytz.utc)
            date = parse(commit.date)

            # If we are pulling date more than we needed, we break the loop
            if (today - date) > timedelta(days=MAX_PULLING_DAYS):
                logger.info("The data pulling is more than " + str(MAX_PULLING_DAYS) + " days old, break now")
                break

            # Initialize the database element to be stored in to DB
            # We only store the commit that is not merge
            # Some of the commit doesn't have username field in commit.author, we will skip that commit
            # This could caused by account setting on bitbucket server
            if commit.type == u"commit" and len(commit.parents) < 2:
                commit_element = {
                    "date": date.replace(tzinfo=from_zone).astimezone(to_zone).strftime("%Y-%m-%d %H:%M:%S"),
                    "author": commit.author.username,
                    "diff": diff,
                    "comment": commit.message
                }

                # We are only storing first 7 chars of each commit hash value as the key of the doc
                project_commits[
                    team_full_name[0] + "_" + team_full_name[1] + "_" + commit.hash[:7]] = commit_element

    if bool(project_commits):
        logger.info("Saving all retrieved commits to db")
        save_to_db("commits", project_commits)

def get_added_project_ids():
    """ Get all the project ids stored in the "project_info" database
    :rtype: a list
    :return: a list of project ids in the application
    """
    ids = []
    try:
        db = couch['project_info']
        for item in db:
            project_info = db[item]
            if project_info.has_key('bitbucket_id'):
                ids.append(db[item]['bitbucket_id'])
        logger.info("Retrieved all project ids from database")
    except Exception as e:
        logger.error('bitbucket.get_project_ids - %s' % e)

    return ids


def check_project_exist(bitbucket_connection, project_name):
    """ Check if the project is exit in the remote bitbucket server
    :param project_name: Project to be checked (project slug).
    :param bitbucket_connection: Clinet,the bitbucket connection.
    :rtype: boolean
    :return: if project exist on remote, return true, else return false
    """

    flag = True
    try:
        logger.info("starting checking project")
        # Replacing the first underscore with slash (This is passed from views.py in main)
        project_name = project_name.replace('_', '/', 1)
        repo = Repository.find_repository_by_full_name(project_name,
                                                       client=bitbucket_connection)
        logger.info("finished checking project")
    except Exception as e:
        flag = False
        logger.error('project doesnt exist - %s' % e)

    return flag


class Auth(Resource):
    def __init__(self):
        """Used to authenticate the user."""
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('username', type=str, required=True, help='No username provided')
        self.reqparse.add_argument('password', type=str, required=True, help='No password provided')
        self.reqparse.add_argument('pname', type=str, required=True, help='No project name provided')
        self.reqparse.add_argument('host', type=str, required=True, help='No host name provided')
        super(Auth, self).__init__()

    def post(self):
        args = self.reqparse.parse_args()
        if (args['username'] == '' or args['password'] == '' or args['pname'] == '' or args['host'] == '' or
                    args['username'] is None or args['password'] is None
                    or args['pname'] is None or args['host'] is None):
            return {"message": {"status": "Username, password, project name, host are required"}}, 200
        else:
            # Try to add a new project to the bitbucket dictionary with the credentials provided
            try:
                Bitbucket(args['username'], args['password'], args['host']).get()
            except ConnectionError:
                return {"message": {"status": "Unauthorized access"}}, 200
            return {"message": {"status": "success"}}, 200


class UpdateData(Resource):
    def __init__(self):
        """Used to authenticate the user."""
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('username', type=str, required=True, help='No username provided')
        self.reqparse.add_argument('password', type=str, required=True, help='No password provided')
        self.reqparse.add_argument('pname', type=str, required=True, help='No project name provided')
        self.reqparse.add_argument('host', type=str, required=True, help='No host name provided')
        super(UpdateData, self).__init__()

    def post(self):
        args = self.reqparse.parse_args()
        try:
            doc_cnt = len(couch['project_info'])
            if doc_cnt == 0:
                logger.info("bitbucket.UpdateData.get - does not pull data as no project is added to the vdm")
                return {"message": {"status": "success"}}, 200
        except Exception as e:
            logger.error("bitbucket.UpdateData.get - read db error")

        logger.info("Kicking off update data")


        try:
            logger.info("fetching data")
            bitbucket_connection = Bitbucket(args['username'], args['password'], args['host']).get()
            get_all_data(bitbucket_connection)
            return {"message": {"status": "success", "time": 0}}, 200

        except Exception, e:
            logger.error("bitbucket.UpdateData.get - could not fetch data:%s" % e)
            traceback.print_exc()
            return {"message": {"status": "Unable to connect it:" + e}}, 200



class Cancel(Resource):
    def __init__(self):
        """Used to cancel a project that is currently being tracked."""
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('pname', type=str)
        super(Cancel, self).__init__()

    def post(self):
        args = self.reqparse.parse_args()

        if args['pname'] == '' or args['pname'] is None:
            logger.error('bitbucket.Cancel.get - no Project name provided')
            return {"message": {"status": "No project name provided"}}, 200
        else:
            return {"message": {"status": "success"}}, 200


class CheckIfExist(Resource):
    def __init__(self):
        """Used to check the project trying to add is existing on server."""
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('username', type=str, required=True, help='No username provided')
        self.reqparse.add_argument('password', type=str, required=True, help='No password provided')
        self.reqparse.add_argument('key', type=str, required=True, help='No key name provided')
        self.reqparse.add_argument('host', type=str, required=True, help='No host name provided')
        super(CheckIfExist, self).__init__()

    def post(self):
        args = self.reqparse.parse_args()
        if (args['username'] == '' or args['password'] == '' or args['key'] == '' or args['host'] == '' or
                    args['username'] is None or args['password'] is None
                    or args['key'] is None or args['host'] is None):
            return {"message": {"status": "Username, password, project name, host are required"}}, 200
        else:
            # Try to check the new project is existing on bitbucket remote server
            try:
                check_result = check_project_exist(
                    Bitbucket(args['username'], args['password'], args['host']).get(), args['key'])
                if (check_result is True):
                    return {"message": "True"}, 200
                else:
                    return {"message": "False"}, 200
            except ConnectionError:
                return {"message": {"status": "Unauthorized access"}}, 200


api.add_resource(Auth, '/auth')
api.add_resource(Cancel, '/cancel')
api.add_resource(UpdateData, '/updateData')
api.add_resource(CheckIfExist, '/checkIfExist')

if __name__ == '__main__':
    app.run(debug=False, threaded=False, port=PORT, host='localhost')
