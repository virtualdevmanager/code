import sys
import os
import unittest

sys.path.insert(0, os.path.abspath('..'))  # Path hack to access sibling packages
from dataprocessing.commits.models import Commit
import os.path

if not os.path.exists("vagrant/vdm/main/logs"):
    os.makedirs("vagrant/vdm/main/logs")
from dataprocessing.commits import charts
from vdm.codeRepo import views
import settings
import couchdb
from dateutil.parser import parse
import datetime
from datetime import timedelta

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
couch = couchdb.Server(settings.COUCH_DB_URL)

DATE_RANGE = 14


def get_categories():
    end_date = parse(str(datetime.datetime.now()))
    start_date = end_date - timedelta(days=DATE_RANGE)
    delta = end_date - start_date
    # a list of dates in the range
    categories = [(str((start_date + timedelta(days=i)).month) + "/" + str(
        (start_date + timedelta(days=i)).day) + "/" + str((start_date + timedelta(days=i)).year)) for i in
                  range(delta.days + 1)]
    return categories


repo_activity = {
    "_id": "virtualdevmanager_code_repo_activity",
    "_rev": "4-da2340eaf8bcfae2bb0c0d406be7b4f7",
    "num": [0, 11, 4, 4, 9, 5, 9, 2, 6, 1, 5, 38, 0, 0, 0],
    "y_axis_title": "Number of Commits",
    "x_axis_title": "Date",
    "categories":
        ["7/15/2017", "7/16/2017", "7/17/2017", "7/18/2017", "7/19/2017", "7/20/2017", "7/21/2017", "7/22/2017",
         "7/23/2017", "7/24/2017", "7/25/2017", "7/26/2017", "7/27/2017", "7/28/2017", "7/29/2017"]
}

modification_activity = {
    "_id": "virtualdevmanager_code_modification_activity", "_rev": "6-5b9f4b37fcc527812b5c75fb345bc49c",
    "added": [0, 986, 204, 143, 119, 129, 995, 56, 423, 40, 302, 11824, 0, 0, 942],
    "deleted": [0, 309, 902, 24, 80, 82, 303, 103, 135, 21, 62, 750, 0, 0, 256],
    "y_axis_title": "Lines of Code",
    "x_axis_title": "Date",
    "categories": ["7/15/2017", "7/16/2017", "7/17/2017", "7/18/2017", "7/19/2017", "7/20/2017", "7/21/2017",
                   "7/22/2017", "7/23/2017", "7/24/2017", "7/25/2017", "7/26/2017", "7/27/2017", "7/28/2017",
                   "7/29/2017"]
}

repo_activity_per_member = {
    "_id": "virtualdevmanager_code_activity_per",
    "_rev": "7-f635773cbdbcdaf510d6ffcc5e670716",
    "x_axis_values": ["lwu613", "hyh19910105", "xiaoleip"],
    "y_axis_values": [57, 17, 21],
    "y_axis_title": "Number of Commits",
    "x_axis_title": "Team Members"
}

modification_activity_per_member = {
    "_id": "virtualdevmanager_code_modification_activity_per",
    "_rev": "9-3530a18e417abe8c2ac2c6737bc31e22",
    "added": [2026, 1094, 13043],
    "deleted": [1717, 320, 990],
    "y_axis_title": "Lines of Code",
    "x_axis_title": "Team Members",
    "categories": ["lwu613", "hyh19910105", "xiaoleip"]
}


class TestCodeRepoView(unittest.TestCase):
    maxDiff = None

    # Setup method run before every test case
    @classmethod
    def setUpClass(cls):
        cls.commit = Commit()

    # Teardown method run before every test case
    @classmethod
    def tearDownClass(cls):
        couch.delete('projects_data')

    # Tests that the correct issues can be returned based on raw issues data stored
    def test_data_prime(self):
        global repo_activity
        global modification_activity
        global repo_activity_per_member
        global modification_activity_per_member

        if 'projects_data' in couch:
            db = couch['projects_data']
        else:
            db = couch.create('projects_data')  # Create test data

        db.save(repo_activity)
        db.save(modification_activity)
        db.save(repo_activity_per_member)
        db.save(modification_activity_per_member)

        data = views.get_commits("virtualdevmanager_code")
        expected_data = {
            'repo_activity': repo_activity,
            'modification_activity': modification_activity,
            'activity_per_user': repo_activity_per_member,
            'modification_per_user': modification_activity_per_member
        }
        self.assertEqual(data, expected_data)

    # Tests charts.create_repo_activity
    def test_create_repo_activity(self):
        cur_time = parse(str(datetime.datetime.now()))
        yesterday = cur_time - timedelta(days=1)
        out_of_range_date = cur_time - timedelta(days=(DATE_RANGE + 1))
        out_of_range_date_2 = cur_time + timedelta(days=1)

        input_data = [
            {
                "_id": "virtualdevmanager_code_f924da5",
                "_rev": "2-f4b2fa3c08abc130bbf5fabcd9ad7472",
                "comment": "VDM-420: fixed doesprojectexist route\n",
                "added": 105,
                "author": "lwu613",
                "deleted": 96,
                "date": str(out_of_range_date.year) + "-" + str(out_of_range_date.month) + "-" + str(
                    out_of_range_date.day) + " 04:58:32",
                "diff": "diff --git a/Adapters/jiraserver.py b/Adapters/jiraserver.py\nindex cad091b..93793c6 100644\n--- a/Adapters/jiraserver.py\n+++ b/Adapters/jiraserver.py\n@@ -514,6 +514,7 @@ class CheckIfExist(Resource):\n         args = self.reqparse.parse_args()\n         host = cleanse_host(args['host'])\n         project_key = args['key']\n+        logger.debug(\"made it into the post jiraserver request\")\n \n         if args['pname'] not in jiraDict.keys():\n             try:\ndiff --git a/Monitor/VDMmonitor.js b/Monitor/VDMmonitor.js\nindex 1e3e134..78ae98c 100644\n--- a/Monitor/VDMmonitor.js\n+++ b/Monitor/VDMmonitor.js\n@@ -575,104 +575,107 @@ app.post('/reschedule',function(req,res){\n \n });\n \n-// FIXME: testing out whether or not project exists\n-// Force update for the adapter information sent through the request body\n-// Parameters: pname\n-app.get('/doesProjectExist',function(req,res) {\n-    log.info(\">> req.body: \" + req.body);\n+app.post('/doesProjectExist',function(req,res) {\n+    log.info(\"VDMMonitor - received request for doesProjectExist: \" + req.body);\n     var adapter_name = req.body.adapter_name;   // pname parameter\n-    var proj_info_type = req.body.idType; // ie jira_id, bitbucket_id\n+    var proj_info_type = req.body.idType;       // ie jira_id, bitbucket_id\n     var project_info_param = req.body.project_info;\n \n     log.info(\">> got into the doesProjectExist route\");\n-    // TODO: check if we should be using .view or .list\n-    // db.list([params], [callback]) will also list all the docs \n     schedulardb.view(\"schedular\",\"by_pname\", function(err,body) {\n-        log.info(\">> got into schedulardb.view\");\n-        log.info(\">> body: \" + body);\n-        log.info(\">> body.rows: \" + body.rows)\n-//        if(!err) {\n-//            body.rows.forEach(function(doc){\n-//                if (doc.value.pname && adapter_name === doc.value.pname) {\n-//                    var port_number = doc.value.port;\n-//                    var user_name = doc.value.username;\n-//                    var password = decrypt(doc.value.password);\n-//                    var host = doc.value.host;\n-//                    var last_update = doc.value.last_update;\n-//\n-//                    /** If it's a JIRA adapter **/\n-//                    if(proj_info_type === 'jira_id') {\n-//                        var postData = querystring.stringify({\n-//                            'pname': adapter_name,\n-//                            'username': user_name,\n-//                            'password': password,\n-//                            'host': host,\n-//                            'last_update': doc.value.last_update,\n-//                            'key': project_info_param['jira_id']\n-//                        });\n-//\n-//                        log.info(\"POST path: http://localhost:\" + port_number + \"/checkIfExist\");\n-//\n-//                        var options = {\n-//                            host: \"localhost\",\n-//                            port: port_number,\n-//                            path: \"/checkIfExist\",\n-//                            method: 'POST',\n-//                            headers: {  'Content-Type': 'application/x-www-form-urlencoded',\n-//                                        'Content-Length': Buffer.byteLength(postData)\n-//                            }\n-//                        };\n-//                        var post_req = http.request(options, function(res) {\n-//                            res.send(res.body));\n-//                        });\n-//\n-//                        post_req.on('error', function (error) {\n-//                            res.end(\"project doesn't exist\");\n-//                            log.info(\"project doesn't exist:\" + error);\n-//                        });\n-//                        post_req.write(postData);\n-//                        post_req.end();\n-//                    }\n-//\n-//                    /** If it's a bitbucket adapter **/\n-//                    if(proj_info_type === 'bitbucket_id') {\n-//                        var postData = querystring.stringify({\n-//                                'pname': adapter_name,\n-//                                'username': user_name,\n-//                                'password': password,\n-//                                'host': host,\n-//                                'last_update': doc.value.last_update,\n-//                                'key': project_info_param['bitbucket_id']\n-//                        });\n-//\n-//                        log.info(\"POST path: http://localhost:\" + port_number + \"/checkIfExist\");\n-//\n-//                        var options = {\n-//                            host: \"localhost\",\n-//                            port: port_number,\n-//                            path: \"/checkIfExist\",\n-//                            method: 'POST',\n-//                            headers: {  'Content-Type': 'application/x-www-form-urlencoded',\n-//                                        'Content-Length': Buffer.byteLength(postData)\n-//                            }\n-//                        };\n-//                        var post_req = http.request(options, function(res) {\n-//                            res.send(res.body));\n-//                        });\n-//\n-//                        post_req.on('error', function (error) {\n-//                            res.end(\"project doesn't exist\");\n-//                            log.info(\"project doesn't exist:\" + error);\n-//                        });\n-//                        post_req.write(postData);\n-//                        post_req.end();\n-//                    }\n-//                }\n-//            });\n-//        }\n-//        else {\n-//            log.info(\"ERROR: Could not connect to db\");\n-//        }\n+        if(!err) {\n+            body.rows.forEach(function(doc) {\n+                log.info('VDMMonitor iterating through every project doc');\n+                log.info('VDMMonitor - doesProjectExist: got pname from db: ' + doc.value.pname);\n+                log.info('VDMMonitor - doesProjectExist: got pname from req: ' + adapter_name);\n+                if (doc.value.pname && adapter_name === doc.value.pname) {\n+                    var port_number = doc.value.port;\n+\n+                    /** If it's a JIRA adapter **/\n+                    if(proj_info_type === 'jira_id') {\n+                        var postData = querystring.stringify({\n+                            'pname': adapter_name,\n+                            'username': doc.value.username,\n+                            'password': decrypt(doc.value.password),\n+                            'host': doc.value.host,\n+                            'last_update': doc.value.last_update,\n+                            'key': project_info_param['jira_id']\n+                        });\n+\n+                        var options = {\n+                            host: \"localhost\",\n+                            port: port_number,\n+                            path: \"/checkIfExist\",\n+                            method: 'POST',\n+                            headers: {  'Content-Type': 'application/x-www-form-urlencoded',\n+                                        'Content-Length': Buffer.byteLength(postData)\n+                            }\n+                        };\n+\n+                        var post_req = http.request(options, function(response) {\n+                            log.info('Status: ' + response.statusCode);\n+                            response.setEncoding('utf8');\n+                            response.on('data', function (body) {\n+                                log.info('Body: ' + body);\n+                                res.send(body);\n+                            });\n+                        });\n+\n+                        post_req.on('error', function (error) {\n+                            res.end(\"project doesn't exist\");\n+                            log.info(\"VDMMonitor - project doesn't exist:\" + error);\n+                        });\n+\n+                        post_req.write(postData);\n+                        post_req.end();\n+                    }\n+                    // res.sendStatus(200);\n+\n+                    /** If it's a bitbucket adapter **/\n+                    else if(proj_info_type === 'bitbucket_id') {\n+                        var postData = querystring.stringify({\n+                            'pname': adapter_name,\n+                            'username': doc.value.username,\n+                            'password': decrypt(doc.value.password),\n+                            'host': doc.value.host,\n+                            'last_update': doc.value.last_update,\n+                            'key': project_info_param['jira_id']\n+                        });\n+\n+                        var options = {\n+                            host: \"localhost\",\n+                            port: port_number,\n+                            path: \"/checkIfExist\",\n+                            method: 'POST',\n+                            headers: {  'Content-Type': 'application/x-www-form-urlencoded',\n+                                        'Content-Length': Buffer.byteLength(postData)\n+                            }\n+                        };\n+\n+                        var post_req = http.request(options, function(response) {\n+                            log.info('Status: ' + response.statusCode);\n+                            response.setEncoding('utf8');\n+                            response.on('data', function (body) {\n+                                log.info('Body: ' + body);\n+                                res.send(body);\n+                            });\n+                        });\n+\n+                        post_req.on('error', function (error) {\n+                            res.end(\"project doesn't exist\");\n+                            log.info(\"VDMMonitor - project doesn't exist:\" + error);\n+                        });\n+\n+                        post_req.write(postData);\n+                        post_req.end();\n+                    }\n+                }\n+            });\n+        }\n+        else {\n+            res.sendStatus(500);\n+            log.info(\"ERROR: Could not connect to db\");\n+        }\n     });\n });\n \ndiff --git a/Vagrantfile b/Vagrantfile\nindex c1427a1..5f186fe 100644\n--- a/Vagrantfile\n+++ b/Vagrantfile\n@@ -21,6 +21,8 @@ Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|\n   # Add port-forward Sails.js application\n   config.vm.network \"forwarded_port\", guest: 8000, host: 8000\n   config.vm.network \"forwarded_port\", guest: 3000, host: 3000\n+  config.vm.network \"forwarded_port\", guest: 5689, host: 5689\n+  config.vm.network \"forwarded_port\", guest: 5656, host: 5656\n \n   # Create a private network, which allows host-only access to the machine\n   # using a specific IP.\ndiff --git a/vdm/main/views.py b/vdm/main/views.py\nindex 7af5778..4e5ea01 100644\n--- a/vdm/main/views.py\n+++ b/vdm/main/views.py\n@@ -207,6 +207,7 @@ def add_project(request):\n     :param request: HttpRequest object\n     :return: return a rendered page\n     \"\"\"\n+    monitor_port = 5689\n     try:\n         if request.method == 'GET':\n             # if the user requests to get the \"add_project.html\" but not to submit new project information\n@@ -236,11 +237,13 @@ def add_project(request):\n             if 'pname' in adapter_db[doc] and adapter_db[doc]['pname'] == project_info['adapter']:\n                 # check if project exists in the external server\n                 if 'jira_id' in project_info:\n-                    response = unirest.get(\"http://localhost:\" + 3000 + \"/doesProjectExist\",\n+                    path = \"http://localhost:\" + str(monitor_port) + \"/doesProjectExist\"\n+                    response = unirest.post(path,\n                                             headers={\"Content-Type\": \"application/x-www-form-urlencoded\"},\n                                             params={'adapter_name': adapter_db[doc]['pname'],\n                                                     'project_info': project_info,\n                                                     'idType': 'jira_id'})\n+                    logger.info(\"response.body: %s\" % response.body)\n                     result = response.body[\"message\"]\n                     logger.info(\"this is the result line 245: %s\" % result)\n \n@@ -249,7 +252,7 @@ def add_project(request):\n                         return redirect(reverse('project_configuration'))\n \n                 if 'bitbucket_id' in project_info:\n-                    response = unirest.get(\"http://localhost:\" + 3000 + \"/doesProjectExist\",\n+                    response = unirest.post(\"http://localhost:\" + str(monitor_port) + \"/doesProjectExist\",\n                                             headers={\"Content-Type\": \"application/x-www-form-urlencoded\"},\n                                             params={'adapter_name': adapter_db[doc]['pname'],\n                                                     'project_info': project_info,\n@@ -266,7 +269,7 @@ def add_project(request):\n                     messages.error(request, error_message)\n                     return redirect(reverse('project_configuration'))\n \n-                # Async API request to correspond adapter\n+                # Async API request to corresponding adapter\n             #     test = unirest.post(\"http://localhost:\" + 3000 + \"/forceUpdate\",\n             #                  headers={\"Content-Type\": \"application/x-www-form-urlencoded\"},\n             #                  params={'pname': result.pname,\n"
            },
            {
                "_id": "virtualdevmanager_code_f924da5",
                "_rev": "2-f4b2fa3c08abc130bbf5fabcd9ad7473",
                "comment": "VDM-420: fixed doesprojectexist route\n",
                "added": 105,
                "author": "lwu613",
                "deleted": 96,
                "date": str(out_of_range_date_2.year) + "-" + str(out_of_range_date_2.month) + "-" + str(
                    out_of_range_date_2.day) + " 05:58:32",
                "diff": "diff --git a/Adapters/jiraserver.py b/Adapters/jiraserver.py\nindex cad091b..93793c6 100644\n--- a/Adapters/jiraserver.py\n+++ b/Adapters/jiraserver.py\n@@ -514,6 +514,7 @@ class CheckIfExist(Resource):\n         args = self.reqparse.parse_args()\n         host = cleanse_host(args['host'])\n         project_key = args['key']\n+        logger.debug(\"made it into the post jiraserver request\")\n \n         if args['pname'] not in jiraDict.keys():\n             try:\ndiff --git a/Monitor/VDMmonitor.js b/Monitor/VDMmonitor.js\nindex 1e3e134..78ae98c 100644\n--- a/Monitor/VDMmonitor.js\n+++ b/Monitor/VDMmonitor.js\n@@ -575,104 +575,107 @@ app.post('/reschedule',function(req,res){\n \n });\n \n-// FIXME: testing out whether or not project exists\n-// Force update for the adapter information sent through the request body\n-// Parameters: pname\n-app.get('/doesProjectExist',function(req,res) {\n-    log.info(\">> req.body: \" + req.body);\n+app.post('/doesProjectExist',function(req,res) {\n+    log.info(\"VDMMonitor - received request for doesProjectExist: \" + req.body);\n     var adapter_name = req.body.adapter_name;   // pname parameter\n-    var proj_info_type = req.body.idType; // ie jira_id, bitbucket_id\n+    var proj_info_type = req.body.idType;       // ie jira_id, bitbucket_id\n     var project_info_param = req.body.project_info;\n \n     log.info(\">> got into the doesProjectExist route\");\n-    // TODO: check if we should be using .view or .list\n-    // db.list([params], [callback]) will also list all the docs \n     schedulardb.view(\"schedular\",\"by_pname\", function(err,body) {\n-        log.info(\">> got into schedulardb.view\");\n-        log.info(\">> body: \" + body);\n-        log.info(\">> body.rows: \" + body.rows)\n-//        if(!err) {\n-//            body.rows.forEach(function(doc){\n-//                if (doc.value.pname && adapter_name === doc.value.pname) {\n-//                    var port_number = doc.value.port;\n-//                    var user_name = doc.value.username;\n-//                    var password = decrypt(doc.value.password);\n-//                    var host = doc.value.host;\n-//                    var last_update = doc.value.last_update;\n-//\n-//                    /** If it's a JIRA adapter **/\n-//                    if(proj_info_type === 'jira_id') {\n-//                        var postData = querystring.stringify({\n-//                            'pname': adapter_name,\n-//                            'username': user_name,\n-//                            'password': password,\n-//                            'host': host,\n-//                            'last_update': doc.value.last_update,\n-//                            'key': project_info_param['jira_id']\n-//                        });\n-//\n-//                        log.info(\"POST path: http://localhost:\" + port_number + \"/checkIfExist\");\n-//\n-//                        var options = {\n-//                            host: \"localhost\",\n-//                            port: port_number,\n-//                            path: \"/checkIfExist\",\n-//                            method: 'POST',\n-//                            headers: {  'Content-Type': 'application/x-www-form-urlencoded',\n-//                                        'Content-Length': Buffer.byteLength(postData)\n-//                            }\n-//                        };\n-//                        var post_req = http.request(options, function(res) {\n-//                            res.send(res.body));\n-//                        });\n-//\n-//                        post_req.on('error', function (error) {\n-//                            res.end(\"project doesn't exist\");\n-//                            log.info(\"project doesn't exist:\" + error);\n-//                        });\n-//                        post_req.write(postData);\n-//                        post_req.end();\n-//                    }\n-//\n-//                    /** If it's a bitbucket adapter **/\n-//                    if(proj_info_type === 'bitbucket_id') {\n-//                        var postData = querystring.stringify({\n-//                                'pname': adapter_name,\n-//                                'username': user_name,\n-//                                'password': password,\n-//                                'host': host,\n-//                                'last_update': doc.value.last_update,\n-//                                'key': project_info_param['bitbucket_id']\n-//                        });\n-//\n-//                        log.info(\"POST path: http://localhost:\" + port_number + \"/checkIfExist\");\n-//\n-//                        var options = {\n-//                            host: \"localhost\",\n-//                            port: port_number,\n-//                            path: \"/checkIfExist\",\n-//                            method: 'POST',\n-//                            headers: {  'Content-Type': 'application/x-www-form-urlencoded',\n-//                                        'Content-Length': Buffer.byteLength(postData)\n-//                            }\n-//                        };\n-//                        var post_req = http.request(options, function(res) {\n-//                            res.send(res.body));\n-//                        });\n-//\n-//                        post_req.on('error', function (error) {\n-//                            res.end(\"project doesn't exist\");\n-//                            log.info(\"project doesn't exist:\" + error);\n-//                        });\n-//                        post_req.write(postData);\n-//                        post_req.end();\n-//                    }\n-//                }\n-//            });\n-//        }\n-//        else {\n-//            log.info(\"ERROR: Could not connect to db\");\n-//        }\n+        if(!err) {\n+            body.rows.forEach(function(doc) {\n+                log.info('VDMMonitor iterating through every project doc');\n+                log.info('VDMMonitor - doesProjectExist: got pname from db: ' + doc.value.pname);\n+                log.info('VDMMonitor - doesProjectExist: got pname from req: ' + adapter_name);\n+                if (doc.value.pname && adapter_name === doc.value.pname) {\n+                    var port_number = doc.value.port;\n+\n+                    /** If it's a JIRA adapter **/\n+                    if(proj_info_type === 'jira_id') {\n+                        var postData = querystring.stringify({\n+                            'pname': adapter_name,\n+                            'username': doc.value.username,\n+                            'password': decrypt(doc.value.password),\n+                            'host': doc.value.host,\n+                            'last_update': doc.value.last_update,\n+                            'key': project_info_param['jira_id']\n+                        });\n+\n+                        var options = {\n+                            host: \"localhost\",\n+                            port: port_number,\n+                            path: \"/checkIfExist\",\n+                            method: 'POST',\n+                            headers: {  'Content-Type': 'application/x-www-form-urlencoded',\n+                                        'Content-Length': Buffer.byteLength(postData)\n+                            }\n+                        };\n+\n+                        var post_req = http.request(options, function(response) {\n+                            log.info('Status: ' + response.statusCode);\n+                            response.setEncoding('utf8');\n+                            response.on('data', function (body) {\n+                                log.info('Body: ' + body);\n+                                res.send(body);\n+                            });\n+                        });\n+\n+                        post_req.on('error', function (error) {\n+                            res.end(\"project doesn't exist\");\n+                            log.info(\"VDMMonitor - project doesn't exist:\" + error);\n+                        });\n+\n+                        post_req.write(postData);\n+                        post_req.end();\n+                    }\n+                    // res.sendStatus(200);\n+\n+                    /** If it's a bitbucket adapter **/\n+                    else if(proj_info_type === 'bitbucket_id') {\n+                        var postData = querystring.stringify({\n+                            'pname': adapter_name,\n+                            'username': doc.value.username,\n+                            'password': decrypt(doc.value.password),\n+                            'host': doc.value.host,\n+                            'last_update': doc.value.last_update,\n+                            'key': project_info_param['jira_id']\n+                        });\n+\n+                        var options = {\n+                            host: \"localhost\",\n+                            port: port_number,\n+                            path: \"/checkIfExist\",\n+                            method: 'POST',\n+                            headers: {  'Content-Type': 'application/x-www-form-urlencoded',\n+                                        'Content-Length': Buffer.byteLength(postData)\n+                            }\n+                        };\n+\n+                        var post_req = http.request(options, function(response) {\n+                            log.info('Status: ' + response.statusCode);\n+                            response.setEncoding('utf8');\n+                            response.on('data', function (body) {\n+                                log.info('Body: ' + body);\n+                                res.send(body);\n+                            });\n+                        });\n+\n+                        post_req.on('error', function (error) {\n+                            res.end(\"project doesn't exist\");\n+                            log.info(\"VDMMonitor - project doesn't exist:\" + error);\n+                        });\n+\n+                        post_req.write(postData);\n+                        post_req.end();\n+                    }\n+                }\n+            });\n+        }\n+        else {\n+            res.sendStatus(500);\n+            log.info(\"ERROR: Could not connect to db\");\n+        }\n     });\n });\n \ndiff --git a/Vagrantfile b/Vagrantfile\nindex c1427a1..5f186fe 100644\n--- a/Vagrantfile\n+++ b/Vagrantfile\n@@ -21,6 +21,8 @@ Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|\n   # Add port-forward Sails.js application\n   config.vm.network \"forwarded_port\", guest: 8000, host: 8000\n   config.vm.network \"forwarded_port\", guest: 3000, host: 3000\n+  config.vm.network \"forwarded_port\", guest: 5689, host: 5689\n+  config.vm.network \"forwarded_port\", guest: 5656, host: 5656\n \n   # Create a private network, which allows host-only access to the machine\n   # using a specific IP.\ndiff --git a/vdm/main/views.py b/vdm/main/views.py\nindex 7af5778..4e5ea01 100644\n--- a/vdm/main/views.py\n+++ b/vdm/main/views.py\n@@ -207,6 +207,7 @@ def add_project(request):\n     :param request: HttpRequest object\n     :return: return a rendered page\n     \"\"\"\n+    monitor_port = 5689\n     try:\n         if request.method == 'GET':\n             # if the user requests to get the \"add_project.html\" but not to submit new project information\n@@ -236,11 +237,13 @@ def add_project(request):\n             if 'pname' in adapter_db[doc] and adapter_db[doc]['pname'] == project_info['adapter']:\n                 # check if project exists in the external server\n                 if 'jira_id' in project_info:\n-                    response = unirest.get(\"http://localhost:\" + 3000 + \"/doesProjectExist\",\n+                    path = \"http://localhost:\" + str(monitor_port) + \"/doesProjectExist\"\n+                    response = unirest.post(path,\n                                             headers={\"Content-Type\": \"application/x-www-form-urlencoded\"},\n                                             params={'adapter_name': adapter_db[doc]['pname'],\n                                                     'project_info': project_info,\n                                                     'idType': 'jira_id'})\n+                    logger.info(\"response.body: %s\" % response.body)\n                     result = response.body[\"message\"]\n                     logger.info(\"this is the result line 245: %s\" % result)\n \n@@ -249,7 +252,7 @@ def add_project(request):\n                         return redirect(reverse('project_configuration'))\n \n                 if 'bitbucket_id' in project_info:\n-                    response = unirest.get(\"http://localhost:\" + 3000 + \"/doesProjectExist\",\n+                    response = unirest.post(\"http://localhost:\" + str(monitor_port) + \"/doesProjectExist\",\n                                             headers={\"Content-Type\": \"application/x-www-form-urlencoded\"},\n                                             params={'adapter_name': adapter_db[doc]['pname'],\n                                                     'project_info': project_info,\n@@ -266,7 +269,7 @@ def add_project(request):\n                     messages.error(request, error_message)\n                     return redirect(reverse('project_configuration'))\n \n-                # Async API request to correspond adapter\n+                # Async API request to corresponding adapter\n             #     test = unirest.post(\"http://localhost:\" + 3000 + \"/forceUpdate\",\n             #                  headers={\"Content-Type\": \"application/x-www-form-urlencoded\"},\n             #                  params={'pname': result.pname,\n"
            },
            {
                "_id": "virtualdevmanager_code_0292602",
                "_rev": "2-a9dda832563a8bf3b61ca631b19aa6c4",
                "comment": "commenting out bitbucket pipelines until ready for publishing\n",
                "added": 6,
                "author": "lwu613",
                "deleted": 2,
                "date": str(cur_time.year) + "-" + str(cur_time.month) + "-" + str(cur_time.day) + " 19:24:43",
                "diff": "diff --git a/bitbucket-pipelines.yml b/bitbucket-pipelines.yml\nindex 2b1364c..560e699 100644\n--- a/bitbucket-pipelines.yml\n+++ b/bitbucket-pipelines.yml\n@@ -9,5 +9,9 @@ pipelines:\n   default:\n     - step:\n         script: # Modify the commands below to build your repository.\n-          - npm install\n-          - python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n+          - cd configurationpage\n+          # - npm install\n+          # - cd ../Monitor\n+          # - npm install\n+          # - cd ..\n+          #- python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n"
            },
            {
                "_id": "virtualdevmanager_code_0292602",
                "_rev": "2-a9dda832563a8bf3b61ca631b19aa610",
                "comment": "commenting out bitbucket pipelines until ready for publishing2\n",
                "added": 6,
                "author": "lwu613",
                "deleted": 2,
                "date": str(cur_time.year) + "-" + str(cur_time.month) + "-" + str(cur_time.day) + " 19:25:43",
                "diff": "diff --git a/bitbucket-pipelines.yml b/bitbucket-pipelines.yml\nindex 2b1364c..560e699 100644\n--- a/bitbucket-pipelines.yml\n+++ b/bitbucket-pipelines.yml\n@@ -9,5 +9,9 @@ pipelines:\n   default:\n     - step:\n         script: # Modify the commands below to build your repository.\n-          - npm install\n-          - python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n+          - cd configurationpage\n+          # - npm install\n+          # - cd ../Monitor\n+          # - npm install\n+          # - cd ..\n+          #- python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n"
            },
            {
                "_id": "virtualdevmanager_code_0292602",
                "_rev": "2-a9dda832563a8bf3b61ca631b19aa6c4",
                "comment": "commenting out bitbucket pipelines until ready for publishing\n",
                "added": 6,
                "author": "lwu613",
                "deleted": 2,
                "date": None,
                "diff": "diff --git a/bitbucket-pipelines.yml b/bitbucket-pipelines.yml\nindex 2b1364c..560e699 100644\n--- a/bitbucket-pipelines.yml\n+++ b/bitbucket-pipelines.yml\n@@ -9,5 +9,9 @@ pipelines:\n   default:\n     - step:\n         script: # Modify the commands below to build your repository.\n-          - npm install\n-          - python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n+          - cd configurationpage\n+          # - npm install\n+          # - cd ../Monitor\n+          # - npm install\n+          # - cd ..\n+          #- python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n"
            },
            {
                "_id": "virtualdevmanager_code_63d8d73",
                "_rev": "2-5ca9ebb1df71db790ca7c1cbf77e2e23",
                "comment": "VDM-446 fix typo\n",
                "added": 1,
                "author": "xiaoleip",
                "deleted": 1,
                "date": str(yesterday.year) + "-" + str(yesterday.month) + "-" + str(yesterday.day) + " 15:18:16",
                "diff": "diff --git a/vdm/main/templates/add_project.html b/vdm/main/templates/add_project.html\nindex a94a9f9..71846b4 100644\n--- a/vdm/main/templates/add_project.html\n+++ b/vdm/main/templates/add_project.html\n@@ -101,7 +101,7 @@\n             <a data-toggle=\"tooltip\" title=\"Enter your FogBugz key, found under **.\"> <!--TODO -->\n                 <img style=\"max-height: 15px; max-width: 15px; float: none;\" src=\"../static/main/images/tooltip.png\">\n             </a>\n-          <input type=\"fogbugz-id\" class=\"form-control\" id=\"fogbugz-id\" name=\"fogbugz-id\" placeholder=\"fogBugz ID\">\n+          <input type=\"fogbugz-id\" class=\"form-control\" id=\"fogbugz-id\" name=\"fogbugz-id\" placeholder=\"FogBugz ID\">\n         </div>\n         <div class=\"submit-button\">\n       \t  <button type=\"submit\" class=\"btn btn-success btn-lg\">Submit</button>\n"
            }
        ]
        data = charts.create_repo_activity(input_data)
        expected_categories = get_categories()
        expected_num_of_commits = [0] * len(expected_categories)
        expected_num_of_commits[DATE_RANGE] = 2
        expected_num_of_commits[DATE_RANGE - 1] = 1
        expected_data = {
            'x_axis_title': "Date",
            'y_axis_title': "Number of Commits",
            'categories': expected_categories,
            'num': expected_num_of_commits
        }
        self.assertEqual(data, expected_data)

    # Tests charts.create_repo_activity
    def test_create_repo_activity_2(self):
        cur_time = parse(str(datetime.datetime.now()))

        input_data = []
        data = charts.create_repo_activity(input_data)
        expected_categories = get_categories()
        expected_num_of_commits = [0] * len(expected_categories)

        expected_data = {
            'x_axis_title': "Date",
            'y_axis_title': "Number of Commits",
            'categories': expected_categories,
            'num': expected_num_of_commits
        }
        self.assertEqual(data, expected_data)

    # Tests charts.create_repo_activity
    def test_create_repo_activity_3(self):
        input_data = None
        data = charts.create_repo_activity(input_data)
        expected_categories = get_categories()
        expected_num_of_commits = [0] * len(expected_categories)

        expected_data = {
            'x_axis_title': "Date",
            'y_axis_title': "Number of Commits",
            'categories': expected_categories,
            'num': expected_num_of_commits
        }
        self.assertEqual(data, expected_data)

    # Tests charts.create_activity_per_user
    def test_create_activity_per_user(self):
        cur_time = parse(str(datetime.datetime.now()))
        out_of_range_date = cur_time - timedelta(days=(DATE_RANGE + 1))
        yesterday = cur_time - timedelta(days=1)

        input_data = [
            {
                "_id": "virtualdevmanager_code_f924da5",
                "_rev": "2-f4b2fa3c08abc130bbf5fabcd9ad7472",
                "comment": "VDM-420: fixed doesprojectexist route\n",
                "added": 105,
                "author": "lwu613",
                "deleted": 96,
                "date": str(out_of_range_date.year) + "-" + str(out_of_range_date.month) + "-" + str(
                    out_of_range_date.day) + " 04:58:32",
                "diff": "diff --git a/Adapters/jiraserver.py b/Adapters/jiraserver.py\nindex cad091b..93793c6 100644\n--- a/Adapters/jiraserver.py\n+++ b/Adapters/jiraserver.py\n@@ -514,6 +514,7 @@ class CheckIfExist(Resource):\n         args = self.reqparse.parse_args()\n         host = cleanse_host(args['host'])\n         project_key = args['key']\n+        logger.debug(\"made it into the post jiraserver request\")\n \n         if args['pname'] not in jiraDict.keys():\n             try:\ndiff --git a/Monitor/VDMmonitor.js b/Monitor/VDMmonitor.js\nindex 1e3e134..78ae98c 100644\n--- a/Monitor/VDMmonitor.js\n+++ b/Monitor/VDMmonitor.js\n@@ -575,104 +575,107 @@ app.post('/reschedule',function(req,res){\n \n });\n \n-// FIXME: testing out whether or not project exists\n-// Force update for the adapter information sent through the request body\n-// Parameters: pname\n-app.get('/doesProjectExist',function(req,res) {\n-    log.info(\">> req.body: \" + req.body);\n+app.post('/doesProjectExist',function(req,res) {\n+    log.info(\"VDMMonitor - received request for doesProjectExist: \" + req.body);\n     var adapter_name = req.body.adapter_name;   // pname parameter\n-    var proj_info_type = req.body.idType; // ie jira_id, bitbucket_id\n+    var proj_info_type = req.body.idType;       // ie jira_id, bitbucket_id\n     var project_info_param = req.body.project_info;\n \n     log.info(\">> got into the doesProjectExist route\");\n-    // TODO: check if we should be using .view or .list\n-    // db.list([params], [callback]) will also list all the docs \n     schedulardb.view(\"schedular\",\"by_pname\", function(err,body) {\n-        log.info(\">> got into schedulardb.view\");\n-        log.info(\">> body: \" + body);\n-        log.info(\">> body.rows: \" + body.rows)\n-//        if(!err) {\n-//            body.rows.forEach(function(doc){\n-//                if (doc.value.pname && adapter_name === doc.value.pname) {\n-//                    var port_number = doc.value.port;\n-//                    var user_name = doc.value.username;\n-//                    var password = decrypt(doc.value.password);\n-//                    var host = doc.value.host;\n-//                    var last_update = doc.value.last_update;\n-//\n-//                    /** If it's a JIRA adapter **/\n-//                    if(proj_info_type === 'jira_id') {\n-//                        var postData = querystring.stringify({\n-//                            'pname': adapter_name,\n-//                            'username': user_name,\n-//                            'password': password,\n-//                            'host': host,\n-//                            'last_update': doc.value.last_update,\n-//                            'key': project_info_param['jira_id']\n-//                        });\n-//\n-//                        log.info(\"POST path: http://localhost:\" + port_number + \"/checkIfExist\");\n-//\n-//                        var options = {\n-//                            host: \"localhost\",\n-//                            port: port_number,\n-//                            path: \"/checkIfExist\",\n-//                            method: 'POST',\n-//                            headers: {  'Content-Type': 'application/x-www-form-urlencoded',\n-//                                        'Content-Length': Buffer.byteLength(postData)\n-//                            }\n-//                        };\n-//                        var post_req = http.request(options, function(res) {\n-//                            res.send(res.body));\n-//                        });\n-//\n-//                        post_req.on('error', function (error) {\n-//                            res.end(\"project doesn't exist\");\n-//                            log.info(\"project doesn't exist:\" + error);\n-//                        });\n-//                        post_req.write(postData);\n-//                        post_req.end();\n-//                    }\n-//\n-//                    /** If it's a bitbucket adapter **/\n-//                    if(proj_info_type === 'bitbucket_id') {\n-//                        var postData = querystring.stringify({\n-//                                'pname': adapter_name,\n-//                                'username': user_name,\n-//                                'password': password,\n-//                                'host': host,\n-//                                'last_update': doc.value.last_update,\n-//                                'key': project_info_param['bitbucket_id']\n-//                        });\n-//\n-//                        log.info(\"POST path: http://localhost:\" + port_number + \"/checkIfExist\");\n-//\n-//                        var options = {\n-//                            host: \"localhost\",\n-//                            port: port_number,\n-//                            path: \"/checkIfExist\",\n-//                            method: 'POST',\n-//                            headers: {  'Content-Type': 'application/x-www-form-urlencoded',\n-//                                        'Content-Length': Buffer.byteLength(postData)\n-//                            }\n-//                        };\n-//                        var post_req = http.request(options, function(res) {\n-//                            res.send(res.body));\n-//                        });\n-//\n-//                        post_req.on('error', function (error) {\n-//                            res.end(\"project doesn't exist\");\n-//                            log.info(\"project doesn't exist:\" + error);\n-//                        });\n-//                        post_req.write(postData);\n-//                        post_req.end();\n-//                    }\n-//                }\n-//            });\n-//        }\n-//        else {\n-//            log.info(\"ERROR: Could not connect to db\");\n-//        }\n+        if(!err) {\n+            body.rows.forEach(function(doc) {\n+                log.info('VDMMonitor iterating through every project doc');\n+                log.info('VDMMonitor - doesProjectExist: got pname from db: ' + doc.value.pname);\n+                log.info('VDMMonitor - doesProjectExist: got pname from req: ' + adapter_name);\n+                if (doc.value.pname && adapter_name === doc.value.pname) {\n+                    var port_number = doc.value.port;\n+\n+                    /** If it's a JIRA adapter **/\n+                    if(proj_info_type === 'jira_id') {\n+                        var postData = querystring.stringify({\n+                            'pname': adapter_name,\n+                            'username': doc.value.username,\n+                            'password': decrypt(doc.value.password),\n+                            'host': doc.value.host,\n+                            'last_update': doc.value.last_update,\n+                            'key': project_info_param['jira_id']\n+                        });\n+\n+                        var options = {\n+                            host: \"localhost\",\n+                            port: port_number,\n+                            path: \"/checkIfExist\",\n+                            method: 'POST',\n+                            headers: {  'Content-Type': 'application/x-www-form-urlencoded',\n+                                        'Content-Length': Buffer.byteLength(postData)\n+                            }\n+                        };\n+\n+                        var post_req = http.request(options, function(response) {\n+                            log.info('Status: ' + response.statusCode);\n+                            response.setEncoding('utf8');\n+                            response.on('data', function (body) {\n+                                log.info('Body: ' + body);\n+                                res.send(body);\n+                            });\n+                        });\n+\n+                        post_req.on('error', function (error) {\n+                            res.end(\"project doesn't exist\");\n+                            log.info(\"VDMMonitor - project doesn't exist:\" + error);\n+                        });\n+\n+                        post_req.write(postData);\n+                        post_req.end();\n+                    }\n+                    // res.sendStatus(200);\n+\n+                    /** If it's a bitbucket adapter **/\n+                    else if(proj_info_type === 'bitbucket_id') {\n+                        var postData = querystring.stringify({\n+                            'pname': adapter_name,\n+                            'username': doc.value.username,\n+                            'password': decrypt(doc.value.password),\n+                            'host': doc.value.host,\n+                            'last_update': doc.value.last_update,\n+                            'key': project_info_param['jira_id']\n+                        });\n+\n+                        var options = {\n+                            host: \"localhost\",\n+                            port: port_number,\n+                            path: \"/checkIfExist\",\n+                            method: 'POST',\n+                            headers: {  'Content-Type': 'application/x-www-form-urlencoded',\n+                                        'Content-Length': Buffer.byteLength(postData)\n+                            }\n+                        };\n+\n+                        var post_req = http.request(options, function(response) {\n+                            log.info('Status: ' + response.statusCode);\n+                            response.setEncoding('utf8');\n+                            response.on('data', function (body) {\n+                                log.info('Body: ' + body);\n+                                res.send(body);\n+                            });\n+                        });\n+\n+                        post_req.on('error', function (error) {\n+                            res.end(\"project doesn't exist\");\n+                            log.info(\"VDMMonitor - project doesn't exist:\" + error);\n+                        });\n+\n+                        post_req.write(postData);\n+                        post_req.end();\n+                    }\n+                }\n+            });\n+        }\n+        else {\n+            res.sendStatus(500);\n+            log.info(\"ERROR: Could not connect to db\");\n+        }\n     });\n });\n \ndiff --git a/Vagrantfile b/Vagrantfile\nindex c1427a1..5f186fe 100644\n--- a/Vagrantfile\n+++ b/Vagrantfile\n@@ -21,6 +21,8 @@ Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|\n   # Add port-forward Sails.js application\n   config.vm.network \"forwarded_port\", guest: 8000, host: 8000\n   config.vm.network \"forwarded_port\", guest: 3000, host: 3000\n+  config.vm.network \"forwarded_port\", guest: 5689, host: 5689\n+  config.vm.network \"forwarded_port\", guest: 5656, host: 5656\n \n   # Create a private network, which allows host-only access to the machine\n   # using a specific IP.\ndiff --git a/vdm/main/views.py b/vdm/main/views.py\nindex 7af5778..4e5ea01 100644\n--- a/vdm/main/views.py\n+++ b/vdm/main/views.py\n@@ -207,6 +207,7 @@ def add_project(request):\n     :param request: HttpRequest object\n     :return: return a rendered page\n     \"\"\"\n+    monitor_port = 5689\n     try:\n         if request.method == 'GET':\n             # if the user requests to get the \"add_project.html\" but not to submit new project information\n@@ -236,11 +237,13 @@ def add_project(request):\n             if 'pname' in adapter_db[doc] and adapter_db[doc]['pname'] == project_info['adapter']:\n                 # check if project exists in the external server\n                 if 'jira_id' in project_info:\n-                    response = unirest.get(\"http://localhost:\" + 3000 + \"/doesProjectExist\",\n+                    path = \"http://localhost:\" + str(monitor_port) + \"/doesProjectExist\"\n+                    response = unirest.post(path,\n                                             headers={\"Content-Type\": \"application/x-www-form-urlencoded\"},\n                                             params={'adapter_name': adapter_db[doc]['pname'],\n                                                     'project_info': project_info,\n                                                     'idType': 'jira_id'})\n+                    logger.info(\"response.body: %s\" % response.body)\n                     result = response.body[\"message\"]\n                     logger.info(\"this is the result line 245: %s\" % result)\n \n@@ -249,7 +252,7 @@ def add_project(request):\n                         return redirect(reverse('project_configuration'))\n \n                 if 'bitbucket_id' in project_info:\n-                    response = unirest.get(\"http://localhost:\" + 3000 + \"/doesProjectExist\",\n+                    response = unirest.post(\"http://localhost:\" + str(monitor_port) + \"/doesProjectExist\",\n                                             headers={\"Content-Type\": \"application/x-www-form-urlencoded\"},\n                                             params={'adapter_name': adapter_db[doc]['pname'],\n                                                     'project_info': project_info,\n@@ -266,7 +269,7 @@ def add_project(request):\n                     messages.error(request, error_message)\n                     return redirect(reverse('project_configuration'))\n \n-                # Async API request to correspond adapter\n+                # Async API request to corresponding adapter\n             #     test = unirest.post(\"http://localhost:\" + 3000 + \"/forceUpdate\",\n             #                  headers={\"Content-Type\": \"application/x-www-form-urlencoded\"},\n             #                  params={'pname': result.pname,\n"
            },
            {
                "_id": "virtualdevmanager_code_0292602",
                "_rev": "2-a9dda832563a8bf3b61ca631b19aa6c4",
                "comment": "commenting out bitbucket pipelines until ready for publishing\n",
                "added": 6,
                "author": "lwu613",
                "deleted": 2,
                "date": str(cur_time.year) + "-" + str(cur_time.month) + "-" + str(cur_time.day) + " 19:24:43",
                "diff": "diff --git a/bitbucket-pipelines.yml b/bitbucket-pipelines.yml\nindex 2b1364c..560e699 100644\n--- a/bitbucket-pipelines.yml\n+++ b/bitbucket-pipelines.yml\n@@ -9,5 +9,9 @@ pipelines:\n   default:\n     - step:\n         script: # Modify the commands below to build your repository.\n-          - npm install\n-          - python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n+          - cd configurationpage\n+          # - npm install\n+          # - cd ../Monitor\n+          # - npm install\n+          # - cd ..\n+          #- python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n"
            },
            {
                "_id": "virtualdevmanager_code_0292602",
                "_rev": "2-a9dda832563a8bf3b61ca631b19aa610",
                "comment": "commenting out bitbucket pipelines until ready for publishing2\n",
                "added": 6,
                "author": "lwu613",
                "deleted": 2,
                "date": str(cur_time.year) + "-" + str(cur_time.month) + "-" + str(cur_time.day) + " 19:25:43",
                "diff": "diff --git a/bitbucket-pipelines.yml b/bitbucket-pipelines.yml\nindex 2b1364c..560e699 100644\n--- a/bitbucket-pipelines.yml\n+++ b/bitbucket-pipelines.yml\n@@ -9,5 +9,9 @@ pipelines:\n   default:\n     - step:\n         script: # Modify the commands below to build your repository.\n-          - npm install\n-          - python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n+          - cd configurationpage\n+          # - npm install\n+          # - cd ../Monitor\n+          # - npm install\n+          # - cd ..\n+          #- python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n"
            },
            {
                "_id": "virtualdevmanager_code_0292602",
                "_rev": "2-a9dda832563a8bf3b61ca631b19aa610",
                "comment": "commenting out bitbucket pipelines until ready for publishing2\n",
                "added": 6,
                "author": None,
                "deleted": 2,
                "date": str(cur_time.year) + "-" + str(cur_time.month) + "-" + str(cur_time.day) + " 19:25:43",
                "diff": "diff --git a/bitbucket-pipelines.yml b/bitbucket-pipelines.yml\nindex 2b1364c..560e699 100644\n--- a/bitbucket-pipelines.yml\n+++ b/bitbucket-pipelines.yml\n@@ -9,5 +9,9 @@ pipelines:\n   default:\n     - step:\n         script: # Modify the commands below to build your repository.\n-          - npm install\n-          - python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n+          - cd configurationpage\n+          # - npm install\n+          # - cd ../Monitor\n+          # - npm install\n+          # - cd ..\n+          #- python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n"
            },
            {
                "_id": "virtualdevmanager_code_0292602",
                "_rev": "2-a9dda832563a8bf3b61ca631b19aa6c4",
                "comment": "commenting out bitbucket pipelines until ready for publishing\n",
                "added": 6,
                "author": "lwu613",
                "deleted": 2,
                "date": None,
                "diff": "diff --git a/bitbucket-pipelines.yml b/bitbucket-pipelines.yml\nindex 2b1364c..560e699 100644\n--- a/bitbucket-pipelines.yml\n+++ b/bitbucket-pipelines.yml\n@@ -9,5 +9,9 @@ pipelines:\n   default:\n     - step:\n         script: # Modify the commands below to build your repository.\n-          - npm install\n-          - python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n+          - cd configurationpage\n+          # - npm install\n+          # - cd ../Monitor\n+          # - npm install\n+          # - cd ..\n+          #- python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n"
            },
            {
                "_id": "virtualdevmanager_code_63d8d73",
                "_rev": "2-5ca9ebb1df71db790ca7c1cbf77e2e23",
                "comment": "VDM-446 fix typo\n",
                "added": 1,
                "author": "xiaoleip",
                "deleted": 1,
                "date": str(yesterday.year) + "-" + str(yesterday.month) + "-" + str(yesterday.day) + " 15:18:16",
                "diff": "diff --git a/vdm/main/templates/add_project.html b/vdm/main/templates/add_project.html\nindex a94a9f9..71846b4 100644\n--- a/vdm/main/templates/add_project.html\n+++ b/vdm/main/templates/add_project.html\n@@ -101,7 +101,7 @@\n             <a data-toggle=\"tooltip\" title=\"Enter your FogBugz key, found under **.\"> <!--TODO -->\n                 <img style=\"max-height: 15px; max-width: 15px; float: none;\" src=\"../static/main/images/tooltip.png\">\n             </a>\n-          <input type=\"fogbugz-id\" class=\"form-control\" id=\"fogbugz-id\" name=\"fogbugz-id\" placeholder=\"fogBugz ID\">\n+          <input type=\"fogbugz-id\" class=\"form-control\" id=\"fogbugz-id\" name=\"fogbugz-id\" placeholder=\"FogBugz ID\">\n         </div>\n         <div class=\"submit-button\">\n       \t  <button type=\"submit\" class=\"btn btn-success btn-lg\">Submit</button>\n"
            }
        ]

        data = charts.create_activity_per_user(input_data)

        expected_num_commits_by_team_member = {"lwu613": 2, "xiaoleip": 1}
        expected_data = {
            'x_axis_title': "Team Members",
            'y_axis_title': "Number of Commits",
            'x_axis_values': expected_num_commits_by_team_member.keys(),
            'y_axis_values': expected_num_commits_by_team_member.values()
        }

        self.assertEqual(data, expected_data)

    # Tests charts.create_activity_per_user
    def test_create_activity_per_user_2(self):
        cur_time = parse(str(datetime.datetime.now()))

        input_data = []

        data = charts.create_activity_per_user(input_data)

        expected_num_commits_by_team_member = {}
        expected_data = {
            'x_axis_title': "Team Members",
            'y_axis_title': "Number of Commits",
            'x_axis_values': expected_num_commits_by_team_member.keys(),
            'y_axis_values': expected_num_commits_by_team_member.values()
        }

        self.assertEqual(data, expected_data)

    # Tests charts.create_activity_per_user
    def test_create_activity_per_user_3(self):
        cur_time = parse(str(datetime.datetime.now()))

        input_data = None

        data = charts.create_activity_per_user(input_data)

        expected_num_commits_by_team_member = {}
        expected_data = {
            'x_axis_title': "Team Members",
            'y_axis_title': "Number of Commits",
            'x_axis_values': expected_num_commits_by_team_member.keys(),
            'y_axis_values': expected_num_commits_by_team_member.values()
        }

        self.assertEqual(data, expected_data)

    # Tests charts.create_modification_activity
    def test_create_modification_activity(self):
        cur_time = parse(str(datetime.datetime.now()))
        out_of_range_date = cur_time - timedelta(days=(DATE_RANGE + 1))
        yesterday = cur_time - timedelta(days=1)

        input_data = [
            {
                "_id": "virtualdevmanager_code_f924da5",
                "_rev": "2-f4b2fa3c08abc130bbf5fabcd9ad7472",
                "comment": "VDM-420: fixed doesprojectexist route\n",
                "added": 105,
                "author": "lwu613",
                "deleted": 96,
                "date": str(out_of_range_date.year) + "-" + str(out_of_range_date.month) + "-" + str(
                    out_of_range_date.day) + " 04:58:32",
                "diff": "diff --git a/Adapters/jiraserver.py b/Adapters/jiraserver.py\nindex cad091b..93793c6 100644\n--- a/Adapters/jiraserver.py\n+++ b/Adapters/jiraserver.py\n@@ -514,6 +514,7 @@ class CheckIfExist(Resource):\n         args = self.reqparse.parse_args()\n         host = cleanse_host(args['host'])\n         project_key = args['key']\n+        logger.debug(\"made it into the post jiraserver request\")\n \n         if args['pname'] not in jiraDict.keys():\n             try:\ndiff --git a/Monitor/VDMmonitor.js b/Monitor/VDMmonitor.js\nindex 1e3e134..78ae98c 100644\n--- a/Monitor/VDMmonitor.js\n+++ b/Monitor/VDMmonitor.js\n@@ -575,104 +575,107 @@ app.post('/reschedule',function(req,res){\n \n });\n \n-// FIXME: testing out whether or not project exists\n-// Force update for the adapter information sent through the request body\n-// Parameters: pname\n-app.get('/doesProjectExist',function(req,res) {\n-    log.info(\">> req.body: \" + req.body);\n+app.post('/doesProjectExist',function(req,res) {\n+    log.info(\"VDMMonitor - received request for doesProjectExist: \" + req.body);\n     var adapter_name = req.body.adapter_name;   // pname parameter\n-    var proj_info_type = req.body.idType; // ie jira_id, bitbucket_id\n+    var proj_info_type = req.body.idType;       // ie jira_id, bitbucket_id\n     var project_info_param = req.body.project_info;\n \n     log.info(\">> got into the doesProjectExist route\");\n-    // TODO: check if we should be using .view or .list\n-    // db.list([params], [callback]) will also list all the docs \n     schedulardb.view(\"schedular\",\"by_pname\", function(err,body) {\n-        log.info(\">> got into schedulardb.view\");\n-        log.info(\">> body: \" + body);\n-        log.info(\">> body.rows: \" + body.rows)\n-//        if(!err) {\n-//            body.rows.forEach(function(doc){\n-//                if (doc.value.pname && adapter_name === doc.value.pname) {\n-//                    var port_number = doc.value.port;\n-//                    var user_name = doc.value.username;\n-//                    var password = decrypt(doc.value.password);\n-//                    var host = doc.value.host;\n-//                    var last_update = doc.value.last_update;\n-//\n-//                    /** If it's a JIRA adapter **/\n-//                    if(proj_info_type === 'jira_id') {\n-//                        var postData = querystring.stringify({\n-//                            'pname': adapter_name,\n-//                            'username': user_name,\n-//                            'password': password,\n-//                            'host': host,\n-//                            'last_update': doc.value.last_update,\n-//                            'key': project_info_param['jira_id']\n-//                        });\n-//\n-//                        log.info(\"POST path: http://localhost:\" + port_number + \"/checkIfExist\");\n-//\n-//                        var options = {\n-//                            host: \"localhost\",\n-//                            port: port_number,\n-//                            path: \"/checkIfExist\",\n-//                            method: 'POST',\n-//                            headers: {  'Content-Type': 'application/x-www-form-urlencoded',\n-//                                        'Content-Length': Buffer.byteLength(postData)\n-//                            }\n-//                        };\n-//                        var post_req = http.request(options, function(res) {\n-//                            res.send(res.body));\n-//                        });\n-//\n-//                        post_req.on('error', function (error) {\n-//                            res.end(\"project doesn't exist\");\n-//                            log.info(\"project doesn't exist:\" + error);\n-//                        });\n-//                        post_req.write(postData);\n-//                        post_req.end();\n-//                    }\n-//\n-//                    /** If it's a bitbucket adapter **/\n-//                    if(proj_info_type === 'bitbucket_id') {\n-//                        var postData = querystring.stringify({\n-//                                'pname': adapter_name,\n-//                                'username': user_name,\n-//                                'password': password,\n-//                                'host': host,\n-//                                'last_update': doc.value.last_update,\n-//                                'key': project_info_param['bitbucket_id']\n-//                        });\n-//\n-//                        log.info(\"POST path: http://localhost:\" + port_number + \"/checkIfExist\");\n-//\n-//                        var options = {\n-//                            host: \"localhost\",\n-//                            port: port_number,\n-//                            path: \"/checkIfExist\",\n-//                            method: 'POST',\n-//                            headers: {  'Content-Type': 'application/x-www-form-urlencoded',\n-//                                        'Content-Length': Buffer.byteLength(postData)\n-//                            }\n-//                        };\n-//                        var post_req = http.request(options, function(res) {\n-//                            res.send(res.body));\n-//                        });\n-//\n-//                        post_req.on('error', function (error) {\n-//                            res.end(\"project doesn't exist\");\n-//                            log.info(\"project doesn't exist:\" + error);\n-//                        });\n-//                        post_req.write(postData);\n-//                        post_req.end();\n-//                    }\n-//                }\n-//            });\n-//        }\n-//        else {\n-//            log.info(\"ERROR: Could not connect to db\");\n-//        }\n+        if(!err) {\n+            body.rows.forEach(function(doc) {\n+                log.info('VDMMonitor iterating through every project doc');\n+                log.info('VDMMonitor - doesProjectExist: got pname from db: ' + doc.value.pname);\n+                log.info('VDMMonitor - doesProjectExist: got pname from req: ' + adapter_name);\n+                if (doc.value.pname && adapter_name === doc.value.pname) {\n+                    var port_number = doc.value.port;\n+\n+                    /** If it's a JIRA adapter **/\n+                    if(proj_info_type === 'jira_id') {\n+                        var postData = querystring.stringify({\n+                            'pname': adapter_name,\n+                            'username': doc.value.username,\n+                            'password': decrypt(doc.value.password),\n+                            'host': doc.value.host,\n+                            'last_update': doc.value.last_update,\n+                            'key': project_info_param['jira_id']\n+                        });\n+\n+                        var options = {\n+                            host: \"localhost\",\n+                            port: port_number,\n+                            path: \"/checkIfExist\",\n+                            method: 'POST',\n+                            headers: {  'Content-Type': 'application/x-www-form-urlencoded',\n+                                        'Content-Length': Buffer.byteLength(postData)\n+                            }\n+                        };\n+\n+                        var post_req = http.request(options, function(response) {\n+                            log.info('Status: ' + response.statusCode);\n+                            response.setEncoding('utf8');\n+                            response.on('data', function (body) {\n+                                log.info('Body: ' + body);\n+                                res.send(body);\n+                            });\n+                        });\n+\n+                        post_req.on('error', function (error) {\n+                            res.end(\"project doesn't exist\");\n+                            log.info(\"VDMMonitor - project doesn't exist:\" + error);\n+                        });\n+\n+                        post_req.write(postData);\n+                        post_req.end();\n+                    }\n+                    // res.sendStatus(200);\n+\n+                    /** If it's a bitbucket adapter **/\n+                    else if(proj_info_type === 'bitbucket_id') {\n+                        var postData = querystring.stringify({\n+                            'pname': adapter_name,\n+                            'username': doc.value.username,\n+                            'password': decrypt(doc.value.password),\n+                            'host': doc.value.host,\n+                            'last_update': doc.value.last_update,\n+                            'key': project_info_param['jira_id']\n+                        });\n+\n+                        var options = {\n+                            host: \"localhost\",\n+                            port: port_number,\n+                            path: \"/checkIfExist\",\n+                            method: 'POST',\n+                            headers: {  'Content-Type': 'application/x-www-form-urlencoded',\n+                                        'Content-Length': Buffer.byteLength(postData)\n+                            }\n+                        };\n+\n+                        var post_req = http.request(options, function(response) {\n+                            log.info('Status: ' + response.statusCode);\n+                            response.setEncoding('utf8');\n+                            response.on('data', function (body) {\n+                                log.info('Body: ' + body);\n+                                res.send(body);\n+                            });\n+                        });\n+\n+                        post_req.on('error', function (error) {\n+                            res.end(\"project doesn't exist\");\n+                            log.info(\"VDMMonitor - project doesn't exist:\" + error);\n+                        });\n+\n+                        post_req.write(postData);\n+                        post_req.end();\n+                    }\n+                }\n+            });\n+        }\n+        else {\n+            res.sendStatus(500);\n+            log.info(\"ERROR: Could not connect to db\");\n+        }\n     });\n });\n \ndiff --git a/Vagrantfile b/Vagrantfile\nindex c1427a1..5f186fe 100644\n--- a/Vagrantfile\n+++ b/Vagrantfile\n@@ -21,6 +21,8 @@ Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|\n   # Add port-forward Sails.js application\n   config.vm.network \"forwarded_port\", guest: 8000, host: 8000\n   config.vm.network \"forwarded_port\", guest: 3000, host: 3000\n+  config.vm.network \"forwarded_port\", guest: 5689, host: 5689\n+  config.vm.network \"forwarded_port\", guest: 5656, host: 5656\n \n   # Create a private network, which allows host-only access to the machine\n   # using a specific IP.\ndiff --git a/vdm/main/views.py b/vdm/main/views.py\nindex 7af5778..4e5ea01 100644\n--- a/vdm/main/views.py\n+++ b/vdm/main/views.py\n@@ -207,6 +207,7 @@ def add_project(request):\n     :param request: HttpRequest object\n     :return: return a rendered page\n     \"\"\"\n+    monitor_port = 5689\n     try:\n         if request.method == 'GET':\n             # if the user requests to get the \"add_project.html\" but not to submit new project information\n@@ -236,11 +237,13 @@ def add_project(request):\n             if 'pname' in adapter_db[doc] and adapter_db[doc]['pname'] == project_info['adapter']:\n                 # check if project exists in the external server\n                 if 'jira_id' in project_info:\n-                    response = unirest.get(\"http://localhost:\" + 3000 + \"/doesProjectExist\",\n+                    path = \"http://localhost:\" + str(monitor_port) + \"/doesProjectExist\"\n+                    response = unirest.post(path,\n                                             headers={\"Content-Type\": \"application/x-www-form-urlencoded\"},\n                                             params={'adapter_name': adapter_db[doc]['pname'],\n                                                     'project_info': project_info,\n                                                     'idType': 'jira_id'})\n+                    logger.info(\"response.body: %s\" % response.body)\n                     result = response.body[\"message\"]\n                     logger.info(\"this is the result line 245: %s\" % result)\n \n@@ -249,7 +252,7 @@ def add_project(request):\n                         return redirect(reverse('project_configuration'))\n \n                 if 'bitbucket_id' in project_info:\n-                    response = unirest.get(\"http://localhost:\" + 3000 + \"/doesProjectExist\",\n+                    response = unirest.post(\"http://localhost:\" + str(monitor_port) + \"/doesProjectExist\",\n                                             headers={\"Content-Type\": \"application/x-www-form-urlencoded\"},\n                                             params={'adapter_name': adapter_db[doc]['pname'],\n                                                     'project_info': project_info,\n@@ -266,7 +269,7 @@ def add_project(request):\n                     messages.error(request, error_message)\n                     return redirect(reverse('project_configuration'))\n \n-                # Async API request to correspond adapter\n+                # Async API request to corresponding adapter\n             #     test = unirest.post(\"http://localhost:\" + 3000 + \"/forceUpdate\",\n             #                  headers={\"Content-Type\": \"application/x-www-form-urlencoded\"},\n             #                  params={'pname': result.pname,\n"
            },
            {
                "_id": "virtualdevmanager_code_0292602",
                "_rev": "2-a9dda832563a8bf3b61ca631b19aa6c4",
                "comment": "commenting out bitbucket pipelines until ready for publishing\n",
                "added": 6,
                "author": "lwu613",
                "deleted": 2,
                "date": str(cur_time.year) + "-" + str(cur_time.month) + "-" + str(cur_time.day) + " 19:24:43",
                "diff": "diff --git a/bitbucket-pipelines.yml b/bitbucket-pipelines.yml\nindex 2b1364c..560e699 100644\n--- a/bitbucket-pipelines.yml\n+++ b/bitbucket-pipelines.yml\n@@ -9,5 +9,9 @@ pipelines:\n   default:\n     - step:\n         script: # Modify the commands below to build your repository.\n-          - npm install\n-          - python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n+          - cd configurationpage\n+          # - npm install\n+          # - cd ../Monitor\n+          # - npm install\n+          # - cd ..\n+          #- python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n"
            },
            {
                "_id": "virtualdevmanager_code_0292602",
                "_rev": "2-a9dda832563a8bf3b61ca631b19aa610",
                "comment": "commenting out bitbucket pipelines until ready for publishing2\n",
                "added": 64,
                "author": "lwu613",
                "deleted": 3,
                "date": str(cur_time.year) + "-" + str(cur_time.month) + "-" + str(cur_time.day) + " 19:25:43",
                "diff": "diff --git a/bitbucket-pipelines.yml b/bitbucket-pipelines.yml\nindex 2b1364c..560e699 100644\n--- a/bitbucket-pipelines.yml\n+++ b/bitbucket-pipelines.yml\n@@ -9,5 +9,9 @@ pipelines:\n   default:\n     - step:\n         script: # Modify the commands below to build your repository.\n-          - npm install\n-          - python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n+          - cd configurationpage\n+          # - npm install\n+          # - cd ../Monitor\n+          # - npm install\n+          # - cd ..\n+          #- python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n"
            },
            {
                "_id": "virtualdevmanager_code_0292602",
                "_rev": "2-a9dda832563a8bf3b61ca631b19aa610",
                "comment": "commenting out bitbucket pipelines until ready for publishing2\n",
                "added": 6,
                "author": "yuheng",
                "deleted": 2,
                "date": str(yesterday.year) + "-" + str(yesterday.month) + "-" + str(yesterday.day) + " 19:27:43",
                "diff": "diff --git a/bitbucket-pipelines.yml b/bitbucket-pipelines.yml\nindex 2b1364c..560e699 100644\n--- a/bitbucket-pipelines.yml\n+++ b/bitbucket-pipelines.yml\n@@ -9,5 +9,9 @@ pipelines:\n   default:\n     - step:\n         script: # Modify the commands below to build your repository.\n-          - npm install\n-          - python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n+          - cd configurationpage\n+          # - npm install\n+          # - cd ../Monitor\n+          # - npm install\n+          # - cd ..\n+          #- python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n"
            },
            {
                "_id": "virtualdevmanager_code_0292602",
                "_rev": "2-a9dda832563a8bf3b61ca631b19aa6c4",
                "comment": "commenting out bitbucket pipelines until ready for publishing\n",
                "added": 6,
                "author": "lwu613",
                "deleted": 2,
                "date": None,
                "diff": "diff --git a/bitbucket-pipelines.yml b/bitbucket-pipelines.yml\nindex 2b1364c..560e699 100644\n--- a/bitbucket-pipelines.yml\n+++ b/bitbucket-pipelines.yml\n@@ -9,5 +9,9 @@ pipelines:\n   default:\n     - step:\n         script: # Modify the commands below to build your repository.\n-          - npm install\n-          - python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n+          - cd configurationpage\n+          # - npm install\n+          # - cd ../Monitor\n+          # - npm install\n+          # - cd ..\n+          #- python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n"
            },
            {
                "_id": "virtualdevmanager_code_63d8d73",
                "_rev": "2-5ca9ebb1df71db790ca7c1cbf77e2e23",
                "comment": "VDM-446 fix typo\n",
                "author": "xiaoleip",
                "deleted": 1,
                "date": str(yesterday.year) + "-" + str(yesterday.month) + "-" + str(yesterday.day) + " 15:18:16",
                "diff": "diff --git a/vdm/main/templates/add_project.html b/vdm/main/templates/add_project.html\nindex a94a9f9..71846b4 100644\n--- a/vdm/main/templates/add_project.html\n+++ b/vdm/main/templates/add_project.html\n@@ -101,7 +101,7 @@\n             <a data-toggle=\"tooltip\" title=\"Enter your FogBugz key, found under **.\"> <!--TODO -->\n                 <img style=\"max-height: 15px; max-width: 15px; float: none;\" src=\"../static/main/images/tooltip.png\">\n             </a>\n-          <input type=\"fogbugz-id\" class=\"form-control\" id=\"fogbugz-id\" name=\"fogbugz-id\" placeholder=\"fogBugz ID\">\n+          <input type=\"fogbugz-id\" class=\"form-control\" id=\"fogbugz-id\" name=\"fogbugz-id\" placeholder=\"FogBugz ID\">\n         </div>\n         <div class=\"submit-button\">\n       \t  <button type=\"submit\" class=\"btn btn-success btn-lg\">Submit</button>\n"
            },
            {
                "_id": "virtualdevmanager_code_63d8d73",
                "_rev": "2-5ca9ebb1df71db790ca7c1cbf77e2e23",
                "comment": "VDM-446 fix typo\n",
                "author": "xiaoleip",
                "added": 10,
                "date": str(yesterday.year) + "-" + str(yesterday.month) + "-" + str(yesterday.day) + " 15:18:16",
                "diff": "diff --git a/vdm/main/templates/add_project.html b/vdm/main/templates/add_project.html\nindex a94a9f9..71846b4 100644\n--- a/vdm/main/templates/add_project.html\n+++ b/vdm/main/templates/add_project.html\n@@ -101,7 +101,7 @@\n             <a data-toggle=\"tooltip\" title=\"Enter your FogBugz key, found under **.\"> <!--TODO -->\n                 <img style=\"max-height: 15px; max-width: 15px; float: none;\" src=\"../static/main/images/tooltip.png\">\n             </a>\n-          <input type=\"fogbugz-id\" class=\"form-control\" id=\"fogbugz-id\" name=\"fogbugz-id\" placeholder=\"fogBugz ID\">\n+          <input type=\"fogbugz-id\" class=\"form-control\" id=\"fogbugz-id\" name=\"fogbugz-id\" placeholder=\"FogBugz ID\">\n         </div>\n         <div class=\"submit-button\">\n       \t  <button type=\"submit\" class=\"btn btn-success btn-lg\">Submit</button>\n"
            },
            {
                "_id": "virtualdevmanager_code_0292602",
                "_rev": "2-a9dda832563a8bf3b61ca631b19aa6c4",
                "comment": "commenting out bitbucket pipelines until ready for publishing\n",
                "added": None,
                "author": "lwu613",
                "deleted": 2,
                "date": str(yesterday.year) + "-" + str(yesterday.month) + "-" + str(yesterday.day) + " 15:20:16",
                "diff": "diff --git a/bitbucket-pipelines.yml b/bitbucket-pipelines.yml\nindex 2b1364c..560e699 100644\n--- a/bitbucket-pipelines.yml\n+++ b/bitbucket-pipelines.yml\n@@ -9,5 +9,9 @@ pipelines:\n   default:\n     - step:\n         script: # Modify the commands below to build your repository.\n-          - npm install\n-          - python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n+          - cd configurationpage\n+          # - npm install\n+          # - cd ../Monitor\n+          # - npm install\n+          # - cd ..\n+          #- python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n"
            },
            {
                "_id": "virtualdevmanager_code_0292602",
                "_rev": "2-a9dda832563a8bf3b61ca631b19aa6c4",
                "comment": "commenting out bitbucket pipelines until ready for publishing\n",
                "added": 3,
                "author": "lwu613",
                "deleted": None,
                "date": str(yesterday.year) + "-" + str(yesterday.month) + "-" + str(yesterday.day) + " 15:20:16",
                "diff": "diff --git a/bitbucket-pipelines.yml b/bitbucket-pipelines.yml\nindex 2b1364c..560e699 100644\n--- a/bitbucket-pipelines.yml\n+++ b/bitbucket-pipelines.yml\n@@ -9,5 +9,9 @@ pipelines:\n   default:\n     - step:\n         script: # Modify the commands below to build your repository.\n-          - npm install\n-          - python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n+          - cd configurationpage\n+          # - npm install\n+          # - cd ../Monitor\n+          # - npm install\n+          # - cd ..\n+          #- python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n"
            },
        ]

        data = charts.create_modification_activity(input_data)
        expected_categories = get_categories()
        added_list = len(expected_categories) * [0]
        deleted_list = len(expected_categories) * [0]
        added_list[DATE_RANGE] = 70
        deleted_list[DATE_RANGE] = 5
        added_list[DATE_RANGE - 1] = 19
        deleted_list[DATE_RANGE - 1] = 5

        expected_data = {
            'x_axis_title': "Date",
            'y_axis_title': "Lines of Code",
            'categories': expected_categories,
            'added': added_list,
            'deleted': deleted_list
        }

        self.assertEqual(data, expected_data)

    # Tests charts.create_modification_activity
    def test_create_modification_activity_2(self):
        input_data = []

        data = charts.create_modification_activity(input_data)
        expected_categories = get_categories()
        added_list = len(expected_categories) * [0]
        deleted_list = len(expected_categories) * [0]

        expected_data = {
            'x_axis_title': "Date",
            'y_axis_title': "Lines of Code",
            'categories': expected_categories,
            'added': added_list,
            'deleted': deleted_list
        }

        self.assertEqual(data, expected_data)

    # Tests charts.create_modification_activity
    def test_create_modification_activity_3(self):
        cur_time = parse(str(datetime.datetime.now()))

        input_data = None

        data = charts.create_modification_activity(input_data)
        expected_categories = get_categories()
        added_list = len(expected_categories) * [0]
        deleted_list = len(expected_categories) * [0]

        expected_data = {
            'x_axis_title': "Date",
            'y_axis_title': "Lines of Code",
            'categories': expected_categories,
            'added': added_list,
            'deleted': deleted_list
        }

        self.assertEqual(data, expected_data)

    # Tests charts.create_modification_activity_per_member
    def test_create_modification_activity_per_member(self):
        cur_time = parse(str(datetime.datetime.now()))
        out_of_range_date = cur_time - timedelta(days=(DATE_RANGE + 1))
        yesterday = cur_time - timedelta(days=1)

        input_data = [
            {
                "_id": "virtualdevmanager_code_f924da5",
                "_rev": "2-f4b2fa3c08abc130bbf5fabcd9ad7472",
                "comment": "VDM-420: fixed doesprojectexist route\n",
                "added": 105,
                "author": "lwu613",
                "deleted": 96,
                "date": str(out_of_range_date.year) + "-" + str(out_of_range_date.month) + "-" + str(
                    out_of_range_date.day) + " 04:58:32",
                "diff": "diff --git a/Adapters/jiraserver.py b/Adapters/jiraserver.py\nindex cad091b..93793c6 100644\n--- a/Adapters/jiraserver.py\n+++ b/Adapters/jiraserver.py\n@@ -514,6 +514,7 @@ class CheckIfExist(Resource):\n         args = self.reqparse.parse_args()\n         host = cleanse_host(args['host'])\n         project_key = args['key']\n+        logger.debug(\"made it into the post jiraserver request\")\n \n         if args['pname'] not in jiraDict.keys():\n             try:\ndiff --git a/Monitor/VDMmonitor.js b/Monitor/VDMmonitor.js\nindex 1e3e134..78ae98c 100644\n--- a/Monitor/VDMmonitor.js\n+++ b/Monitor/VDMmonitor.js\n@@ -575,104 +575,107 @@ app.post('/reschedule',function(req,res){\n \n });\n \n-// FIXME: testing out whether or not project exists\n-// Force update for the adapter information sent through the request body\n-// Parameters: pname\n-app.get('/doesProjectExist',function(req,res) {\n-    log.info(\">> req.body: \" + req.body);\n+app.post('/doesProjectExist',function(req,res) {\n+    log.info(\"VDMMonitor - received request for doesProjectExist: \" + req.body);\n     var adapter_name = req.body.adapter_name;   // pname parameter\n-    var proj_info_type = req.body.idType; // ie jira_id, bitbucket_id\n+    var proj_info_type = req.body.idType;       // ie jira_id, bitbucket_id\n     var project_info_param = req.body.project_info;\n \n     log.info(\">> got into the doesProjectExist route\");\n-    // TODO: check if we should be using .view or .list\n-    // db.list([params], [callback]) will also list all the docs \n     schedulardb.view(\"schedular\",\"by_pname\", function(err,body) {\n-        log.info(\">> got into schedulardb.view\");\n-        log.info(\">> body: \" + body);\n-        log.info(\">> body.rows: \" + body.rows)\n-//        if(!err) {\n-//            body.rows.forEach(function(doc){\n-//                if (doc.value.pname && adapter_name === doc.value.pname) {\n-//                    var port_number = doc.value.port;\n-//                    var user_name = doc.value.username;\n-//                    var password = decrypt(doc.value.password);\n-//                    var host = doc.value.host;\n-//                    var last_update = doc.value.last_update;\n-//\n-//                    /** If it's a JIRA adapter **/\n-//                    if(proj_info_type === 'jira_id') {\n-//                        var postData = querystring.stringify({\n-//                            'pname': adapter_name,\n-//                            'username': user_name,\n-//                            'password': password,\n-//                            'host': host,\n-//                            'last_update': doc.value.last_update,\n-//                            'key': project_info_param['jira_id']\n-//                        });\n-//\n-//                        log.info(\"POST path: http://localhost:\" + port_number + \"/checkIfExist\");\n-//\n-//                        var options = {\n-//                            host: \"localhost\",\n-//                            port: port_number,\n-//                            path: \"/checkIfExist\",\n-//                            method: 'POST',\n-//                            headers: {  'Content-Type': 'application/x-www-form-urlencoded',\n-//                                        'Content-Length': Buffer.byteLength(postData)\n-//                            }\n-//                        };\n-//                        var post_req = http.request(options, function(res) {\n-//                            res.send(res.body));\n-//                        });\n-//\n-//                        post_req.on('error', function (error) {\n-//                            res.end(\"project doesn't exist\");\n-//                            log.info(\"project doesn't exist:\" + error);\n-//                        });\n-//                        post_req.write(postData);\n-//                        post_req.end();\n-//                    }\n-//\n-//                    /** If it's a bitbucket adapter **/\n-//                    if(proj_info_type === 'bitbucket_id') {\n-//                        var postData = querystring.stringify({\n-//                                'pname': adapter_name,\n-//                                'username': user_name,\n-//                                'password': password,\n-//                                'host': host,\n-//                                'last_update': doc.value.last_update,\n-//                                'key': project_info_param['bitbucket_id']\n-//                        });\n-//\n-//                        log.info(\"POST path: http://localhost:\" + port_number + \"/checkIfExist\");\n-//\n-//                        var options = {\n-//                            host: \"localhost\",\n-//                            port: port_number,\n-//                            path: \"/checkIfExist\",\n-//                            method: 'POST',\n-//                            headers: {  'Content-Type': 'application/x-www-form-urlencoded',\n-//                                        'Content-Length': Buffer.byteLength(postData)\n-//                            }\n-//                        };\n-//                        var post_req = http.request(options, function(res) {\n-//                            res.send(res.body));\n-//                        });\n-//\n-//                        post_req.on('error', function (error) {\n-//                            res.end(\"project doesn't exist\");\n-//                            log.info(\"project doesn't exist:\" + error);\n-//                        });\n-//                        post_req.write(postData);\n-//                        post_req.end();\n-//                    }\n-//                }\n-//            });\n-//        }\n-//        else {\n-//            log.info(\"ERROR: Could not connect to db\");\n-//        }\n+        if(!err) {\n+            body.rows.forEach(function(doc) {\n+                log.info('VDMMonitor iterating through every project doc');\n+                log.info('VDMMonitor - doesProjectExist: got pname from db: ' + doc.value.pname);\n+                log.info('VDMMonitor - doesProjectExist: got pname from req: ' + adapter_name);\n+                if (doc.value.pname && adapter_name === doc.value.pname) {\n+                    var port_number = doc.value.port;\n+\n+                    /** If it's a JIRA adapter **/\n+                    if(proj_info_type === 'jira_id') {\n+                        var postData = querystring.stringify({\n+                            'pname': adapter_name,\n+                            'username': doc.value.username,\n+                            'password': decrypt(doc.value.password),\n+                            'host': doc.value.host,\n+                            'last_update': doc.value.last_update,\n+                            'key': project_info_param['jira_id']\n+                        });\n+\n+                        var options = {\n+                            host: \"localhost\",\n+                            port: port_number,\n+                            path: \"/checkIfExist\",\n+                            method: 'POST',\n+                            headers: {  'Content-Type': 'application/x-www-form-urlencoded',\n+                                        'Content-Length': Buffer.byteLength(postData)\n+                            }\n+                        };\n+\n+                        var post_req = http.request(options, function(response) {\n+                            log.info('Status: ' + response.statusCode);\n+                            response.setEncoding('utf8');\n+                            response.on('data', function (body) {\n+                                log.info('Body: ' + body);\n+                                res.send(body);\n+                            });\n+                        });\n+\n+                        post_req.on('error', function (error) {\n+                            res.end(\"project doesn't exist\");\n+                            log.info(\"VDMMonitor - project doesn't exist:\" + error);\n+                        });\n+\n+                        post_req.write(postData);\n+                        post_req.end();\n+                    }\n+                    // res.sendStatus(200);\n+\n+                    /** If it's a bitbucket adapter **/\n+                    else if(proj_info_type === 'bitbucket_id') {\n+                        var postData = querystring.stringify({\n+                            'pname': adapter_name,\n+                            'username': doc.value.username,\n+                            'password': decrypt(doc.value.password),\n+                            'host': doc.value.host,\n+                            'last_update': doc.value.last_update,\n+                            'key': project_info_param['jira_id']\n+                        });\n+\n+                        var options = {\n+                            host: \"localhost\",\n+                            port: port_number,\n+                            path: \"/checkIfExist\",\n+                            method: 'POST',\n+                            headers: {  'Content-Type': 'application/x-www-form-urlencoded',\n+                                        'Content-Length': Buffer.byteLength(postData)\n+                            }\n+                        };\n+\n+                        var post_req = http.request(options, function(response) {\n+                            log.info('Status: ' + response.statusCode);\n+                            response.setEncoding('utf8');\n+                            response.on('data', function (body) {\n+                                log.info('Body: ' + body);\n+                                res.send(body);\n+                            });\n+                        });\n+\n+                        post_req.on('error', function (error) {\n+                            res.end(\"project doesn't exist\");\n+                            log.info(\"VDMMonitor - project doesn't exist:\" + error);\n+                        });\n+\n+                        post_req.write(postData);\n+                        post_req.end();\n+                    }\n+                }\n+            });\n+        }\n+        else {\n+            res.sendStatus(500);\n+            log.info(\"ERROR: Could not connect to db\");\n+        }\n     });\n });\n \ndiff --git a/Vagrantfile b/Vagrantfile\nindex c1427a1..5f186fe 100644\n--- a/Vagrantfile\n+++ b/Vagrantfile\n@@ -21,6 +21,8 @@ Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|\n   # Add port-forward Sails.js application\n   config.vm.network \"forwarded_port\", guest: 8000, host: 8000\n   config.vm.network \"forwarded_port\", guest: 3000, host: 3000\n+  config.vm.network \"forwarded_port\", guest: 5689, host: 5689\n+  config.vm.network \"forwarded_port\", guest: 5656, host: 5656\n \n   # Create a private network, which allows host-only access to the machine\n   # using a specific IP.\ndiff --git a/vdm/main/views.py b/vdm/main/views.py\nindex 7af5778..4e5ea01 100644\n--- a/vdm/main/views.py\n+++ b/vdm/main/views.py\n@@ -207,6 +207,7 @@ def add_project(request):\n     :param request: HttpRequest object\n     :return: return a rendered page\n     \"\"\"\n+    monitor_port = 5689\n     try:\n         if request.method == 'GET':\n             # if the user requests to get the \"add_project.html\" but not to submit new project information\n@@ -236,11 +237,13 @@ def add_project(request):\n             if 'pname' in adapter_db[doc] and adapter_db[doc]['pname'] == project_info['adapter']:\n                 # check if project exists in the external server\n                 if 'jira_id' in project_info:\n-                    response = unirest.get(\"http://localhost:\" + 3000 + \"/doesProjectExist\",\n+                    path = \"http://localhost:\" + str(monitor_port) + \"/doesProjectExist\"\n+                    response = unirest.post(path,\n                                             headers={\"Content-Type\": \"application/x-www-form-urlencoded\"},\n                                             params={'adapter_name': adapter_db[doc]['pname'],\n                                                     'project_info': project_info,\n                                                     'idType': 'jira_id'})\n+                    logger.info(\"response.body: %s\" % response.body)\n                     result = response.body[\"message\"]\n                     logger.info(\"this is the result line 245: %s\" % result)\n \n@@ -249,7 +252,7 @@ def add_project(request):\n                         return redirect(reverse('project_configuration'))\n \n                 if 'bitbucket_id' in project_info:\n-                    response = unirest.get(\"http://localhost:\" + 3000 + \"/doesProjectExist\",\n+                    response = unirest.post(\"http://localhost:\" + str(monitor_port) + \"/doesProjectExist\",\n                                             headers={\"Content-Type\": \"application/x-www-form-urlencoded\"},\n                                             params={'adapter_name': adapter_db[doc]['pname'],\n                                                     'project_info': project_info,\n@@ -266,7 +269,7 @@ def add_project(request):\n                     messages.error(request, error_message)\n                     return redirect(reverse('project_configuration'))\n \n-                # Async API request to correspond adapter\n+                # Async API request to corresponding adapter\n             #     test = unirest.post(\"http://localhost:\" + 3000 + \"/forceUpdate\",\n             #                  headers={\"Content-Type\": \"application/x-www-form-urlencoded\"},\n             #                  params={'pname': result.pname,\n"
            },
            {
                "_id": "virtualdevmanager_code_0292602",
                "_rev": "2-a9dda832563a8bf3b61ca631b19aa6c4",
                "comment": "commenting out bitbucket pipelines until ready for publishing\n",
                "added": 6,
                "author": "lwu613",
                "deleted": 2,
                "date": str(cur_time.year) + "-" + str(cur_time.month) + "-" + str(cur_time.day) + " 19:24:43",
                "diff": "diff --git a/bitbucket-pipelines.yml b/bitbucket-pipelines.yml\nindex 2b1364c..560e699 100644\n--- a/bitbucket-pipelines.yml\n+++ b/bitbucket-pipelines.yml\n@@ -9,5 +9,9 @@ pipelines:\n   default:\n     - step:\n         script: # Modify the commands below to build your repository.\n-          - npm install\n-          - python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n+          - cd configurationpage\n+          # - npm install\n+          # - cd ../Monitor\n+          # - npm install\n+          # - cd ..\n+          #- python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n"
            },
            {
                "_id": "virtualdevmanager_code_0292602",
                "_rev": "2-a9dda832563a8bf3b61ca631b19aa610",
                "comment": "commenting out bitbucket pipelines until ready for publishing2\n",
                "added": 64,
                "author": "lwu613",
                "deleted": 3,
                "date": str(cur_time.year) + "-" + str(cur_time.month) + "-" + str(cur_time.day) + " 19:25:43",
                "diff": "diff --git a/bitbucket-pipelines.yml b/bitbucket-pipelines.yml\nindex 2b1364c..560e699 100644\n--- a/bitbucket-pipelines.yml\n+++ b/bitbucket-pipelines.yml\n@@ -9,5 +9,9 @@ pipelines:\n   default:\n     - step:\n         script: # Modify the commands below to build your repository.\n-          - npm install\n-          - python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n+          - cd configurationpage\n+          # - npm install\n+          # - cd ../Monitor\n+          # - npm install\n+          # - cd ..\n+          #- python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n"
            },
            {
                "_id": "virtualdevmanager_code_0292602",
                "_rev": "2-a9dda832563a8bf3b61ca631b19aa610",
                "comment": "commenting out bitbucket pipelines until ready for publishing2\n",
                "added": 6,
                "author": "yuheng",
                "deleted": 2,
                "date": str(yesterday.year) + "-" + str(yesterday.month) + "-" + str(yesterday.day) + " 19:27:43",
                "diff": "diff --git a/bitbucket-pipelines.yml b/bitbucket-pipelines.yml\nindex 2b1364c..560e699 100644\n--- a/bitbucket-pipelines.yml\n+++ b/bitbucket-pipelines.yml\n@@ -9,5 +9,9 @@ pipelines:\n   default:\n     - step:\n         script: # Modify the commands below to build your repository.\n-          - npm install\n-          - python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n+          - cd configurationpage\n+          # - npm install\n+          # - cd ../Monitor\n+          # - npm install\n+          # - cd ..\n+          #- python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n"
            },
            {
                "_id": "virtualdevmanager_code_0292602",
                "_rev": "2-a9dda832563a8bf3b61ca631b19aa6c4",
                "comment": "commenting out bitbucket pipelines until ready for publishing\n",
                "added": 6,
                "author": "lwu613",
                "deleted": 2,
                "date": None,
                "diff": "diff --git a/bitbucket-pipelines.yml b/bitbucket-pipelines.yml\nindex 2b1364c..560e699 100644\n--- a/bitbucket-pipelines.yml\n+++ b/bitbucket-pipelines.yml\n@@ -9,5 +9,9 @@ pipelines:\n   default:\n     - step:\n         script: # Modify the commands below to build your repository.\n-          - npm install\n-          - python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n+          - cd configurationpage\n+          # - npm install\n+          # - cd ../Monitor\n+          # - npm install\n+          # - cd ..\n+          #- python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n"
            },
            {
                "_id": "virtualdevmanager_code_63d8d73",
                "_rev": "2-5ca9ebb1df71db790ca7c1cbf77e2e23",
                "comment": "VDM-446 fix typo\n",
                "author": "xiaoleip",
                "deleted": 1,
                "date": str(yesterday.year) + "-" + str(yesterday.month) + "-" + str(yesterday.day) + " 15:18:16",
                "diff": "diff --git a/vdm/main/templates/add_project.html b/vdm/main/templates/add_project.html\nindex a94a9f9..71846b4 100644\n--- a/vdm/main/templates/add_project.html\n+++ b/vdm/main/templates/add_project.html\n@@ -101,7 +101,7 @@\n             <a data-toggle=\"tooltip\" title=\"Enter your FogBugz key, found under **.\"> <!--TODO -->\n                 <img style=\"max-height: 15px; max-width: 15px; float: none;\" src=\"../static/main/images/tooltip.png\">\n             </a>\n-          <input type=\"fogbugz-id\" class=\"form-control\" id=\"fogbugz-id\" name=\"fogbugz-id\" placeholder=\"fogBugz ID\">\n+          <input type=\"fogbugz-id\" class=\"form-control\" id=\"fogbugz-id\" name=\"fogbugz-id\" placeholder=\"FogBugz ID\">\n         </div>\n         <div class=\"submit-button\">\n       \t  <button type=\"submit\" class=\"btn btn-success btn-lg\">Submit</button>\n"
            },
            {
                "_id": "virtualdevmanager_code_63d8d73",
                "_rev": "2-5ca9ebb1df71db790ca7c1cbf77e2e23",
                "comment": "VDM-446 fix typo\n",
                "author": "xiaoleip",
                "added": 10,
                "date": str(yesterday.year) + "-" + str(yesterday.month) + "-" + str(yesterday.day) + " 15:18:16",
                "diff": "diff --git a/vdm/main/templates/add_project.html b/vdm/main/templates/add_project.html\nindex a94a9f9..71846b4 100644\n--- a/vdm/main/templates/add_project.html\n+++ b/vdm/main/templates/add_project.html\n@@ -101,7 +101,7 @@\n             <a data-toggle=\"tooltip\" title=\"Enter your FogBugz key, found under **.\"> <!--TODO -->\n                 <img style=\"max-height: 15px; max-width: 15px; float: none;\" src=\"../static/main/images/tooltip.png\">\n             </a>\n-          <input type=\"fogbugz-id\" class=\"form-control\" id=\"fogbugz-id\" name=\"fogbugz-id\" placeholder=\"fogBugz ID\">\n+          <input type=\"fogbugz-id\" class=\"form-control\" id=\"fogbugz-id\" name=\"fogbugz-id\" placeholder=\"FogBugz ID\">\n         </div>\n         <div class=\"submit-button\">\n       \t  <button type=\"submit\" class=\"btn btn-success btn-lg\">Submit</button>\n"
            },
            {
                "_id": "virtualdevmanager_code_0292602",
                "_rev": "2-a9dda832563a8bf3b61ca631b19aa6c4",
                "comment": "commenting out bitbucket pipelines until ready for publishing\n",
                "added": None,
                "author": "lwu613",
                "deleted": 2,
                "date": str(yesterday.year) + "-" + str(yesterday.month) + "-" + str(yesterday.day) + " 15:20:16",
                "diff": "diff --git a/bitbucket-pipelines.yml b/bitbucket-pipelines.yml\nindex 2b1364c..560e699 100644\n--- a/bitbucket-pipelines.yml\n+++ b/bitbucket-pipelines.yml\n@@ -9,5 +9,9 @@ pipelines:\n   default:\n     - step:\n         script: # Modify the commands below to build your repository.\n-          - npm install\n-          - python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n+          - cd configurationpage\n+          # - npm install\n+          # - cd ../Monitor\n+          # - npm install\n+          # - cd ..\n+          #- python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n"
            },
            {
                "_id": "virtualdevmanager_code_0292602",
                "_rev": "2-a9dda832563a8bf3b61ca631b19aa6c4",
                "comment": "commenting out bitbucket pipelines until ready for publishing\n",
                "added": 3,
                "author": "lwu613",
                "deleted": None,
                "date": str(yesterday.year) + "-" + str(yesterday.month) + "-" + str(yesterday.day) + " 15:20:16",
                "diff": "diff --git a/bitbucket-pipelines.yml b/bitbucket-pipelines.yml\nindex 2b1364c..560e699 100644\n--- a/bitbucket-pipelines.yml\n+++ b/bitbucket-pipelines.yml\n@@ -9,5 +9,9 @@ pipelines:\n   default:\n     - step:\n         script: # Modify the commands below to build your repository.\n-          - npm install\n-          - python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n+          - cd configurationpage\n+          # - npm install\n+          # - cd ../Monitor\n+          # - npm install\n+          # - cd ..\n+          #- python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n"
            },
            {
                "_id": "virtualdevmanager_code_0292602",
                "_rev": "2-a9dda832563a8bf3b61ca631b19aa6c4",
                "comment": "commenting out bitbucket pipelines until ready for publishing\n",
                "added": 3,
                "author": "tim",
                "deleted": None,
                "date": str(yesterday.year) + "-" + str(yesterday.month) + "-" + str(yesterday.day) + " 15:20:16",
                "diff": "diff --git a/bitbucket-pipelines.yml b/bitbucket-pipelines.yml\nindex 2b1364c..560e699 100644\n--- a/bitbucket-pipelines.yml\n+++ b/bitbucket-pipelines.yml\n@@ -9,5 +9,9 @@ pipelines:\n   default:\n     - step:\n         script: # Modify the commands below to build your repository.\n-          - npm install\n-          - python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n+          - cd configurationpage\n+          # - npm install\n+          # - cd ../Monitor\n+          # - npm install\n+          # - cd ..\n+          #- python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n"
            }

        ]

        data = charts.create_modification_activity_per_member(input_data)
        num_lines_added_by_member = {"lwu613": 73, "yuheng": 6, "xiaoleip": 10, "tim": 3}
        num_lines_deleted_by_member = {"lwu613": 7, "yuheng": 2, "xiaoleip": 1, "tim": 0}

        expected_data = {
            'x_axis_title': "Team Members",
            'y_axis_title': "Lines of Code",
            'categories': num_lines_added_by_member.keys(),
            'added': num_lines_added_by_member.values(),
            'deleted': num_lines_deleted_by_member.values()
        }

        self.assertEqual(data, expected_data)

    # Tests charts.create_modification_activity_per_member
    def test_create_modification_activity_per_member_2(self):
        input_data = []

        data = charts.create_modification_activity_per_member(input_data)
        num_lines_added_by_member = {}
        num_lines_deleted_by_member = {}

        expected_data = {
            'x_axis_title': "Team Members",
            'y_axis_title': "Lines of Code",
            'categories': num_lines_added_by_member.keys(),
            'added': num_lines_added_by_member.values(),
            'deleted': num_lines_deleted_by_member.values()
        }

        self.assertEqual(data, expected_data)

    # Tests charts.create_modification_activity_per_member
    def test_create_modification_activity_per_member_3(self):
        input_data = None

        data = charts.create_modification_activity_per_member(input_data)
        num_lines_added_by_member = {}
        num_lines_deleted_by_member = {}

        expected_data = {
            'x_axis_title': "Team Members",
            'y_axis_title': "Lines of Code",
            'categories': num_lines_added_by_member.keys(),
            'added': num_lines_added_by_member.values(),
            'deleted': num_lines_deleted_by_member.values()
        }

        self.assertEqual(data, expected_data)

    # Tests charts.create_commit_case_activity
    def test_create_commit_case_activity(self):
        input_data = [
            {
                "_id": "virtualdevmanager_code_0292602",
                "_rev": "2-a9dda832563a8bf3b61ca631b19aa6c4",
                "comment": "commenting out bitbucket pipelines until ready for publishing\n",
                "added": 6,
                "author": "lwu613",
                "deleted": 2,
                "date": "2017-07-25 19:24:43",
                "diff": "diff --git a/bitbucket-pipelines.yml b/bitbucket-pipelines.yml\nindex 2b1364c..560e699 100644\n--- a/bitbucket-pipelines.yml\n+++ b/bitbucket-pipelines.yml\n@@ -9,5 +9,9 @@ pipelines:\n   default:\n     - step:\n         script: # Modify the commands below to build your repository.\n-          - npm install\n-          - python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n+          - cd configurationpage\n+          # - npm install\n+          # - cd ../Monitor\n+          # - npm install\n+          # - cd ..\n+          #- python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n"
            },
            {
                "_id": "virtualdevmanager_code_1eca53b",
                "_rev": "2-d02252ec82a3c06c155e5aeac78bcdf2",
                "comment": "VDM-464: removing unit test configuration for now\n",
                "added": 1,
                "author": "lwu613",
                "deleted": 8,
                "date": "2017-07-23 19:22:50",
                "diff": "diff --git a/bitbucket-pipelines.yml b/bitbucket-pipelines.yml\nindex 4402459..65c0f07 100644\n--- a/bitbucket-pipelines.yml\n+++ b/bitbucket-pipelines.yml\n@@ -15,4 +15,4 @@ pipelines:\n           - cd ../Monitor\n           - npm install\n           - cd ..\n-          - python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n+          #- python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\ndiff --git a/tests/settings.py b/tests/settings.py\nindex 138eac1..1c6da58 100644\n--- a/tests/settings.py\n+++ b/tests/settings.py\n@@ -5,12 +5,6 @@ import struct\n import sys\n import logging\n \n-formatter = logging.Formatter(fmt='%(asctime)s-%(levelname)s-%(module)s-%(message)s')\n-handler = logging.FileHandler('/vagrant/dataprocessing/logs/settings.log')\n-handler.setFormatter(formatter)\n-logger = logging.getLogger('root')\n-logger.setLevel(logging.WARNING)\n-logger.addHandler(handler)\n \n SECRET_KEY = 'l3fm#x$+uhp3qlvd3bt9o&3q-@f26%$ys-b(tq^u5^dxmq&7#q'\n \n@@ -21,7 +15,6 @@ def get_ip_address(ifname):\n             s.fileno(), 0x8915, struct.pack('256s', ifname[:15])\n             )[20:24])\n     except Exception as e:\n-        logger.error('dataprocessing.settings.get_ip_address - %s' % e)\n         return \"0.0.0.0\"\n \n IP_ADDRESS = get_ip_address('eth1')\n"
            },
            {
                "_id": "virtualdevmanager_code_f5b10d2",
                "_rev": "2-6ab98e2bee275ab1ee597008c59ba0d4",
                "comment": "fixing bitbucket.yml issue\n",
                "added": 2,
                "author": "lwu613",
                "deleted": 2,
                "date": "2017-07-26 19:10:01",
                "diff": "diff --git a/bitbucket-pipelines.yml b/bitbucket-pipelines.yml\nindex 44e252b..4402459 100644\n--- a/bitbucket-pipelines.yml\n+++ b/bitbucket-pipelines.yml\n@@ -12,7 +12,7 @@ pipelines:\n           - cd configurationpage\n           - npm install\n           - npm test\n-          - cd Monitor\n+          - cd ../Monitor\n           - npm install\n-          - cd code\n+          - cd ..\n           - python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n"
            },
            {
                "_id": "virtualdevmanager_code_b01091d",
                "_rev": "2-218290768fdc2024bc61b35a80ba135c",
                "comment": "trying bitbucket yml edits\n",
                "added": 5,
                "author": "lwu613",
                "deleted": 1,
                "date": "2017-07-26 19:08:34",
                "diff": "diff --git a/bitbucket-pipelines.yml b/bitbucket-pipelines.yml\nindex 3bf208f..44e252b 100644\n--- a/bitbucket-pipelines.yml\n+++ b/bitbucket-pipelines.yml\n@@ -11,4 +11,8 @@ pipelines:\n         script: # Modify the commands below to build your repository.\n           - cd configurationpage\n           - npm install\n-          - npm test\n\\ No newline at end of file\n+          - npm test\n+          - cd Monitor\n+          - npm install\n+          - cd code\n+          - python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n"
            },
            {
                "_id": "virtualdevmanager_code_0999b44",
                "_rev": "2-b8fe845a13f519d981b532cc73c423be",
                "comment": "adding unit test run to bitbucket.yml\n",
                "added": 1,
                "author": "lwu613",
                "deleted": 1,
                "date": "2017-07-26 07:06:09",
                "diff": "diff --git a/bitbucket-pipelines.yml b/bitbucket-pipelines.yml\nindex 8dc997d..2b1364c 100644\n--- a/bitbucket-pipelines.yml\n+++ b/bitbucket-pipelines.yml\n@@ -10,4 +10,4 @@ pipelines:\n     - step:\n         script: # Modify the commands below to build your repository.\n           - npm install\n-          - npm test\n\\ No newline at end of file\n+          - python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n"
            },
            {
                "_id": "virtualdevmanager_code_0999b18",
                "_rev": "2-b8fe845a13f519d981b532cc73c423be",
                "comment": "adding unit test run to bitbucket.yml\n",
                "added": 1,
                "author": "lwu613",
                "deleted": 1,
                "date": None,
                "diff": "diff --git a/bitbucket-pipelines.yml b/bitbucket-pipelines.yml\nindex 8dc997d..2b1364c 100644\n--- a/bitbucket-pipelines.yml\n+++ b/bitbucket-pipelines.yml\n@@ -10,4 +10,4 @@ pipelines:\n     - step:\n         script: # Modify the commands below to build your repository.\n           - npm install\n-          - npm test\n\\ No newline at end of file\n+          - python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n"
            }
        ]

        data = charts.create_commit_case_activity(input_data, "virtualdevmanager_code")
        expected_data = {

            'last_commits': [
                (
                    '2017-07-26 19:10:01',
                    {
                        'author': 'lwu613',
                        'comment': 'fixing bitbucket.yml issue\n',
                        'id': 'f5b10d2'
                    }
                ),
                (
                    '2017-07-26 19:08:34',
                    {
                        'author': 'lwu613',
                        'comment': 'trying bitbucket yml edits\n',
                        'id': 'b01091d'
                    }
                ),
                (
                    '2017-07-26 07:06:09',
                    {
                        'author': 'lwu613',
                        'comment': 'adding unit test run to bitbucket.yml\n',
                        'id': '0999b44'
                    }
                ),
                (
                    '2017-07-25 19:24:43',
                    {
                        'author': 'lwu613',
                        'comment': 'commenting out bitbucket pipelines until ready for publishing\n',
                        'id': '0292602'
                    }
                ),
                (
                    '2017-07-23 19:22:50',
                    {
                        'author': 'lwu613',
                        'comment': 'VDM-464: removing unit test configuration for now\n',
                        'id': '1eca53b'
                    }
                )
            ],
            'project_info_to_return':
                {
                    'repo_id': 'code',
                    'team_id': 'virtualdevmanager'
                }

        }
        self.assertEqual(data, expected_data)

    # Tests charts.create_commit_case_activity
    def test_create_commit_case_activity_2(self):
        input_data = [
            {
                "_id": "virtualdevmanager_code_0292602",
                "_rev": "2-a9dda832563a8bf3b61ca631b19aa6c4",
                "comment": "commenting out bitbucket pipelines until ready for publishing\n",
                "added": 6,
                "author": "lwu613",
                "deleted": 2,
                "date": "2017-07-25 19:24:43",
                "diff": "diff --git a/bitbucket-pipelines.yml b/bitbucket-pipelines.yml\nindex 2b1364c..560e699 100644\n--- a/bitbucket-pipelines.yml\n+++ b/bitbucket-pipelines.yml\n@@ -9,5 +9,9 @@ pipelines:\n   default:\n     - step:\n         script: # Modify the commands below to build your repository.\n-          - npm install\n-          - python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n+          - cd configurationpage\n+          # - npm install\n+          # - cd ../Monitor\n+          # - npm install\n+          # - cd ..\n+          #- python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n"
            },
            {
                "_id": "virtualdevmanager_code_1eca53b",
                "_rev": "2-d02252ec82a3c06c155e5aeac78bcdf2",
                "comment": "VDM-464: removing unit test configuration for now\n",
                "added": 1,
                "author": "lwu613",
                "deleted": 8,
                "date": "2017-07-23 19:22:50",
                "diff": "diff --git a/bitbucket-pipelines.yml b/bitbucket-pipelines.yml\nindex 4402459..65c0f07 100644\n--- a/bitbucket-pipelines.yml\n+++ b/bitbucket-pipelines.yml\n@@ -15,4 +15,4 @@ pipelines:\n           - cd ../Monitor\n           - npm install\n           - cd ..\n-          - python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n+          #- python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\ndiff --git a/tests/settings.py b/tests/settings.py\nindex 138eac1..1c6da58 100644\n--- a/tests/settings.py\n+++ b/tests/settings.py\n@@ -5,12 +5,6 @@ import struct\n import sys\n import logging\n \n-formatter = logging.Formatter(fmt='%(asctime)s-%(levelname)s-%(module)s-%(message)s')\n-handler = logging.FileHandler('/vagrant/dataprocessing/logs/settings.log')\n-handler.setFormatter(formatter)\n-logger = logging.getLogger('root')\n-logger.setLevel(logging.WARNING)\n-logger.addHandler(handler)\n \n SECRET_KEY = 'l3fm#x$+uhp3qlvd3bt9o&3q-@f26%$ys-b(tq^u5^dxmq&7#q'\n \n@@ -21,7 +15,6 @@ def get_ip_address(ifname):\n             s.fileno(), 0x8915, struct.pack('256s', ifname[:15])\n             )[20:24])\n     except Exception as e:\n-        logger.error('dataprocessing.settings.get_ip_address - %s' % e)\n         return \"0.0.0.0\"\n \n IP_ADDRESS = get_ip_address('eth1')\n"
            },
            {
                "_id": "virtualdevmanager_code_f5b10d2",
                "_rev": "2-6ab98e2bee275ab1ee597008c59ba0d4",
                "comment": "fixing bitbucket.yml issue\n",
                "added": 2,
                "author": "lwu613",
                "deleted": 2,
                "date": "2017-07-26 19:10:01",
                "diff": "diff --git a/bitbucket-pipelines.yml b/bitbucket-pipelines.yml\nindex 44e252b..4402459 100644\n--- a/bitbucket-pipelines.yml\n+++ b/bitbucket-pipelines.yml\n@@ -12,7 +12,7 @@ pipelines:\n           - cd configurationpage\n           - npm install\n           - npm test\n-          - cd Monitor\n+          - cd ../Monitor\n           - npm install\n-          - cd code\n+          - cd ..\n           - python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n"
            }
        ]

        data = charts.create_commit_case_activity(input_data, "virtualdevmanager_code")
        expected_data = {

            'last_commits': [
                (
                    '2017-07-26 19:10:01',
                    {
                        'author': 'lwu613',
                        'comment': 'fixing bitbucket.yml issue\n',
                        'id': 'f5b10d2'
                    }
                ),
                (
                    '2017-07-25 19:24:43',
                    {
                        'author': 'lwu613',
                        'comment': 'commenting out bitbucket pipelines until ready for publishing\n',
                        'id': '0292602'
                    }
                ),
                (
                    '2017-07-23 19:22:50',
                    {
                        'author': 'lwu613',
                        'comment': 'VDM-464: removing unit test configuration for now\n',
                        'id': '1eca53b'
                    }
                )
            ],
            'project_info_to_return':
                {
                    'repo_id': 'code',
                    'team_id': 'virtualdevmanager'
                }

        }
        self.assertEqual(data, expected_data)

    # Tests charts.create_commit_case_activity
    def test_create_commit_case_activity_3(self):
        input_data = [
            {
                "_id": "virtualdevmanager_code_0292602",
                "_rev": "2-a9dda832563a8bf3b61ca631b19aa6c4",
                "comment": "commenting out bitbucket pipelines until ready for publishing\n",
                "added": 6,
                "author": "lwu613",
                "deleted": 2,
                "date": "2017-07-25 19:24:43",
                "diff": "diff --git a/bitbucket-pipelines.yml b/bitbucket-pipelines.yml\nindex 2b1364c..560e699 100644\n--- a/bitbucket-pipelines.yml\n+++ b/bitbucket-pipelines.yml\n@@ -9,5 +9,9 @@ pipelines:\n   default:\n     - step:\n         script: # Modify the commands below to build your repository.\n-          - npm install\n-          - python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n+          - cd configurationpage\n+          # - npm install\n+          # - cd ../Monitor\n+          # - npm install\n+          # - cd ..\n+          #- python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n"
            },
            {
                "_id": "virtualdevmanager_code_1eca53b",
                "_rev": "2-d02252ec82a3c06c155e5aeac78bcdf2",
                "comment": "VDM-464: removing unit test configuration for now\n",
                "added": 1,
                "author": "lwu613",
                "deleted": 8,
                "date": "2017-07-23 19:22:50",
                "diff": "diff --git a/bitbucket-pipelines.yml b/bitbucket-pipelines.yml\nindex 4402459..65c0f07 100644\n--- a/bitbucket-pipelines.yml\n+++ b/bitbucket-pipelines.yml\n@@ -15,4 +15,4 @@ pipelines:\n           - cd ../Monitor\n           - npm install\n           - cd ..\n-          - python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n+          #- python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\ndiff --git a/tests/settings.py b/tests/settings.py\nindex 138eac1..1c6da58 100644\n--- a/tests/settings.py\n+++ b/tests/settings.py\n@@ -5,12 +5,6 @@ import struct\n import sys\n import logging\n \n-formatter = logging.Formatter(fmt='%(asctime)s-%(levelname)s-%(module)s-%(message)s')\n-handler = logging.FileHandler('/vagrant/dataprocessing/logs/settings.log')\n-handler.setFormatter(formatter)\n-logger = logging.getLogger('root')\n-logger.setLevel(logging.WARNING)\n-logger.addHandler(handler)\n \n SECRET_KEY = 'l3fm#x$+uhp3qlvd3bt9o&3q-@f26%$ys-b(tq^u5^dxmq&7#q'\n \n@@ -21,7 +15,6 @@ def get_ip_address(ifname):\n             s.fileno(), 0x8915, struct.pack('256s', ifname[:15])\n             )[20:24])\n     except Exception as e:\n-        logger.error('dataprocessing.settings.get_ip_address - %s' % e)\n         return \"0.0.0.0\"\n \n IP_ADDRESS = get_ip_address('eth1')\n"
            },
            {
                "_id": "virtualdevmanager_code_f5b10d2",
                "_rev": "2-6ab98e2bee275ab1ee597008c59ba0d4",
                "comment": "fixing bitbucket.yml issue\n",
                "added": 2,
                "author": "lwu613",
                "deleted": 2,
                "date": "2017-07-26 19:10:01",
                "diff": "diff --git a/bitbucket-pipelines.yml b/bitbucket-pipelines.yml\nindex 44e252b..4402459 100644\n--- a/bitbucket-pipelines.yml\n+++ b/bitbucket-pipelines.yml\n@@ -12,7 +12,7 @@ pipelines:\n           - cd configurationpage\n           - npm install\n           - npm test\n-          - cd Monitor\n+          - cd ../Monitor\n           - npm install\n-          - cd code\n+          - cd ..\n           - python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n"
            }
        ]

        data = charts.create_commit_case_activity(input_data, "virtualdevmanager")
        expected_data = {}
        self.assertEqual(data, expected_data)

    # Tests charts.create_commit_case_activity
    def test_create_commit_case_activity_4(self):
        input_data = [
            {
                "_id": "virtualdevmanager_code",
                "_rev": "2-a9dda832563a8bf3b61ca631b19aa6c4",
                "comment": "commenting out bitbucket pipelines until ready for publishing\n",
                "added": 6,
                "author": "lwu613",
                "deleted": 2,
                "date": "2017-07-25 19:24:43",
                "diff": "diff --git a/bitbucket-pipelines.yml b/bitbucket-pipelines.yml\nindex 2b1364c..560e699 100644\n--- a/bitbucket-pipelines.yml\n+++ b/bitbucket-pipelines.yml\n@@ -9,5 +9,9 @@ pipelines:\n   default:\n     - step:\n         script: # Modify the commands below to build your repository.\n-          - npm install\n-          - python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n+          - cd configurationpage\n+          # - npm install\n+          # - cd ../Monitor\n+          # - npm install\n+          # - cd ..\n+          #- python -m unittest discover -s ./tests -p '*Tests.py'\n\\ No newline at end of file\n"
            }
        ]

        data = charts.create_commit_case_activity(input_data, "virtualdevmanager_code")
        expected_data = {
            'last_commits': [],
            'project_info_to_return': {'repo_id': 'code', 'team_id': 'virtualdevmanager'}
        }
        self.assertEqual(data, expected_data)

    # Tests charts.create_commit_case_activity
    def test_create_commit_case_activity_5(self):
        input_data = []

        data = charts.create_commit_case_activity(input_data, "virtualdevmanager_code")
        expected_data = {
            'last_commits': [],
            'project_info_to_return': {'repo_id': 'code', 'team_id': 'virtualdevmanager'}
        }
        self.assertEqual(data, expected_data)

    # Tests charts.create_commit_case_activity
    def test_create_commit_case_activity_6(self):
        input_data = None

        data = charts.create_commit_case_activity(input_data, "virtualdevmanager_code")
        expected_data = {
            'last_commits': [],
            'project_info_to_return': {'repo_id': 'code', 'team_id': 'virtualdevmanager'}
        }
        self.assertEqual(data, expected_data)
