import os
import socket
import fcntl
import struct
import sys
import logging


SECRET_KEY = 'l3fm#x$+uhp3qlvd3bt9o&3q-@f26%$ys-b(tq^u5^dxmq&7#q'

def get_ip_address(ifname):
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        return socket.inet_ntoa(fcntl.ioctl(
            s.fileno(), 0x8915, struct.pack('256s', ifname[:15])
            )[20:24])
    except Exception as e:
        return "0.0.0.0"

IP_ADDRESS = get_ip_address('eth1')

COUCH_DB_URL = "http://" + get_ip_address('eth1') + ":5980/"