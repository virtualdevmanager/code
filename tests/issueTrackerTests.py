import sys
import os
import unittest
sys.path.insert(0, os.path.abspath('..'))  # Path hack to access sibling packages
from dataprocessing.issues.models import Issue
import os.path
if not os.path.exists("vagrant/vdm/main/logs"):
    os.makedirs("vagrant/vdm/main/logs")
from dataprocessing.issues import charts
from vdm.issueTracker import views
import settings
import couchdb

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
couch = couchdb.Server(settings.COUCH_DB_URL)

burndown_doc = {"_id": "GREEN_burndown", "_rev": "8-3aa26b335932ae192bceb9b4bce7d285", "x_axis": "value",
                    "y_axis_title": "Story Points", "x_axis_title": "Date",
                    "estimated": [87.0, 79.090909090909093493, 71.181818181818186986, 63.272727272727280479,
                                  55.363636363636373972, 47.454545454545467464, 39.545454545454560957,
                                  31.63636363636365445, 23.727272727272747943, 15.818181818181841436,
                                  7.9090909090909349288, 2.8421709430404007435e-14],
                    "remaining": [84.0, 84.0, 84.0, 84.0, 76.0],
                    "categories": ["7/17/2017", "7/18/2017", "7/19/2017", "7/20/2017", "7/21/2017", "7/22/2017",
                                   "7/23/2017", "7/24/2017", "7/25/2017", "7/26/2017", "7/27/2017", "7/28/2017"]}

case_status_doc = {"_id": "GREEN_case_status", "_rev": "12-d1e6c3cb08678596844d711b6243b8cc",
                       "x_axis": ["tleng", "junyuan", "shanh2", "ranantha"], "y_axis_title": "Number of Issues",
                       "x_axis_title": "Developers", "closed": [33, 31, 34, 30], "in_progress": [3, 1, 1, 0],
                       "estimated": [22, 24, 30, 24], "open": [0, 0, 1, 0], "not_estimated": [14, 8, 6, 6]}

priority_doc = {"_id":"GREEN_issues_count_by_priority","_rev":"13-b47e57fd8edf2bab2ead30b53ac27737",
                    "high":0,"medium":0,"low":0}

issue_status_doc = {"_id":"GREEN_issues_status","_rev":"13-780c7c05df5e5680408de4f574079fe4",
                        "progress":7,"open":12,"closed":139}

proj_burndown = {"_id": "GREEN_project_burndown", "_rev": "14-7ec3bb6df04165148a9cb7d9a740359e", "x_axis": "value",
     "y_axis_title": "Story Points", "x_axis_title": "Date",
     "estimated": [473.76666666666665151, 470.84218106995882636, 467.9176954732510012, 464.99320987654317605,
                   462.0687242798353509, 459.14423868312752575, 456.21975308641970059, 453.29526748971187544,
                   450.37078189300405029],
     "remaining": [473.76666666666665151, 473.76666666666665151, 473.76666666666665151, 473.76666666666665151,
                   468.76666666666665151, 465.76666666666665151, 465.76666666666665151, 465.76666666666665151,
                   465.76666666666665151],
     "categories": ["2/15/2017", "2/16/2017", "2/17/2017", "2/18/2017", "2/19/2017", "2/20/2017", "2/21/2017",
                    "2/22/2017", "2/23/2017"]}

project_progress_doc = {"_id":"GREEN_project_progress","_rev":"15-8c558905ce3e383d5f4e190ee13a03af",
                            "progress":87.974683544303800886}

sprint_case_status_doc = {"_id":"GREEN_sprint_case_status","_rev":"16-468d49345ed5a7c0e62d47aaf6056161",
                          "x_axis":["tleng","junyuan","ranantha"],"y_axis_title":"Number of Issues",
                          "x_axis_title":"Developers","closed":[1,0,1],"in_progress":[2,1,0],
                          "estimated":[3,1,1],"open":[0,0,0],"not_estimated":[0,0,0]}

workload_doc = {"_id":"GREEN_workload",
                    "_rev":"17-63acebbbd7fb41719032fffe2f57bbb5",
                    "categories":["tleng","ranantha","junyuan"], "lists":[[8.0,0.0,8.0],[0.0,0.0,0.0],[0.0,0.0,0.0]]}


class TestIssueTrackerView(unittest.TestCase):
    # Setup method run before every test case
    @classmethod
    def setUpClass(cls):
        cls.issue = Issue()

    # Teardown method run before every test case
    @classmethod
    def tearDownClass(cls):
        couch.delete('projects_data')

    # Tests that the correct issues can be returned based on raw issues data stored
    def test_data_prime(self):
        global burndown_doc
        global case_status_doc
        global priority_doc
        global issue_status_doc
        global proj_burndown
        global project_progress_doc
        global sprint_case_status_doc
        global workload_doc

        if 'projects_data' in couch:
            db = couch['projects_data']
        else:
            db = couch.create('projects_data')  # Create test data

        db.save(burndown_doc)
        db.save(case_status_doc)
        db.save(priority_doc)
        db.save(issue_status_doc)
        db.save(proj_burndown)
        db.save(project_progress_doc)
        db.save(sprint_case_status_doc)
        db.save(workload_doc)
        data = views.get_issues("GREEN")
        expected_data = {
            'status': {'progress': 7, '_rev': '14-c1a35a4a5df74937a1de4c90f9e01a5d', '_id': 'GREEN_issues_status',
                    'open': 12, 'closed': 139},
            'issue_by_priority': {'high': 0, '_rev': '14-4d9558fe0236e3345788a63421b9a5d5',
                               '_id': 'GREEN_issues_count_by_priority', 'medium': 0, 'low': 0},
            'workload': {'_rev': '18-d95c2acffd67bfa750443521cd885012', '_id': 'GREEN_workload',
                      'categories': ['tleng', 'ranantha', 'junyuan'],
                      'lists': [[8.0, 0.0, 8.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]]},
            'sprint_case_status': {'estimated': [3, 1, 1], '_rev': '17-ee27539256965c2ba5af11500cbc74ad',
                                'y_axis_title': 'Number of Issues', 'x_axis_title': 'Developers', 'closed': [1, 0, 1],
                                'in_progress': [2, 1, 0], '_id': 'GREEN_sprint_case_status', 'open': [0, 0, 0],
                                'not_estimated': [0, 0, 0], 'x_axis': ['tleng', 'junyuan', 'ranantha']},
            'project_burndown': {'estimated': [473.76666666666665, 470.8421810699588, 467.917695473251,
                                               464.9932098765432,462.06872427983535, 459.1442386831275,
                                               456.2197530864197, 453.2952674897119, 450.37078189300405],
                                 '_rev': '15-c95ca3a0508211f5c3f6d9ccfe0cd2c8',
                              'y_axis_title': 'Story Points', 'x_axis_title': 'Date', '_id': 'GREEN_project_burndown',
                              'remaining': [473.76666666666665, 473.76666666666665, 473.76666666666665,
                                            473.76666666666665, 468.76666666666665, 465.76666666666665,
                                            465.76666666666665, 465.76666666666665, 465.76666666666665],
                              'categories': ['2/15/2017', '2/16/2017', '2/17/2017', '2/18/2017', '2/19/2017',
                                             '2/20/2017', '2/21/2017', '2/22/2017', '2/23/2017'], 'x_axis': 'value'},
            'case_status': {'estimated': [22, 24, 30, 24], '_rev': '13-7e17ceade2f90b3f4bbc79855d00adb8',
                         'y_axis_title': 'Number of Issues', 'x_axis_title': 'Developers', 'closed': [33, 31, 34, 30],
                         'in_progress': [3, 1, 1, 0], '_id': 'GREEN_case_status', 'open': [0, 0, 1, 0],
                         'not_estimated': [14, 8, 6, 6], 'x_axis': ['tleng', 'junyuan', 'shanh2', 'ranantha']},
            'burn_down': {'estimated': [87.0, 79.0909090909091, 71.18181818181819, 63.27272727272728,
                                        55.363636363636374,47.45454545454547, 39.54545454545456, 31.636363636363654,
                                        23.727272727272748,15.818181818181841, 7.909090909090935,
                                        2.842170943040401e-14],
                       '_rev': '9-245f147cfc6763c41c480661b9174742', 'y_axis_title': 'Story Points',
                       'x_axis_title': 'Date', '_id': 'GREEN_burndown', 'remaining': [84.0, 84.0, 84.0, 84.0, 76.0],
                       'categories': ['7/17/2017', '7/18/2017', '7/19/2017', '7/20/2017', '7/21/2017', '7/22/2017',
                                      '7/23/2017', '7/24/2017', '7/25/2017', '7/26/2017', '7/27/2017', '7/28/2017'],
                       'x_axis': 'value'}}
        self.assertEqual(data, expected_data)

    # Tests charts.create_issue_count_by_priority
    def test_create_issues_count_by_priority(self):
        input_data = [{"status": "In Progress", "_id": "vdm_1", "_rev": "14-5cb627900cf9c7f0450c32dafc00cec7", "resolve_date": "2016-06-30T20:10:16.000-0400", "logged": "12", "due": "2016-06-30T20:10:16.000-0400", "summary": "Implement Monitor DAO", "priority": "High", "assignee": "Gunjan Raghav", "sprint": 1, "creation_date": "2016-06-27T20:10:16.000-0400", "estimate": None}, {"status": "Closed", "_id": "vdm_2", "_rev": "5-5943e1cbfe642862c750a359227034df", "resolve_date": "2016-06-28T20:10:16.000-0400", "logged": "5", "due": "2016-06-30T20:10:16.000-0400", "summary": "Implement Monitor DTO", "priority": "Medium", "assignee": "Anurag Kanungo", "sprint": 1, "creation_date": "2016-06-27T20:10:16.000-0400", "estimate": "6"}, {"status": "Open", "_id": "vdm_3", "_rev": "5-b0086379a076c06825ed2c9b32baf72e", "resolve_date": None, "logged": "5", "due": "2016-06-30T20:10:16.000-0400", "summary": "Security Layer", "priority": "Low", "assignee": "Anurag Kanungo", "sprint": 1, "creation_date": "2016-06-27T20:10:16.000-0400", "estimate": "6"}, {"status": "Closed", "_id": "vdm_4", "_rev": "3-4b81480ffb2d9a84d51b1cf148609077", "resolve_date": "2016-06-29T20:10:16.000-0400", "logged": "5", "due": "2016-06-30T20:10:16.000-0400", "summary": "Data access layer", "priority": "High", "assignee": "Xiao Bao", "sprint": 1, "creation_date": "2016-06-27T20:10:16.000-0400", "estimate": "6"}]
        data = charts.create_issues_count_by_priority(input_data)
        expected_data = {'high': 1, 'medium': 0, 'low': 1}
        self.assertEqual(data, expected_data)

    # Tests another variation of charts.create_issue_count_by_priority
    def test_create_issues_count_by_priority2(self):
        input_data = [{"status": "In Progress", "_id": "vdm_1", "_rev": "14-5cb627900cf9c7f0450c32dafc00cec7", "resolve_date": "2016-06-30T20:10:16.000-0400", "logged": "12", "due": "2016-06-30T20:10:16.000-0400", "summary": "Implement Monitor DAO", "priority": "High", "assignee": "Gunjan Raghav", "sprint": 1, "creation_date": "2016-06-27T20:10:16.000-0400", "estimate": None}, {"status": "Closed", "_id": "vdm_2", "_rev": "5-5943e1cbfe642862c750a359227034df", "resolve_date": "2016-06-28T20:10:16.000-0400", "logged": "5", "due": "2016-06-30T20:10:16.000-0400", "summary": "Implement Monitor DTO", "priority": "Medium", "assignee": "Anurag Kanungo", "sprint": 1, "creation_date": "2016-06-27T20:10:16.000-0400", "estimate": "6"}, {"status": "Open", "_id": "vdm_3", "_rev": "5-b0086379a076c06825ed2c9b32baf72e", "resolve_date": None, "logged": "5", "due": "2016-06-30T20:10:16.000-0400", "summary": "Security Layer", "priority": "Medium", "assignee": "Anurag Kanungo", "sprint": 1, "creation_date": "2016-06-27T20:10:16.000-0400", "estimate": "6"}, {"status": "Closed", "_id": "vdm_4", "_rev": "3-4b81480ffb2d9a84d51b1cf148609077", "resolve_date": "2016-06-29T20:10:16.000-0400", "logged": "5", "due": "2016-06-30T20:10:16.000-0400", "summary": "Data access layer", "priority": "High", "assignee": "Xiao Bao", "sprint": 1, "creation_date": "2016-06-27T20:10:16.000-0400", "estimate": "6"}]
        data = charts.create_issues_count_by_priority(input_data)
        expected_data = {'high': 1, 'medium': 1, 'low': 0}
        self.assertEqual(data, expected_data)

    # Tests charts.create_issue_count_by_priority with no data passed in
    def test_create_issues_count_by_priority_no_data(self):
        input_data = None
        data = charts.create_issues_count_by_priority(input_data)
        expected_data = {}
        self.assertEqual(data, expected_data)

    # Tests charts.create_case_status
    def test_create_case_status(self):
        input_data = [{"status": "Closed", "_id": "vdm_1", "_rev": "14-5cb627900cf9c7f0450c32dafc00cec7", "resolve_date": "2016-06-30T20:10:16.000-0400", "logged": "12", "due": "2016-06-30T20:10:16.000-0400", "summary": "Implement Monitor DAO", "priority": "High", "assignee": "Gunjan Raghav", "sprint": 1, "creation_date": "2016-06-27T20:10:16.000-0400", "estimate": None}, {"status": "Closed", "_id": "vdm_2", "_rev": "5-5943e1cbfe642862c750a359227034df", "resolve_date": "2016-06-28T20:10:16.000-0400", "logged": "5", "due": "2016-06-30T20:10:16.000-0400", "summary": "Implement Monitor DTO", "priority": "Medium", "assignee": "Anurag Kanungo", "sprint": 1, "creation_date": "2016-06-27T20:10:16.000-0400", "estimate": "6"}, {"status": "Closed", "_id": "vdm_3", "_rev": "5-b0086379a076c06825ed2c9b32baf72e", "resolve_date": None, "logged": "5", "due": "2016-06-30T20:10:16.000-0400", "summary": "Security Layer", "priority": "Medium", "assignee": "Anurag Kanungo", "sprint": 1, "creation_date": "2016-06-27T20:10:16.000-0400", "estimate": "6"}, {"status": "Closed", "_id": "vdm_4", "_rev": "3-4b81480ffb2d9a84d51b1cf148609077", "resolve_date": "2016-06-29T20:10:16.000-0400", "logged": "5", "due": "2016-06-30T20:10:16.000-0400", "summary": "Data access layer", "priority": "High", "assignee": "Xiao Bao", "sprint": 1, "creation_date": "2016-06-27T20:10:16.000-0400", "estimate": "6"}]
        data = charts.create_case_status(input_data)
        expected_data = {'x_axis_title': 'Developers', 'closed': [2, 1, 1], 'open': [0, 0, 0], 'x_axis': ['Anurag Kanungo', 'Gunjan Raghav', 'Xiao Bao'], 'estimated': [2, 0, 1], 'y_axis_title': 'Number of Issues', 'in_progress': [0, 0, 0], 'not_estimated': [0, 1, 0]}
        self.assertEqual(data, expected_data)

    # Tests second variation of input data for charts.create_case_status
    def test_create_case_status2(self):
        input_data = [{"status": "Open", "_id": "vdm_1", "_rev": "14-5cb627900cf9c7f0450c32dafc00cec7", "resolve_date": "2016-06-30T20:10:16.000-0400", "logged": "12", "due": "2016-06-30T20:10:16.000-0400", "summary": "Implement Monitor DAO", "priority": "High", "assignee": "Gunjan Raghav", "sprint": 1, "creation_date": "2016-06-27T20:10:16.000-0400", "estimate": None}, {"status": "Open", "_id": "vdm_2", "_rev": "5-5943e1cbfe642862c750a359227034df", "resolve_date": "2016-06-28T20:10:16.000-0400", "logged": "5", "due": "2016-06-30T20:10:16.000-0400", "summary": "Implement Monitor DTO", "priority": "Medium", "assignee": "Anurag Kanungo", "sprint": 1, "creation_date": "2016-06-27T20:10:16.000-0400", "estimate": "6"}, {"status": "Open", "_id": "vdm_3", "_rev": "5-b0086379a076c06825ed2c9b32baf72e", "resolve_date": None, "logged": "5", "due": "2016-06-30T20:10:16.000-0400", "summary": "Security Layer", "priority": "Medium", "assignee": "Anurag Kanungo", "sprint": 1, "creation_date": "2016-06-27T20:10:16.000-0400", "estimate": "6"}, {"status": "Open", "_id": "vdm_4", "_rev": "3-4b81480ffb2d9a84d51b1cf148609077", "resolve_date": "2016-06-29T20:10:16.000-0400", "logged": "5", "due": "2016-06-30T20:10:16.000-0400", "summary": "Data access layer", "priority": "High", "assignee": "Xiao Bao", "sprint": 1, "creation_date": "2016-06-27T20:10:16.000-0400", "estimate": "6"}]
        data = charts.create_case_status(input_data)
        expected_data = {'x_axis_title': 'Developers', 'closed': [0, 0, 0], 'open': [2, 1, 1], 'x_axis': ['Anurag Kanungo', 'Gunjan Raghav', 'Xiao Bao'], 'estimated': [2, 0, 1], 'y_axis_title': 'Number of Issues', 'in_progress': [0, 0, 0], 'not_estimated': [0, 1, 0]}
        self.assertEqual(data, expected_data)

    # Tests third variation of input data for charts.create_case_status
    def test_create_case_status3(self):
        input_data = [{"status": "In Progress", "_id": "vdm_1", "_rev": "14-5cb627900cf9c7f0450c32dafc00cec7", "resolve_date": "2016-06-30T20:10:16.000-0400", "logged": "12", "due": "2016-06-30T20:10:16.000-0400", "summary": "Implement Monitor DAO", "priority": "High", "assignee": "Gunjan Raghav", "sprint": 1, "creation_date": "2016-06-27T20:10:16.000-0400", "estimate": None}, {"status": "In Progress", "_id": "vdm_2", "_rev": "5-5943e1cbfe642862c750a359227034df", "resolve_date": "2016-06-28T20:10:16.000-0400", "logged": "5", "due": "2016-06-30T20:10:16.000-0400", "summary": "Implement Monitor DTO", "priority": "Medium", "assignee": "Anurag Kanungo", "sprint": 1, "creation_date": "2016-06-27T20:10:16.000-0400", "estimate": "6"}, {"status": "In Progress", "_id": "vdm_3", "_rev": "5-b0086379a076c06825ed2c9b32baf72e", "resolve_date": None, "logged": "5", "due": "2016-06-30T20:10:16.000-0400", "summary": "Security Layer", "priority": "Medium", "assignee": "Anurag Kanungo", "sprint": 1, "creation_date": "2016-06-27T20:10:16.000-0400", "estimate": "6"}, {"status": "In Progress", "_id": "vdm_4", "_rev": "3-4b81480ffb2d9a84d51b1cf148609077", "resolve_date": "2016-06-29T20:10:16.000-0400", "logged": "5", "due": "2016-06-30T20:10:16.000-0400", "summary": "Data access layer", "priority": "High", "assignee": "Xiao Bao", "sprint": 1, "creation_date": "2016-06-27T20:10:16.000-0400", "estimate": "6"}]
        data = charts.create_case_status(input_data)
        expected_data = {'x_axis_title': 'Developers', 'closed': [0, 0, 0], 'open': [0, 0, 0], 'x_axis': ['Anurag Kanungo', 'Gunjan Raghav', 'Xiao Bao'], 'estimated': [2, 0, 1], 'y_axis_title': 'Number of Issues', 'in_progress': [2, 1, 1], 'not_estimated': [0, 1, 0]}
        self.assertEqual(data, expected_data)

    # Tests charts.create_workload
    def test_create_workload(self):
        data = [ {u'status': u'In Progress',
                  u'estimate': u'46',
                  u'_rev': u'14-5cb627900cf9c7f0450c32dafc00cec7',
                  u'resolve_date': u'2016-06-30T20:10:16.000-0400',
                  u'logged': u'12',
                  u'due': u'2016-06-30T20:10:16.000-0400',
                  u'summary': u'Implement Monitor DAO',
                  u'priority': u'High',
                  u'assignee': u'Gunjan Raghav',
                  u'sprint': 1,
                  u'_id': u'vdm_1',
                  u'creation_date': u'2016-06-27T20:10:16.000-0400'
                  },
                 {u'status': u'Open',
                  u'estimate': u'0',
                  u'_rev': u'5-5943e1cbfe642862c750a359227034df',
                  u'resolve_date': u'2016-06-28T20:10:16.000-0400',
                  u'logged': u'5',
                  u'due': u'2016-06-30T20:10:16.000-0400',
                  u'summary': u'Implement Monitor DTO',
                  u'priority': u'High',
                  u'assignee': u'Anurag Kanungo',
                  u'sprint': 1,
                  u'_id': u'vdm_2',
                  u'creation_date': u'2016-06-27T20:10:16.000-0400'},
                 {u'status': u'Open',
                  u'estimate': u'6',
                  u'_rev': u'5-b0086379a076c06825ed2c9b32baf72e',
                  u'resolve_date': None, u'logged': u'5',
                  u'due': u'2016-06-30T20:10:16.000-0400',
                  u'summary': u'Security Layer',
                  u'priority': u'High',
                  u'assignee': u'Anurag Kanungo',
                  u'sprint': 1,
                  u'_id': u'vdm_3',
                  u'creation_date': u'2016-06-27T20:10:16.000-0400'},
                  {u'status': u'Open',
                   u'estimate': u'1',
                   u'_rev': u'3-4b81480ffb2d9a84d51b1cf148609077',
                   u'resolve_date': u'2016-06-29T20:10:16.000-0400',
                   u'logged': u'5',
                   u'due': u'2016-06-30T20:10:16.000-0400',
                   u'summary': u'Data access layer',
                   u'priority': u'High',
                   u'assignee': u'Xiao Bao',
                   u'sprint': 1,
                   u'_id': u'vdm_4',
                   u'creation_date': u'2016-06-27T20:10:16.000-0400'}]
        sprint = [{u'_rev': u'1-1cc356dfee9a47133174a92381b99f8a',
                    u'_id': u'vdm_1',
                    u'name': u'sprint 1',
                    u'end_date': u'2016-07-15T20:10:16.000-0400',
                    u'start_date': u'2016-06-27T20:10:16.000-0400'}]
        actual_data = charts.create_workload(data, sprint, "vdm")
        expected_data = {'lists': [[6.0, 20, 1.0], [0, 20, 0], [0, 6.0, 0]], 'categories': ['Anurag Kanungo', 'Gunjan Raghav', 'Xiao Bao']}
        self.assertEqual(actual_data, expected_data)

    # Tests different variation of input for charts.create_workload
    def test_create_workload2(self):
        data = []
        sprint = []
        actual_data = charts.create_workload(data, sprint, "vdm")
        expected_data = {'lists': [[], [], []], 'categories': []}
        self.assertEqual(actual_data, expected_data)

    # Tests charts.create_project_progress
    def test_create_project_progress(self):
        actual_data = charts.create_project_progress({'progress': 2, 'open': 3, 'closed': 9})
        expected_data = 64.28571428571429
        self.assertEqual(actual_data, {'progress': expected_data})

    # Tests charts.create_project_progress where null data is provided
    def test_create_project_progress_no_data(self):
        actual_data = charts.create_project_progress(None)
        expected_data = {}
        self.assertEqual(actual_data, expected_data)

    # Tests charts.create_issues_status
    def test_create_issues_status(self):
        input_data = [
                        {'status': 'Closed', 'resolve_date': '2016-02-18T00:54:58.694+0000',
                         'logged': 0, 'due': None, 'creation_date': '2016-01-25T15:49:00.000+0000',
                         'priority': 'Medium', 'assignee': 'graghav', 'sprint': 'VDMBOAR Sprint 1',
                         'summary': 'Review Top 2 risks and TOS', 'estimate': 1},
                        {'status': 'Open', 'resolve_date': '2016-02-18T00:54:58.694+0000',
                         'logged': 0, 'due': None, 'creation_date': '2016-01-25T15:49:00.000+0000',
                         'priority': 'Medium', 'assignee': 'graghav', 'sprint': 'VDMBOAR Sprint 1',
                         'summary': 'Review Top 2 risks and TOS', 'estimate': 1},
                        {'status': 'In Progress', 'resolve_date': '2016-02-18T00:54:58.694+0000',
                         'logged': 0, 'due': None, 'creation_date': '2016-01-25T15:49:00.000+0000',
                         'priority': 'Medium', 'assignee': 'graghav', 'sprint': 'VDMBOAR Sprint 1',
                         'summary': 'Review Top 2 risks and TOS', 'estimate': 1},
                        {'status': 'Closed', 'resolve_date': '2016-02-18T00:54:58.694+0000',
                         'logged': 0, 'due': None, 'creation_date': '2016-01-25T15:49:00.000+0000',
                         'priority': 'Medium', 'assignee': 'graghav', 'sprint': 'VDMBOAR Sprint 1',
                         'summary': 'Review Top 2 risks and TOS', 'estimate': 1}
                    ]
        actual_data = charts.create_issues_status(input_data)
        expected_data = {'open': 1,
                         'progress': 1,
                         'closed': 2}
        self.assertEqual(actual_data, expected_data)

    # Tests charts.get_project_date
    def test_get_project_date(self):
        input_data = [{'_id': "VDM_Sprint 05.24.2017 - 06.05.2017", '_rev': "3-77b183630f330d28c3d3db43c1d09a39",
                      'start_date': "24/May/17 10:42 PM", 'end_date': "05/Jun/17 10:42 PM"}]
        actual_data = charts.get_project_date(input_data)
        expected_data = ("24/May/17 10:42 PM", "05/Jun/17 10:42 PM")
        self.assertEqual(actual_data, expected_data)
