import sys
import os
import unittest
sys.path.insert(0, os.path.abspath('..'))  # Path hack to access sibling packages
from dataprocessing.builds.models import Build
import os.path
if not os.path.exists("vagrant/vdm/main/logs"):
    os.makedirs("vagrant/vdm/main/logs")
from dataprocessing.builds import charts
from vdm.buildServer import views
import settings
import couchdb
from datetime import datetime, timedelta
from dateutil import tz
from dateutil.parser import parse
DATE_RANGE = 7


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
couch = couchdb.Server(settings.COUCH_DB_URL)

builds_doc = {"_id": "VDM-VB_builds_status", "_rev": "1-aa55f4f7e8aee23f187089b49b44f16d",
              "successful": [0, 6, 6, 4, 0, 4, 1, 4], "y_axis_title": "Number of Builds",
              "failed": [0, 0, 0, 0, 0, 0, 0, 0], "x_axis_title": "Date",
              "categories": ["7/18/2017", "7/19/2017", "7/20/2017", "7/21/2017",
                             "7/22/2017", "7/23/2017", "7/24/2017", "7/25/2017"]}



class TestBuildView(unittest.TestCase):
    # Setup method run before every test case
    @classmethod
    def setUpClass(cls):
        cls.build = Build()

    # Teardown method run before every test case
    @classmethod
    def tearDownClass(cls):
        couch.delete('projects_data')

    # Tests that the correct issues can be returned based on raw issues data stored
    def test_data_prime(self):
        global builds_doc

        if 'projects_data' in couch:
            db = couch['projects_data']
        else:
            db = couch.create('projects_data')  # Create test data
        db.save(builds_doc)
        data = views.get_builds("VDM-VB")
        expected_data = {
            'builds_status': {'successful': [0, 6, 6, 4, 0, 4, 1, 4], '_rev': '2-a0ceca1a37702b50496e67f9f55ad040',
                               'y_axis_title': 'Number of Builds', 'failed': [0, 0, 0, 0, 0, 0, 0, 0],
                               'x_axis_title': 'Date', '_id': 'VDM-VB_builds_status',
                               'categories': ['7/18/2017', '7/19/2017', '7/20/2017', '7/21/2017', '7/22/2017',
                                              '7/23/2017', '7/24/2017', '7/25/2017']}
        }
        self.assertEqual(data, expected_data)

    # Tests build server builds with successful builds
    def test_create_build_status_with_success_builds(self):
        input_data = [
                      {"_id": "VDM-VB-42", "_rev": "1-a4837ae1c4146f20141dd5f23b8b07d9",
                       "reasonSummary": "Changes by <a href=\"http://128.2.24.123:8085/users/viewUserSummary.action?currentUserName=huangyuhen\">Yuheng Huang</a>",
                       "key": "VDM-VB-42", "comments_size": 0, "quarantinedTestCount": 0, "number": 42,
                       "prettyBuildStartedTime": "Tue, 25 Jul, 03:30 PM",
                       "prettyBuildCompletedTime": "Tue, 25 Jul, 03:30 PM", "id": 688211,
                       "buildRelativeTime": "42 minutes ago", "notRunYet": "false",
                       "buildCompletedTime": datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "state": "Successful",
                       "buildCompletedDate": datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "continuable": "false", "restartable": "false",
                       "successful": "true", "projectName": "VDM", "buildDuration": 982, "buildState": "Successful",
                       "buildNumber": 42, "finished": "true", "failedTestCount": 0, "buildTestSummary": "No tests found",
                       "plan": {"name": "VDM - VDM BAMBOO", "key": "VDM-VB",
                                "link_href": "http://128.2.24.123:8085/rest/api/latest/plan/VDM-VB",
                                "planKey": "VDM-VB", "shortName": "VDM BAMBOO", "shortKey": "VB"},
                       "planResultKey": "VDM-VB-42", "skippedTestCount": 0, "planName": "VDM BAMBOO",
                       "buildDurationInSeconds": 0,
                       "buildStartedTime": (datetime.now() - timedelta(days=1)).strftime("%Y-%m-%d %H:%M:%S"),
                       "successfulTestCount": 0,
                       "buildReason": "Changes by <a href=\"http://128.2.24.123:8085/users/viewUserSummary.action?currentUserName=huangyuhen\">Yuheng Huang</a>",
                       "lifeCycleState": "Finished", "buildDurationDescription": "< 1 second",
                       "link_href": "http://128.2.24.123:8085/rest/api/latest/result/VDM-VB-42",
                       "buildResultKey": "VDM-VB-42"},
                      {"_id": "VDM-VB-43", "_rev": "1-194a39a55a6013b286002bdbb17d2010",
                       "reasonSummary": "Changes by <a href=\"http://128.2.24.123:8085/authors/viewAuthor.action?authorName=xiaoleip%20%3Cxiaoleip%40andrew.cmu.edu%3E\">xiaoleip &lt;xiaoleip@andrew.cmu.edu&gt;</a>",
                       "key": "VDM-VB-43", "comments_size": 0, "quarantinedTestCount": 0, "number": 43,
                       "prettyBuildStartedTime": "Tue, 25 Jul, 03:51 PM",
                       "prettyBuildCompletedTime": "Tue, 25 Jul, 03:51 PM", "id": 688213,
                       "buildRelativeTime": "21 minutes ago", "notRunYet": "false",
                       "buildCompletedTime": (datetime.now() - timedelta(days=1)).strftime("%Y-%m-%d %H:%M:%S"), "state": "Successful",
                       "buildCompletedDate": (datetime.now() - timedelta(days=1)).strftime("%Y-%m-%d %H:%M:%S"), "continuable": "false", "restartable": "false",
                       "successful": "true", "projectName": "VDM", "buildDuration": 1080, "buildState": "Successful",
                       "buildNumber": 43, "finished": "true", "failedTestCount": 0, "buildTestSummary": "No tests found",
                       "plan": {"name": "VDM - VDM BAMBOO", "key": "VDM-VB",
                                "link_href": "http://128.2.24.123:8085/rest/api/latest/plan/VDM-VB",
                                "planKey": "VDM-VB", "shortName": "VDM BAMBOO", "shortKey": "VB"},
                       "planResultKey": "VDM-VB-43", "skippedTestCount": 0, "planName": "VDM BAMBOO",
                       "buildDurationInSeconds": 1,
                       "buildStartedTime": (datetime.now() - timedelta(days=2)).strftime("%Y-%m-%d %H:%M:%S"),
                       "successfulTestCount": 0,
                       "buildReason": "Changes by <a href=\"http://128.2.24.123:8085/authors/viewAuthor.action?authorName=xiaoleip%20%3Cxiaoleip%40andrew.cmu.edu%3E\">xiaoleip &lt;xiaoleip@andrew.cmu.edu&gt;</a>",
                       "lifeCycleState": "Finished", "buildDurationDescription": "1 second",
                       "link_href": "http://128.2.24.123:8085/rest/api/latest/result/VDM-VB-43",
                       "buildResultKey": "VDM-VB-43"}]
        data = charts.create_builds_status(input_data)

        end_date = parse(str(datetime.now()))
        start_date = end_date - timedelta(days=DATE_RANGE)
        delta = end_date - start_date
        # a list of dates in the range
        categories = [(str((start_date + timedelta(days=i)).month) + "/" + str(
            (start_date + timedelta(days=i)).day) + "/" + str((start_date + timedelta(days=i)).year)) for i in
                      range(delta.days + 1)]

        expected_data = {'successful': [0, 0, 0, 0, 0, 0, 1, 1],
                         'failed': [0, 0, 0, 0, 0, 0, 0, 0],
                         'x_axis_title': 'Date', 'y_axis_title': 'Number of Builds',
                         'categories': categories}
        self.assertEqual(data, expected_data)

    # Tests build server builds with failed builds
    def test_create_build_status_with_failed_builds(self):
        input_data = [
            {"_id": "VDM-VB-42", "_rev": "1-a4837ae1c4146f20141dd5f23b8b07d9",
             "reasonSummary": "Changes by <a href=\"http://128.2.24.123:8085/users/viewUserSummary.action?currentUserName=huangyuhen\">Yuheng Huang</a>",
             "key": "VDM-VB-42", "comments_size": 0, "quarantinedTestCount": 0, "number": 42,
             "prettyBuildStartedTime": "Tue, 25 Jul, 03:30 PM",
             "prettyBuildCompletedTime": "Tue, 25 Jul, 03:30 PM", "id": 688211,
             "buildRelativeTime": "42 minutes ago", "notRunYet": "false",
             "buildCompletedTime": datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "state": "Failed",
             "buildCompletedDate": datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "continuable": "false",
             "restartable": "false",
             "successful": False, "projectName": "VDM", "buildDuration": 982, "buildState": "Failed",
             "buildNumber": 42, "finished": "true", "failedTestCount": 0, "buildTestSummary": "No tests found",
             "plan": {"name": "VDM - VDM BAMBOO", "key": "VDM-VB",
                      "link_href": "http://128.2.24.123:8085/rest/api/latest/plan/VDM-VB",
                      "planKey": "VDM-VB", "shortName": "VDM BAMBOO", "shortKey": "VB"},
             "planResultKey": "VDM-VB-42", "skippedTestCount": 0, "planName": "VDM BAMBOO",
             "buildDurationInSeconds": 0,
             "buildStartedTime": (datetime.now() - timedelta(days=1)).strftime("%Y-%m-%d %H:%M:%S"),
             "successfulTestCount": 0,
             "buildReason": "Changes by <a href=\"http://128.2.24.123:8085/users/viewUserSummary.action?currentUserName=huangyuhen\">Yuheng Huang</a>",
             "lifeCycleState": "Finished", "buildDurationDescription": "< 1 second",
             "link_href": "http://128.2.24.123:8085/rest/api/latest/result/VDM-VB-42",
             "buildResultKey": "VDM-VB-42"},
            {"_id": "VDM-VB-43", "_rev": "1-194a39a55a6013b286002bdbb17d2010",
             "reasonSummary": "Changes by <a href=\"http://128.2.24.123:8085/authors/viewAuthor.action?authorName=xiaoleip%20%3Cxiaoleip%40andrew.cmu.edu%3E\">xiaoleip &lt;xiaoleip@andrew.cmu.edu&gt;</a>",
             "key": "VDM-VB-43", "comments_size": 0, "quarantinedTestCount": 0, "number": 43,
             "prettyBuildStartedTime": "Tue, 25 Jul, 03:51 PM",
             "prettyBuildCompletedTime": "Tue, 25 Jul, 03:51 PM", "id": 688213,
             "buildRelativeTime": "21 minutes ago", "notRunYet": "false",
             "buildCompletedTime": (datetime.now() - timedelta(days=1)).strftime("%Y-%m-%d %H:%M:%S"),
             "state": "Failed",
             "buildCompletedDate": (datetime.now() - timedelta(days=1)).strftime("%Y-%m-%d %H:%M:%S"),
             "continuable": "false", "restartable": "false",
             "successful": False, "projectName": "VDM", "buildDuration": 1080, "buildState": "Failed",
             "buildNumber": 43, "finished": "true", "failedTestCount": 0, "buildTestSummary": "No tests found",
             "plan": {"name": "VDM - VDM BAMBOO", "key": "VDM-VB",
                      "link_href": "http://128.2.24.123:8085/rest/api/latest/plan/VDM-VB",
                      "planKey": "VDM-VB", "shortName": "VDM BAMBOO", "shortKey": "VB"},
             "planResultKey": "VDM-VB-43", "skippedTestCount": 0, "planName": "VDM BAMBOO",
             "buildDurationInSeconds": 1,
             "buildStartedTime": (datetime.now() - timedelta(days=2)).strftime("%Y-%m-%d %H:%M:%S"),
             "successfulTestCount": 0,
             "buildReason": "Changes by <a href=\"http://128.2.24.123:8085/authors/viewAuthor.action?authorName=xiaoleip%20%3Cxiaoleip%40andrew.cmu.edu%3E\">xiaoleip &lt;xiaoleip@andrew.cmu.edu&gt;</a>",
             "lifeCycleState": "Finished", "buildDurationDescription": "1 second",
             "link_href": "http://128.2.24.123:8085/rest/api/latest/result/VDM-VB-43",
             "buildResultKey": "VDM-VB-43"}]
        data = charts.create_builds_status(input_data)
        end_date = parse(str(datetime.now()))
        start_date = end_date - timedelta(days=DATE_RANGE)
        delta = end_date - start_date
        # a list of dates in the range
        categories = [(str((start_date + timedelta(days=i)).month) + "/" + str(
            (start_date + timedelta(days=i)).day) + "/" + str((start_date + timedelta(days=i)).year)) for i in
                      range(delta.days + 1)]

        expected_data = {'successful': [0, 0, 0, 0, 0, 0, 0, 0],
                         'failed': [0, 0, 0, 0, 0, 0, 1, 1],
                         'x_axis_title': 'Date', 'y_axis_title': 'Number of Builds',
                         'categories': categories}
        self.assertEqual(data, expected_data)

    # Tests build server builds with failed and successful builds
    def test_create_build_status_with_failed_and_successful_builds(self):
        input_data = [
            {"_id": "VDM-VB-42", "_rev": "1-a4837ae1c4146f20141dd5f23b8b07d9",
             "reasonSummary": "Changes by <a href=\"http://128.2.24.123:8085/users/viewUserSummary.action?currentUserName=huangyuhen\">Yuheng Huang</a>",
             "key": "VDM-VB-42", "comments_size": 0, "quarantinedTestCount": 0, "number": 42,
             "prettyBuildStartedTime": "Tue, 25 Jul, 03:30 PM",
             "prettyBuildCompletedTime": "Tue, 25 Jul, 03:30 PM", "id": 688211,
             "buildRelativeTime": "42 minutes ago", "notRunYet": "false",
             "buildCompletedTime": datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "state": "Failed",
             "buildCompletedDate": datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "continuable": "false",
             "restartable": "false",
             "successful": True, "projectName": "VDM", "buildDuration": 982, "buildState": "Failed",
             "buildNumber": 42, "finished": "true", "failedTestCount": 0, "buildTestSummary": "No tests found",
             "plan": {"name": "VDM - VDM BAMBOO", "key": "VDM-VB",
                      "link_href": "http://128.2.24.123:8085/rest/api/latest/plan/VDM-VB",
                      "planKey": "VDM-VB", "shortName": "VDM BAMBOO", "shortKey": "VB"},
             "planResultKey": "VDM-VB-42", "skippedTestCount": 0, "planName": "VDM BAMBOO",
             "buildDurationInSeconds": 0,
             "buildStartedTime": (datetime.now() - timedelta(days=1)).strftime("%Y-%m-%d %H:%M:%S"),
             "successfulTestCount": 0,
             "buildReason": "Changes by <a href=\"http://128.2.24.123:8085/users/viewUserSummary.action?currentUserName=huangyuhen\">Yuheng Huang</a>",
             "lifeCycleState": "Finished", "buildDurationDescription": "< 1 second",
             "link_href": "http://128.2.24.123:8085/rest/api/latest/result/VDM-VB-42",
             "buildResultKey": "VDM-VB-42"},
            {"_id": "VDM-VB-43", "_rev": "1-194a39a55a6013b286002bdbb17d2010",
             "reasonSummary": "Changes by <a href=\"http://128.2.24.123:8085/authors/viewAuthor.action?authorName=xiaoleip%20%3Cxiaoleip%40andrew.cmu.edu%3E\">xiaoleip &lt;xiaoleip@andrew.cmu.edu&gt;</a>",
             "key": "VDM-VB-43", "comments_size": 0, "quarantinedTestCount": 0, "number": 43,
             "prettyBuildStartedTime": "Tue, 25 Jul, 03:51 PM",
             "prettyBuildCompletedTime": "Tue, 25 Jul, 03:51 PM", "id": 688213,
             "buildRelativeTime": "21 minutes ago", "notRunYet": "false",
             "buildCompletedTime": (datetime.now() - timedelta(days=1)).strftime("%Y-%m-%d %H:%M:%S"),
             "state": "Failed",
             "buildCompletedDate": (datetime.now() - timedelta(days=1)).strftime("%Y-%m-%d %H:%M:%S"),
             "continuable": "false", "restartable": "false",
             "successful": False, "projectName": "VDM", "buildDuration": 1080, "buildState": "Failed",
             "buildNumber": 43, "finished": "true", "failedTestCount": 0, "buildTestSummary": "No tests found",
             "plan": {"name": "VDM - VDM BAMBOO", "key": "VDM-VB",
                      "link_href": "http://128.2.24.123:8085/rest/api/latest/plan/VDM-VB",
                      "planKey": "VDM-VB", "shortName": "VDM BAMBOO", "shortKey": "VB"},
             "planResultKey": "VDM-VB-43", "skippedTestCount": 0, "planName": "VDM BAMBOO",
             "buildDurationInSeconds": 1,
             "buildStartedTime": (datetime.now() - timedelta(days=2)).strftime("%Y-%m-%d %H:%M:%S"),
             "successfulTestCount": 0,
             "buildReason": "Changes by <a href=\"http://128.2.24.123:8085/authors/viewAuthor.action?authorName=xiaoleip%20%3Cxiaoleip%40andrew.cmu.edu%3E\">xiaoleip &lt;xiaoleip@andrew.cmu.edu&gt;</a>",
             "lifeCycleState": "Finished", "buildDurationDescription": "1 second",
             "link_href": "http://128.2.24.123:8085/rest/api/latest/result/VDM-VB-43",
             "buildResultKey": "VDM-VB-43"}]
        data = charts.create_builds_status(input_data)
        end_date = parse(str(datetime.now()))
        start_date = end_date - timedelta(days=DATE_RANGE)
        delta = end_date - start_date
        # a list of dates in the range
        categories = [(str((start_date + timedelta(days=i)).month) + "/" + str(
            (start_date + timedelta(days=i)).day) + "/" + str((start_date + timedelta(days=i)).year)) for i in
                      range(delta.days + 1)]

        expected_data = {'successful': [0, 0, 0, 0, 0, 0, 0, 1],
                         'failed': [0, 0, 0, 0, 0, 0, 1, 0],
                         'x_axis_title': 'Date', 'y_axis_title': 'Number of Builds',
                         'categories': categories}
        self.assertEqual(data, expected_data)
    
