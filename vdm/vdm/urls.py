"""vdm URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from issueTracker import urls as issue_urls
from django.conf.urls import (
handler404
)

handler404 = 'main.views.page_not_found'



urlpatterns = [
	url(r'^$', 'main.views.project_overview', name = 'project_overview'),
    url(r'^organization/$', 'main.views.organization', name = 'organization'),
	url(r'^dashboard/(?P<project_name>.+)/$', 'main.views.dashboard', name='dashboard'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^issue/', include(issue_urls)),
    url(r'^project_configuration/$', 'main.views.project_configuration', name='project_configuration'),
    url(r'^add_project/$', 'main.views.add_project', name='add_project'),
    url(r'^delete_project/(?P<project_name>.+)/$', 'main.views.delete_project', name='delete_project')

]
