function drawChart(data, rendervar) {
    new Highcharts.Chart({

        colors: ["#aaeeee", "#90ee7e",  "#7798BF", "#ff0066", "#eeaaee",
      "#55BF3B", "#DF5353", "#7798BF", "#aaeeee"],

        chart: {
            renderTo: rendervar,
            defaultSeriesType: 'line',
            backgroundColor: '#333333'
        },
        credits: {
            enabled: false
        },
        title: {
                text: null
        },
        legend: {
              itemStyle: {
                 color: '#A0A0A0'
              },
              itemHoverStyle: {
                 color: '#FFF'
              },
              itemHiddenStyle: {
                 color: '#FFF'
              }
        },
        xAxis: {
            title: {
                text: data.x_axis_title,
                    style: {
            color: '#FFF',
            fontWeight: 'bold',
            fontSize: '12px',
            fontFamily: 'Trebuchet MS, Verdana, sans-serif'

         }
                },
            labels: {
                rotation: -45
            },
            categories: data.categories
        },
        yAxis: {
                title: {
                    text: data.y_axis_title,
                    style: {
            color: '#FFF',
            fontWeight: 'bold',
            fontSize: '12px',
            fontFamily: 'Trebuchet MS, Verdana, sans-serif'

         }
                },
                max: getMaxValue(data.num),
                min: 0,
                gridLineColor: '#666666'
        },
        tooltip: {
            enabled: true,
            formatter: function() {

                    return '<b>'+ this.series.name +'</b><br/>'+
                            this.x +': '+ this.y;
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                        enabled: false
                },
                enableMouseTracking: true
            }
        },
        series: [{
            name: 'Commits',
            data: data.num
        }]
    });
}

function getMaxValue(data) {
    max = Number.MIN_VALUE;
    for(var i = 0; i < data.length; i++) {
        if(data[i] > max) {
            max = data[i];
        }
    }
    return max;
}

