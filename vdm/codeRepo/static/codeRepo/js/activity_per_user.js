function drawActivityLevelChart(data, rendervar) {
    new Highcharts.Chart({
        chart: {
            renderTo: rendervar,
            defaultSeriesType: 'column',
            backgroundColor: '#333333'
        },
        legend: {
              itemStyle: {
                 color: '#A0A0A0'
              },
              itemHoverStyle: {
                 color: '#FFF'
              },
              itemHiddenStyle: {
                 color: '#FFF'
              }

        },
        credits: {
            enabled: false
        },
        title: {
                text: null
        },
        xAxis: {
            title: {
                text: data.x_axis_title,
                style: {
                    color: '#FFF',
                    fontWeight: 'bold',
                    fontSize: '12px',
                    fontFamily: 'Trebuchet MS, Verdana, sans-serif'
                }
            },
            labels: {
                rotation: -45,
                style: {
                color: '#d0d0d0'
                }
            },
            categories: data.x_axis_values
        },
        yAxis: {
            title: {
                text: data.y_axis_title,
                style: {
                    color: '#FFF',
                    fontWeight: 'bold',
                    fontSize: '12px',
                    fontFamily: 'Trebuchet MS, Verdana, sans-serif'
                }
            },
            max: getMaxValue(data.y_axis_values),
            min: 0,
            gridLineColor: '#666666'
        },
        tooltip: {
            formatter: function() {
                    return '<b>' + this.x + '</b>: <b>' + this.y + '</b> commits';
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                        enabled: false
                },
                enableMouseTracking: true
            }
        },
        series: [{
            name: 'Commits',
            borderWidth: 0,
            data: data.y_axis_values,
            color: '#23BADF'
        }]
    });
}

function getMaxValue(data) {
    max = Number.MIN_VALUE;
    for(var i = 0; i < data.length; i++) {
        if(data[i] > max) {
            max = data[i];
        }
    }
    return max;
}

