function renderModificationActivityPerUser (data, rendervar) {
    new Highcharts.Chart( {
        // colors: ['#2f7ed8','#910000','#fff000','#8bbc21', '#910000'],
        colors: ["#aaeeee", "#DF5353", "#7798BF", "#ff0066", "#eeaaee", "#55BF3B", "#90ee7e", "#7798BF", "#aaeeee"], chart: {
            type: 'column', renderTo: rendervar, backgroundColor: '#333333'
        }
        , credits: {
            enabled: false
        }
        , title: {
            text: null
        }
        , xAxis: {
            categories: data.categories, title: {
                text: data.x_axis_title, style: {
                    color: '#FFF', fontWeight: 'bold', fontSize: '12px', fontFamily: 'Trebuchet MS, Verdana, sans-serif'
                }
            }
            , labels: {
                rotation: -45,
                style: {
                    color: '#d0d0d0'
                 }
            }
            ,
        }
        , yAxis: {
            gridLineColor: '#666666',
            allowDecimals: false, min: 0, title: {
                text: data.y_axis_title, style: {
                    color: '#FFF', fontWeight: 'bold', fontSize: '12px', fontFamily: 'Trebuchet MS, Verdana, sans-serif'
                }
            }
        }
        , tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' + this.series.name + ': ' + this.y + '<br/>' + 'Total: ' + this.point.stackTotal;
            }
        }
        , legend: {
              itemStyle: {
                 color: '#A0A0A0'
              },
              itemHoverStyle: {
                 color: '#FFF'
              },
              itemHiddenStyle: {
                 color: '#FFF'
              }

        }, plotOptions: {
            column: {
                stacking: 'normal',
            }
        }
        , series: [ {
            name: 'Added', data: data.added, stack: 'stack1',
            borderWidth: 0
        }
        , {
            name: 'Deleted', data: data.deleted, stack: 'stack2',
            borderWidth: 0
        }
        ]
    }
    );
}