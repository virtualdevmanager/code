
function drawPieChart(data) {
    new Highcharts.Chart({
        colors: ['#395C9B', '#923532', '#7B972E'],
        chart: {
            renderTo: 'issues_by_priority',
            type: 'pie',
            margin: [0, 0, 0, 0],
            spacingTop: 0,
            spacingBottom: 0,
            spacingLeft: 0,
            spacingRight: 0,
            backgroundColor: 'black'
        },
        credits: {
            enabled: false
        },
        title: {
            text: null
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y} ({point.percentage:.1f}%) </b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    style: {
                        //fontSize:'1px',
                        font: '11pt Trebuchet MS, Verdana, sans-serif',
                        color: '#A0A0A0'
                    }
                }
            }
        },
        series: [{
            name: 'Open and In Progress Issues',
            colorByPoint: true,
            data: [{
                name: 'Low',
                y: data.low
            }, {
                name: 'Medium',
                y: data.medium,
                selected: true
            }, {
                name: 'High',
                y: data.high
            }]
        }]
    });
}