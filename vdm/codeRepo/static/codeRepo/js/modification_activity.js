function drawModificationChart(data, rendervar) {
    console.log(data);
    new Highcharts.Chart({

        colors: ["#aaeeee", "#DF5353",  "#7798BF", "#ff0066", "#eeaaee",
      "#55BF3B", "#90ee7e", "#7798BF", "#aaeeee"],


        chart: {
            renderTo: rendervar,
            defaultSeriesType: 'line',
            backgroundColor: '#333333'
        },
        legend: {
              itemStyle: {
                 color: '#A0A0A0'
              },
              itemHoverStyle: {
                 color: '#FFF'
              },
              itemHiddenStyle: {
                 color: '#FFF'
              }

        },
        credits: {
            enabled: false
        },
        title: {
                text: null
        },
        xAxis: {
            title: {
                text: 'Date',
                    style: {
            color: '#FFF',
            fontWeight: 'bold',
            fontSize: '12px',
            fontFamily: 'Trebuchet MS, Verdana, sans-serif'

         }
                },
            labels: {
                rotation: -45
            },
            categories: data.categories
        },
        yAxis: {
                title: {
                    text: 'Lines of Code',
                    style: {
            color: '#FFF',
            fontWeight: 'bold',
            fontSize: '12px',
            fontFamily: 'Trebuchet MS, Verdana, sans-serif',
            gridLineColor: '#666666'
         }
                },
                max: getMaxValueFromTwoLists(data.added, data.deleted),
                min: 0
        },
        tooltip: {
            enabled: true,
            formatter: function() {

                    return '<b>'+ this.series.name +'</b><br/>'+
                            this.x +': '+ this.y;
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                        enabled: false
                },
                enableMouseTracking: true
            }
        },
        series: [{
            name: 'Added',
            data: data.added
        }, {
            name: 'Deleted',
            data: data.deleted
        }]
    });
}

function getMaxValueFromTwoLists(data1, data2) {
    max = Number.MIN_VALUE;
    for(var i = 0; i < data1.length; i++) {
        if(data1[i] > max) {
            max = data1[i];
        }
    }
    for(var i = 0; i < data2.length; i++) {
        if(data2[i] > max) {
            max = data2[i];
        }
    }
    return max;
}

