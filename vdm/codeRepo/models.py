import couchdb
from django.conf import settings
import logging

formatter = logging.Formatter(fmt='%(asctime)s-%(levelname)s-%(module)s-%(message)s')
handler = logging.FileHandler('vagrant/vdm/main/logs/coderepo-models.log')
handler.setFormatter(formatter)
logger = logging.getLogger('root')
logger.setLevel(logging.WARNING)
logger.addHandler(handler)


class Data:
    def __init__(self):
        self.repo_data = []
        try:
            couch = couchdb.Server(settings.COUCH_DB_URL)
            self.repo_data = couch['projects_data']
        except Exception as e:
            logger.error("vdm/codeRepo/models.py Data init - %s" %e)

    def get_data(self):
        """ get all documents in the database 'projects_data' in a list
        :rtype: list<couchdb.client.Document>
        :return: a list of documents (couchdb.client.Document)
        """
        try:
            return [ self.repo_data[x] for x in self.repo_data ]
        except Exception as e:
            logger.error("vdm/codeRepo/models.py Data get_data - %s" %e)
        return []


class Commit:
    def __init__(self):
        """Initializes the issue DAO to get issue and sprint data from couchdb"""
        self.commits = []
        self.couch = couchdb.Server(settings.COUCH_DB_URL)
        try:
            logger.debug('models.Commit.__init__ - initializing Issue()')
            self.commits = self.couch['commits']
        except Exception as e:
            logger.error('models.Issue.__init__ - %s' % str(e))

    def get_commits(self, project_name):
        logger.info('getting commits for: ', project_name)
        return [self.commits[x] for x in self.commits if x.startswith(project_name) and project_name != ""]

    def process_diff(self):
        try:
            for key in self.commits:
                commit = self.commits[key]
                if commit["diff"] is not None and ("added" not in commit or "deleted" not in commit):
                    added = 0
                    deleted = 0
                    diff = commit["diff"].encode('utf-8')
                    diff_lines = str(diff).splitlines()
                    logger.error("diff_lines length:" + str(len(diff_lines)))
                    for line in diff_lines:
                        if line.startswith('+') and not line.startswith('+++'):
                            added += 1
                        if line.startswith('-') and not line.startswith('---'):
                            deleted += 1
                    commit["added"] = added
                    commit["deleted"] = deleted

                    self.save_to_commits_db(commit, commit["_id"])
        except Exception as e:
            logger.error('commits.models process_diff - %s' % e)

class Project:
    def __init__(self):
        """Initializes the save DAO to save, remove, or return relevant project-related data from couchdb"""
        try:
            couch = couchdb.Server(settings.COUCH_DB_URL)
            self.db = couch['project_info']
            logger.debug('models.Project.__init__ - initializing Project()')
        except Exception as e:
            logger.error('models.Project.__init__ - %s' % str(e))

    def get(self):
        logging.info('getting project info')
        return [self.db[x] for x in self.db]

    def get_db(self):
        logging.info('getting db')
        return self.db