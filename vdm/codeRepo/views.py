from django.shortcuts import render
from django.http import HttpResponse
import urllib2
import json
from django.conf import settings
from dateutil.parser import parse
from datetime import timedelta
import numpy as np
import datetime
from models import Data, Commit, Project
import sys, os
import ast
import logging

formatter = logging.Formatter(fmt='%(asctime)s-%(levelname)s-%(module)s-%(message)s')
handler = logging.FileHandler('/vagrant/vdm/main/logs/coderepo-views.log')
handler.setFormatter(formatter)
logger = logging.getLogger('codeRepo')
logger.setLevel(logging.INFO)
logger.addHandler(handler)


def sort_dict_by_keys(my_dict):
    sorted_dict = {}
    for key in sorted(my_dict.iterkeys(), reverse=True):
        sorted_dict[key] = my_dict[key]
    logger.error(">>" + str(sorted_dict.keys()))
    return sorted_dict


def get_commits(project_name):
    """ return a dictionary object containing all the charts of a project (id: projectName)
    :param project_name: project id
    :rtype: dict
    :return: a dict with keys: repo_activity
    """
    try:
        # represents the repo activity
        repo_activity = {}
        activity_per_user = {}
        modification_activity = {}
        modification_activity_per = {}

        docs = Data().get_data()
        for doc in docs:
            if doc["_id"] == (project_name + "_repo_activity"):
                repo_activity = ast.literal_eval(json.dumps(doc))
            elif doc["_id"] == (project_name + "_activity_per"):
                activity_per_user = ast.literal_eval(json.dumps(doc))
            elif doc["_id"] == (project_name + "_modification_activity"):
                modification_activity = ast.literal_eval(json.dumps(doc))
            elif doc["_id"] == (project_name + "_modification_activity_per"):
                modification_activity_per = ast.literal_eval(json.dumps(doc))

        commit_json = {
            'repo_activity': repo_activity,
            'modification_activity': modification_activity,
            'activity_per_user': activity_per_user,
            'modification_per_user': modification_activity_per
        }
        return commit_json
    except Exception as e:
        logger.error("vdm/codeRepo/views.py get_commits - %s" %e)
    return {}