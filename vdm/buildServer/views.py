from django.shortcuts import render
from django.http import HttpResponse
import urllib2
import json
from django.conf import settings
from dateutil.parser import parse
from datetime import timedelta
import numpy as np
import datetime
from models import Data
import json
import ast
import logging

formatter = logging.Formatter(fmt='%(asctime)s-%(levelname)s-%(module)s-%(message)s')
handler = logging.FileHandler('/vagrant/vdm/main/logs/buildServer-views.log')
handler.setFormatter(formatter)
logger = logging.getLogger('buildServer')
logger.setLevel(logging.INFO)
logger.addHandler(handler)


def get_builds(project_name):
    try:

        builds_status = {}

        docs = Data().get_data()

        for doc in docs:
            if doc["_id"] == (project_name + "_builds_status"):
                builds_status = ast.literal_eval(json.dumps(doc))

        build_json = {
            'builds_status': builds_status
        }
        return build_json
    except Exception as e:
        logger.error("vdm/buildServer/views.py get_builds - %s" % e)
    return {}
