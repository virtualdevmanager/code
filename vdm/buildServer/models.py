import couchdb
from django.conf import settings
import logging


class Data:

    def __init__(self):
        self.build_data = []
        try:
            couch = couchdb.Server(settings.COUCH_DB_URL)
            self.build_data = couch['projects_data']
        except Exception as e:
            logging.error(e)

    def get_data(self):
        """ get all documents in the database 'projects_data' in a list
        :rtype: list<couchdb.client.Document>
        :return: a list of documents (couchdb.client.Document)
        """
        return [ self.build_data[x] for x in self.build_data ]