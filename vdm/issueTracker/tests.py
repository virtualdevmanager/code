from django.test import TestCase
import views
import datetime

class TestIssueTrackerView(TestCase):

    main_data = [ {u'status': u'In Progress', u'estimate': None, u'_rev': u'14-5cb627900cf9c7f0450c32dafc00cec7', u'resolve_date': u'2016-06-30T20:10:16.000-0400', u'logged': u'12', u'due': u'2016-06-30T20:10:16.000-0400', u'summary': u'Implement Monitor DAO', u'priority': u'High', u'assignee': u'Gunjan Raghav', u'sprint': 1, u'_id': u'vdm_1', u'creation_date': u'2016-06-27T20:10:16.000-0400'},{u'status': u'Closed', u'estimate': u'6', u'_rev': u'5-5943e1cbfe642862c750a359227034df', u'resolve_date': u'2016-06-28T20:10:16.000-0400', u'logged': u'5', u'due': u'2016-06-30T20:10:16.000-0400', u'summary': u'Implement Monitor DTO', u'priority': u'High', u'assignee': u'Anurag Kanungo', u'sprint': 1, u'_id': u'vdm_2', u'creation_date': u'2016-06-27T20:10:16.000-0400'},{u'status': u'Open', u'estimate': u'6', u'_rev': u'5-b0086379a076c06825ed2c9b32baf72e', u'resolve_date': None, u'logged': u'5', u'due': u'2016-06-30T20:10:16.000-0400', u'summary': u'Security Layer', u'priority': u'High', u'assignee': u'Anurag Kanungo', u'sprint': 1, u'_id': u'vdm_3', u'creation_date': u'2016-06-27T20:10:16.000-0400'}
,{u'status': u'Closed', u'estimate': u'6', u'_rev': u'3-4b81480ffb2d9a84d51b1cf148609077', u'resolve_date': u'2016-06-29T20:10:16.000-0400', u'logged': u'5', u'due': u'2016-06-30T20:10:16.000-0400', u'summary': u'Data access layer', u'priority': u'High', u'assignee': u'Xiao Bao', u'sprint': 1, u'_id': u'vdm_4', u'creation_date': u'2016-06-27T20:10:16.000-0400'}]
    main_sprint = [{u'_rev': u'1-1cc356dfee9a47133174a92381b99f8a', u'_id': u'vdm_1', u'name': u'sprint 1', u'end_date': u'2016-07-15T20:10:16.000-0400', u'start_date': u'2016-06-27T20:10:16.000-0400'}]

    def setup(self):
        return

    def test_data_prime(self):
        data = views.get_issues("vdm", self.main_data, self.main_sprint)
        print data
        expected_data = {'burn_down': {}, 'status': {}, 'case_status': {}, 'workload': {}, 'issue_by_priority': {}}
        self.assertEqual(data, expected_data)

    def test_data(self):
        data = views.get_issues("vdm")
        self.assertEqual(True, True)

    def test_date_handle(self):
        views.date_handler(datetime.datetime.now().isoformat())
        self.assertEqual(True, True)