from django.shortcuts import render
from django.http import HttpResponse
import urllib2
import json
from django.conf import settings
from dateutil.parser import parse
from datetime import timedelta
import numpy as np
import datetime
from models import Data
import sys, os
import ast
import logging

formatter = logging.Formatter(fmt='%(asctime)s-%(levelname)s-%(module)s-%(message)s')
handler = logging.FileHandler('vagrant/vdm/main/logs/issuetracker-views.log')
handler.setFormatter(formatter)
logger = logging.getLogger('root')
logger.setLevel(logging.WARNING)
logger.addHandler(handler)


def get_issues(project_name):
    """ return a dictionary object containing all the charts of a project (id: projectName)
    :param project_name: project id
    :rtype: dict
    :return: a dict with keys: workload, burn_down, case_status, issue_by_priority, status, project_burndown.
             Each element is also a dict.
    """
    try:
        # burn_down: represents the sprint burndown chart
        burn_down = {}
        # case_status: represents the case status per team member chart
        case_status = {}
        # sprint_case_status: represents the case status per team member chart in the latest sprint
        sprint_case_status = {}
        # issue_by_priority: represents the case distribution by priority chart
        issue_by_priority = {}
        # workload: represents the workload per team member chart
        workload = {}
        # status: represents the case statistics
        status = {}
        # burn_down: represent the project burndown chart
        project_burndown = {}

        docs = Data().get_data()
        for doc in docs:
            if doc["_id"] == (project_name + "_burndown"):
                burn_down = ast.literal_eval(json.dumps(doc))
            elif doc["_id"] == (project_name + "_case_status"):
                case_status = ast.literal_eval(json.dumps(doc))
            elif doc["_id"] == (project_name + "_sprint_case_status"):
                sprint_case_status = ast.literal_eval(json.dumps(doc))
            elif doc["_id"] == (project_name + "_issues_count_by_priority"):
                issue_by_priority = ast.literal_eval(json.dumps(doc))
            elif doc["_id"] == (project_name + "_workload"):
                workload = ast.literal_eval(json.dumps(doc))
            elif doc["_id"] == (project_name + "_issues_status"):
                status = ast.literal_eval(json.dumps(doc))
            elif doc["_id"] == (project_name + "_project_burndown"):
                project_burndown = ast.literal_eval(json.dumps(doc))

        issue_json = {
            'workload': workload,
            'burn_down': burn_down,
            'case_status': case_status,
            'sprint_case_status': sprint_case_status,
            'issue_by_priority': issue_by_priority,
            'status': status,
            'project_burndown': project_burndown,
        }
        return issue_json
    except Exception as e:
        logger.error("vdm/issueTracker/views.py get_issues - %s" %e)
    return {}