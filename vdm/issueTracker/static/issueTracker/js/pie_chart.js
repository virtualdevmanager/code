
function drawPieChart(data) {
    new Highcharts.Chart({
        colors: ['#031A6B', '#087CA7', '#23BADF'],
        chart: {
            renderTo: 'issues_by_priority',
            type: 'pie',
            margin: [0, 0, 0, 0],
            spacingTop: 0,
            spacingBottom: 0,
            spacingLeft: 0,
            spacingRight: 0,
            backgroundColor: '#333333'
        },
        legend: {
              itemStyle: {
                 color: '#A0A0A0'
              },
              itemHoverStyle: {
                 color: '#FFF'
              },
              itemHiddenStyle: {
                 color: '#FFF'
              }

        },
        credits: {
            enabled: false
        },
        title: {
            text: null
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y} ({point.percentage:.1f}%) </b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    style: {
                        font: '11pt Trebuchet MS, Verdana, sans-serif',
                        color: '#A0A0A0'
                    }
                }
            }
        },
        series: [{
            name: 'Open and In Progress Issues',
            colorByPoint: true,
            data: [{
                name: 'Low',
                y: data.low
            }, {
                name: 'Medium',
                y: data.medium,
                selected: true
            }, {
                name: 'High',
                y: data.high
            }]
        }]
    });
}