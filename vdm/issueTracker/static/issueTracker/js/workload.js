function renderWorkload (data) {
    new Highcharts.Chart({
        chart: {
            renderTo: 'workload',
            backgroundColor: '#333333',
            polar: true,
            type: 'column'
        },

        colors: ["#2b908f", "#90ee7e", "#f45b5b", "#7798BF", "#aaeeee", "#ff0066", "#eeaaee",
      "#55BF3B", "#DF5353", "#7798BF", "#aaeeee"],

        title: {
            text: null
        },
        credits: {
            enabled: false
        },

        pane: {
            size: '85%'
        },

        xAxis: {
            tickmarkPlacement: 'on',
            categories: data.categories,
            labels: {
                 style: {
                    color: '#FFF'
                 }
             }
        },

        yAxis: {
            min: 0,
            tickInterval: 20,
            labels: {
            enabled: false,
            },
            reversedStacks: false

        },

    legend: {
              itemStyle: {
                 color: '#A0A0A0'
              },
              itemHoverStyle: {
                 color: '#FFF'
              },
              itemHiddenStyle: {
                 color: '#FFF'
              }

        },
        tooltip: {
            valueSuffix: 'pts'
        },

        plotOptions: {
            series: {
                stacking: 'normal',
                groupPadding: 0,
                pointPlacement: 'on'
            }

        },

      series: [
         {
            name: "0-20pts",
            data: data.lists[0]
         },
         {
            name: "20-40pts",
            data: data.lists[1]
         },
         {
            name: "Above 40pts",
            data: data.lists[2]
         }]
    });
}