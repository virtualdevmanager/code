function renderSprintCaseStatus (data) {
    new Highcharts.Chart({
        // colors: ['#2f7ed8','#910000','#fff000','#8bbc21', '#910000'],
        chart: {
            type: 'column',
            renderTo: 'sprint_case_status',
            backgroundColor: '#333333'
        },
        legend: {
              itemStyle: {
                 color: '#A0A0A0'
              },
              itemHoverStyle: {
                 color: '#FFF'
              },
              itemHiddenStyle: {
                 color: '#FFF'
              }

        },
        credits: {
            enabled: false
        },
        title: {
            text: null
        },

        xAxis: {
            categories: data.x_axis,
            title: {
                text: data.x_axis_title
            },
            labels: {
                 style: {
                    color: '#d0d0d0'
                 }
             }
        },
        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: data.y_axis_title
            },
            gridLineColor: '#666666'
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + this.y + '<br/>' +
                    'Total: ' + this.point.stackTotal;
            }
        },
        plotOptions: {
                column: {
                stacking: 'normal',
            }
        },
        series: [{
            name: 'Closed',
            borderWidth: 0,
            data: data.closed,
            color: '#23BADF',
            stack: 'stack1'
        }, {
            name: 'In Progress',
            borderWidth: 0,
            data: data.in_progress,
            color: '#D6FFF6',
            stack: 'stack1'
        }, {
            name: 'Open',
            borderWidth: 0,
            data: data.open, //it should be completed
            color: '#031A6B',
            stack: 'stack1'
        }, {
            name: 'Without Estimate',
            borderWidth: 0,
            data: data.not_estimated,
            color: '#004385',
            stack: 'stack2'
        }, {
            name: 'Estimated',
            borderWidth: 0,
            data: data.estimated,
            color: '#087CA7',
            stack: 'stack2'
        }]
    });
}