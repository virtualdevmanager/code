function drawChart(data, rendervar) {
    new Highcharts.Chart({

        colors: ["#aaeeee", "#90ee7e",  "#7798BF", "#ff0066", "#eeaaee",
      "#55BF3B", "#DF5353", "#7798BF", "#aaeeee"],


        chart: {
            renderTo: rendervar,
            defaultSeriesType: 'line',
            backgroundColor: '#333333'
        },
        credits: {
            enabled: false
        },
        title: {
                text: null
        },
        legend: {
              itemStyle: {
                 color: '#A0A0A0'
              },
              itemHoverStyle: {
                 color: '#FFF'
              },
              itemHiddenStyle: {
                 color: '#FFF'
              }

        },
        xAxis: {
            title: {
                text: 'Date',
                    style: {
            color: '#FFF',
            labels: {
                 style: {
                    color: '#FFF',
                    fontWeight: 'bold',
                    fontSize: '12px',
                    fontFamily: 'Trebuchet MS, Verdana, sans-serif'
                 }
             }

         }
                },
            labels: {
                rotation: -45
            },
            categories: data.categories
        },
        yAxis: {
                title: {
                    text: 'Story Points',
                    style: {
            color: '#FFF',
            fontWeight: 'bold',
            fontSize: '12px',
            fontFamily: 'Trebuchet MS, Verdana, sans-serif'

         }
                },
                gridLineColor: '#666666',
                max: data.estimated[0],
                min: 0
        },
        tooltip: {
            enabled: true,
            formatter: function() {
                    return '<b>'+ this.series.name +'</b><br/>'+
                            this.x +': '+ this.y;
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                        enabled: false
                },
                enableMouseTracking: true
            }
        },
        series: [{
            name: 'Plan',
            data: data.estimated,
            dataLabels: {
                 color: 'white'
             }
        }, {
            name: 'Remaining',
            data: data.remaining,
            dataLabels: {
                 color: 'white'
             }
        }]
    });
}

