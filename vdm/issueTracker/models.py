import couchdb
from django.conf import settings
import logging

formatter = logging.Formatter(fmt='%(asctime)s-%(levelname)s-%(module)s-%(message)s')
handler = logging.FileHandler('vagrant/vdm/main/logs/issuetracker-models.log')
handler.setFormatter(formatter)
logger = logging.getLogger('root')
logger.setLevel(logging.WARNING)
logger.addHandler(handler)

class Data:

    def __init__(self):
        self.issue_data = []
        try:
            couch = couchdb.Server(settings.COUCH_DB_URL)
            self.issue_data = couch['projects_data']
        except Exception as e:
            logger.error("vdm/issueTracker/models.py Data init - %s" %e)

    def get_data(self):
        """ get all documents in the database 'projects_data' in a list
        :rtype: list<couchdb.client.Document>
        :return: a list of documents (couchdb.client.Document)
        """
        try:
            return [ self.issue_data[x] for x in self.issue_data ]
        except Exception as e:
            logger.error("vdm/issueTracker/models.py Data get_data - %s" %e)
        return []