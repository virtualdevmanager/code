import json
import logging
import unirest
import couchdb
import urllib
from buildServer import views as buildServer
from codeRepo import views as codeRepo
from django.conf import settings
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.http import Http404
from django.shortcuts import render, render_to_response, redirect
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from issueTracker import views as issueTracker
from main.models import get_project, get_project_doc, save_project_info, get_adapter_info


from models import Data

formatter = logging.Formatter(fmt='%(asctime)s-%(levelname)s-%(module)s-%(message)s')
handler = logging.FileHandler('/vagrant/vdm/main/logs/views.log')
handler.setFormatter(formatter)
logger = logging.getLogger('mainViews')
logger.setLevel(logging.INFO)
logger.addHandler(handler)


def date_handler(obj):
    return obj.isoformat() if hasattr(obj, 'isoformat') else obj


def project_overview(request):
    """ render data on the project overview page  and returns the project overview page
    :param request: HttpRequest object
    :return: return a rendered HttpResponse object
    """
    try:
        # projects: the database: project_info
        projects = get_project()
        # commits = codeRepo.test()
        #logger.info(commits)
        # project_progress: a dictionary, each element is the project progress of a project.
        projects_progress = {}
        # get all documents in the database 'projects_data'.
        docs = Data().get_data()
        # adapters: a list of adapter names
        adapters = get_adapter_info()

        commits = {}
        for project in projects:
            if "adapter" in project:
                project["adapter"] = urllib.unquote(project["adapter"]).decode('utf8')

            for doc in docs:
                # if the document records the project progress
                # if the document records the project progress
                if "jira_id" in project and str(doc["_id"]) == (str(project["jira_id"]) + "_project_progress"):
                    projects_progress[str(project["name"])] = doc["progress"]
                if "bitbucket_id" in project and str(doc["_id"]) == (
                    str(project["bitbucket_id"]) + "_commit_case_activity"):
                    commits[str(project["name"])] = {
                        'last_commits': doc["last_commits"],
                        'project_info_to_return': doc["project_info_to_return"]
                    }

        return render(request, 'project_overview.html',
                      {
                          'projects': projects,
                          'dbName': 'project_info',
                          'couchdb': settings.COUCH_DB_URL,
                          'ip': settings.IP_ADDRESS,
                          'projects_progress': projects_progress,
                          'commits': commits,
                          'adapters': adapters
                      })
    except Exception as e:
        logger.error("main/views.py project_overview - %s" % e)
        raise Http404


def organization(request):
    """ render data on the organizational page and returns the organization page
    :param request: HttpRequest object
    :return: return a rendered HttpResponse object
    """
    try:
        # get the  database 'project_info'
        projects = get_project()
        # global_workload: represents global workload chart
        global_workload = {}
        # global_issue_status_project: represents the issue status by Projects chart
        global_issue_status_project = {}
        # get all documents in the database 'projects_data'
        docs = Data().get_data()
        for doc in docs:
            if doc["_id"] == "global_workload":
                global_workload = json.dumps(doc)
            elif doc["_id"] == "global_issue_status_project":
                global_issue_status_project = json.dumps(doc)
        return render(request, 'organization.html',
                      {
                          'projects': projects,
                          'dbName': 'project_info',
                          'couchdb': settings.COUCH_DB_URL,
                          'ip': settings.IP_ADDRESS,
                          "global_workload": global_workload,
                          "issue_status_project": global_issue_status_project
                      })
    except Exception as e:
        logger.error("main/views.py organization - %s" % e)
        raise Http404


def dashboard(request, project_name):
    """ render data on the project dashboard page and returns the dashboard page
    :param request: HttpRequest object
    :param project_name: the id of the project
    :return: return a rendered HttpResponse object
    """
    try:
        # get the document whose id is project_name in the project_info database
        project_doc = get_project_doc(project_name)
        data = {}
        if 'jira_id' in project_doc.keys() and project_doc['jira_id'] != '':
            # data["issues"]: all the project charts
            data["issues"] = issueTracker.get_issues(project_doc['jira_id'])
            return render(request, 'dashboard.html',
                          {
                              'project': project_doc,
                              'data': data,
                              'dbName': 'project_info',
                              'couchdb': settings.COUCH_DB_URL,
                              'ip': settings.IP_ADDRESS
                          })
        elif 'bamboo_id' in project_doc.keys() and project_doc['bamboo_id'] != '':
            data["builds"] = buildServer.get_builds(project_doc['bamboo_id'])
            return render(request, 'dashboard_bamboo.html',
                          {
                              'project': project_doc,
                              'data': data,
                              'dbName': 'project_info',
                              'couchdb': settings.COUCH_DB_URL,
                              'ip': settings.IP_ADDRESS
                          })
        elif 'bitbucket_id' in project_doc.keys() and project_doc['bitbucket_id'] != '':
            # data["issues"]: all the project charts
            data["commits"] = codeRepo.get_commits(project_doc['bitbucket_id'])
            return render(request, 'dashboard_bitbucket.html',
                          {
                              'project': project_doc,
                              'data': data,
                              'dbName': 'project_info',
                              'couchdb': settings.COUCH_DB_URL,
                              'ip': settings.IP_ADDRESS
                          })
        elif 'fogbugz_id' in project_doc.keys() and project_doc['fogbugz_id'] != '':
            # data["issues"]: all the project charts
            data["issues"] = issueTracker.get_issues(project_doc['fogbugz_id'])
            return render(request, 'dashboard.html',
                          {
                              'project': project_doc,
                              'data': data,
                              'dbName': 'project_info',
                              'couchdb': settings.COUCH_DB_URL,
                              'ip': settings.IP_ADDRESS
                          })
    except Exception as e:
        logger.error("main/views.py dashboard - %s" % e)
        return render(request, '404.html', {'msg': e})


def page_not_found(request):
    """ render 404 not found page and return it as a response
    :param request: HttpRequest object
    :return: return a rendered HttpResponse object
    """
    response = render_to_response('404.html', {}, context_instance=RequestContext(request))
    response.status_code = 404
    return response


def project_configuration(request):
    """ render add project page with all fields empty and return it as a response
    :param request: HttpRequest object
    :return: return a rendered HttpResponse obj
    """
    adapters = []
    try:
        # read adapter names from the db
        couch = couchdb.Server(settings.COUCH_DB_URL)
        adapter_db = couch['schedular']
        for doc in adapter_db:
            if 'pname' in adapter_db[doc] and 'port' in adapter_db[doc]:
                data = {
                    'name': urllib.unquote(adapter_db[doc]['pname']).decode('utf8'),
                    'port': adapter_db[doc]['port']
                }
                adapters.append(data)

    except Exception as e:
        logger.error("main/views.py project_configuration - %s" % e)
    return render(request, 'add_project.html', {
        'ip': settings.IP_ADDRESS,
        'adapters': adapters
    })


@csrf_exempt
def add_project(request):
    """ Response to request with url "add_project".
        Return the add_project.html if it is requested using the 'GET' method.
        If the POST method is used:
          add new project information to the database when the project name does not exist.
          Otherwise, return error message.
        This function does not check if the JIRA id exists or not. i.e, user can add
        a non-existing JIRA project to the system.
    :param request: HttpRequest object
    :return: return a rendered page
    """

    try:
        if request.method == 'GET':
            # if the user requests to get the "add_project.html" but not to submit new project information
            redirect(reverse('project_configuration'))
        # if the user uses POST method to submit new project information
        project_info = {
            'project_name': request.POST.get('project-name', False),
            'project_description': request.POST.get('project-description', False),
            'file_data': request.FILES['project-logo'].read(),
            'content_type': request.FILES['project-logo'].content_type,
            'adapter': urllib.quote(request.POST.get('adapter-name', False))
        }
        if 'jira-id' in request.POST and request.POST['jira-id'] != '':
            project_info['jira_id'] = request.POST.get('jira-id', False)
        if 'bamboo-id' in request.POST and request.POST['bamboo-id'] != '':
            project_info['bamboo_id'] = request.POST.get('bamboo-id', False)
        if 'bitbucket-team-id' in request.POST and request.POST['bitbucket-team-id'] != '':
            project_info['bitbucket_id'] = request.POST.get('bitbucket-team-id', False) + '_' + request.POST.get(
                'bitbucket-repo-id', False)
        if 'fogbugz-id' in request.POST and request.POST['fogbugz-id'] != '':
            project_info['fogbugz_id'] = urllib.unquote(request.POST.get('fogbugz-id', False))
        # Forcing an update data whenever a user add a new project
        # If user add a adapter first then add a project on home page, a force update data is required.
        couch = couchdb.Server(settings.COUCH_DB_URL)
        adapter_db = couch['schedular']

        for doc in adapter_db:
            if 'pname' in adapter_db[doc] and adapter_db[doc]['pname'] == project_info['adapter']:
                # check if project exists in the external server
                if 'jira_id' in project_info:
                    path = "http://localhost:" + str(settings.MONITOR_PORT) + "/doesProjectExist"
                    response = unirest.post(path,
                                            headers={"Content-Type": "application/x-www-form-urlencoded"},
                                            params={'adapter_name': adapter_db[doc]['pname'],
                                                    'project_key': project_info['jira_id']})
                    logger.info("response.body: %s" % response.body)
                    result = response.body["message"]

                    if str(result) == 'False':
                        messages.error(request, "Project does not exist!")
                        return redirect(reverse('project_configuration'))

                if 'bitbucket_id' in project_info:
                    response = unirest.post("http://localhost:" + str(settings.MONITOR_PORT) + "/doesProjectExist",
                                            headers={"Content-Type": "application/x-www-form-urlencoded"},
                                            params={'adapter_name': adapter_db[doc]['pname'],
                                                    'project_key': project_info['bitbucket_id']})
                    logger.info("response.body: %s" % response.body)
                    result = response.body["message"]

                    if str(result) == 'False':
                        messages.error(request, "Project does not exist!")
                        return redirect(reverse('project_configuration'))

                if 'bamboo_id' in project_info:
                    # get response
                    response = unirest.post("http://localhost:" + str(settings.MONITOR_PORT) + "/doesProjectExist",
                                            headers={"Content-Type": "application/x-www-form-urlencoded"},
                                            params={'adapter_name': adapter_db[doc]['pname'],
                                                    'project_key': project_info['bamboo_id']})
                    logger.info("response.body: %s" % response.body)
                    result = response.body["message"]

                    if str(result) == 'False':
                        messages.error(request, "Project does not exist!")
                        return redirect(reverse('project_configuration'))


                if 'fogbugz_id' in project_info:
                    response = unirest.post("http://localhost:" + str(settings.MONITOR_PORT) + "/doesProjectExist",
                                            headers={"Content-Type": "application/x-www-form-urlencoded"},
                                            params={'adapter_name': adapter_db[doc]['pname'],
                                                    'project_key': project_info['fogbugz_id']})
                    logger.info("response.body: %s" % response.body)
                    result = response.body["message"]

                    if str(result) == 'False':
                        messages.error(request, "Project does not exist!")
                        return redirect(reverse('project_configuration'))


                if not save_project_info(project_info):
                    error_message = 'Project called "' + project_info['project_name'] + '" already exists!'
                    messages.error(request, error_message)
                    return redirect(reverse('project_configuration'))

                # Async API request to corresponding adapter
                unirest.post("http://localhost:" + str(settings.MONITOR_PORT) + "/forceUpdate",
                             headers={"Content-Type": "application/x-www-form-urlencoded"},
                             params={'pname': adapter_db[doc]['pname']}, callback=callback_function)
        # if succeeded, return to the project_overview page
        return redirect(reverse('project_overview'))
    except Exception as e:
        logger.error("main/views.py add_project - %s" % e)
        return redirect(reverse('project_configuration'))


def delete_project(request, project_name):
    """ delete a project whose name is project_name from the project_info database.
        this function does not delete other project-related data from the database
        other than the one in the project_info database.
    :param request: HttpRequest object
    :param project_name: the id of the project
    :return: a rendered project_overview page
    """
    try:
        couch = couchdb.Server(settings.COUCH_DB_URL)
        projectdb = couch['project_info']
        doc = projectdb[project_name]
        projectdb.delete(doc)
        return redirect(reverse('project_overview'))
    except Exception as e:
        logger.error("main/views.py delete_project - %s" % e)
        raise Http404


def callback_function(response):
    response.code  # The HTTP status code
    response.headers  # The HTTP headers
    response.body  # The parsed response
    response.raw_body  # The unparsed response
