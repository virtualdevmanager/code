
$(document).ready(function(e) {
	var contentInners = document.getElementsByClassName("content-inner");
	Array.prototype.forEach.call(contentInners, function(item) {
		if(item.childElementCount == 0) {
			var newNode = document.createElement('div');
			newNode.innerHTML = "<div><span style=\"color:#a5a5a5;padding-left: 20px\">" +
				"<i>Please add a project for this connector.</i></span></div>"
			item.appendChild(newNode);
		};
	});

 	$('.overview-icon').mouseenter(function() {
 		$(this).find('.edit').css('display', 'inline-block');
 		
 	}).mouseleave(function() {
 		$(this).find('.edit').hide();
 	});

 	$('.edit').click(function() {
 		e.preventDefault();
	    var targetUrl = $(this).attr("href");

	    $("#dialog").dialog({
	    	width: "500", 
	    	dialogClass: 'no-close success-dialog',
	      	buttons : {
		        "Yes" : function() {
		          window.location.href = targetUrl;
		        },
		        "No" : function() {
		          $(this).dialog("close");
		        }
		    }
 		});
 		$("#dialog").dialog("open");
 	});

	$("span[id^='icon-']").click(function() {
		var commitId =$(this).attr('id').substring(5);

		var curClass = $(this).attr('class');

		if(curClass == "glyphicon glyphicon-chevron-down")  {
			$("#msg-" + commitId).show();
			$(this).attr('class', "glyphicon glyphicon-chevron-up");
		}
		else if(curClass == "glyphicon glyphicon-chevron-up") {
			$("#msg-" + commitId).hide();
			$(this).attr('class', "glyphicon glyphicon-chevron-down");
		}



	});
	$("span[id^='adapter-span-']").click(function() {
		var adapterName =$(this).attr('id').substring(13);

		var curClass = $(this).attr('class');
		var contentId = "content-inner-" + adapterName;
		var contentElement = document.getElementById(contentId);
		if(curClass == "glyphicon glyphicon-chevron-down")  {
			contentElement.style.display = 'block';

			var hintElement = document.getElementById("adapter-hint-" + adapterName);
			hintElement.innerHTML = "Hide";
			$(this).attr('class', "glyphicon glyphicon-chevron-up");
		}
		else if(curClass == "glyphicon glyphicon-chevron-up") {
			var hintElement = document.getElementById("adapter-hint-" + adapterName);
			hintElement.innerHTML = "View Projects";
			contentElement.style.display = 'none';
			$(this).attr('class', "glyphicon glyphicon-chevron-down");
		}

	});
});