function renderGlobalWorkload (data) {
    new Highcharts.Chart({
        colors: ['#2f7ed8','#910000','#fff000','#8bbc21', '#910000'],

        chart: {
            type: 'column',
            renderTo: 'global-workload',
            backgroundColor: '#333333'
        },
        credits: {
            enabled: false
        },
        title: {
            text: null
        },

        xAxis: {
            categories: data.categories,
            title: {
                text: "Developers"
            },
            labels: {
                 style: {
                    color: '#d0d0d0'
                 }
             }
        },
        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: "Number of Story Points"
            },
            gridLineColor: '#666666'
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + this.y + '<br/>' +
                    'Total: ' + this.point.stackTotal;
            }
        },
        legend: {
            borderWidth: 0,
            itemStyle: {
                 font: '9pt Trebuchet MS, Verdana, sans-serif',
                 color: '#A0A0A0'
              },
        },
        plotOptions: {
                column: {
                stacking: 'normal',
            }
        },
        series: [{
            name: 'Upto 40 pts',
            data: data.lists[0],
            stack: 'stack1',
            borderWidth: 0
        }, {
            name: 'Above 40 pts',
            data: data.lists[1],
            stack: 'stack1',
            borderWidth: 0
        }]
    });
}