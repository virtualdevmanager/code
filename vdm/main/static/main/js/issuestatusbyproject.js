function renderIssueProjectStatus (data) {
   new Highcharts.Chart({
        // colors: ['#2f7ed8','#910000','#fff000','#8bbc21', '#910000'],
        chart: {
            type: 'column',
            renderTo: 'issue_status_projects',
            backgroundColor: '#333333'
        },
        credits: {
            enabled: false
        },
        title: {
            text: null
        },

        xAxis: {
            categories: data.categories,
            title: {
                text: "Project Name"
            },
            labels: {
                 style: {
                    color: '#d0d0d0'
                 }
             }
        },
        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: "Number of Issues"
            },
            gridLineColor: '#666666'
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + this.y + '<br/>' +
                    'Total: ' + this.point.stackTotal;
            }
        },
        legend: {
            borderWidth: 0,
            itemStyle: {
                 font: '9pt Trebuchet MS, Verdana, sans-serif',
                 color: '#A0A0A0'
              },
        },
        plotOptions: {
            series: {
                pointWidth: 50

            },
                column: {
                stacking: 'normal',
            }
        },
        series: [{
            name: 'Closed',
            data: data.closed,
            stack: 'stack1',
            borderWidth: 0
        }, {
            name: 'Open',
            data: data.open, //it should be completed
            stack: 'stack1',
            borderWidth: 0
        }, {
            name: 'In Progress',
            data: data.progress,
            stack: 'stack1',
            borderWidth: 0
        }]
    });
}