$(document).ready(function() {

	$('#jira').change(function() {
		if(this.checked) {
			$("#jira-id").prop('required', true);
			$('#jira-div').show();
			$('#bamboo').checked = false;
			$('#bamboo-id').prop('required', false);
			$('#bamboo-div').hide();
			$('#bitbucket').checked = false;
			$('#bitbucket-id').prop('required', false);
			$('#bitbucket-div').hide();
			$('#fogbugz').checked = false;
			$('#fogbugz-id').prop('required', false);
			$('#fogbugz-div').hide();
		} else {
			$('#jira-div').hide();
			$("#jira-id").prop('required', false);
		}
	});
	$('#bamboo').change(function() {
		if(this.checked) {
			$('#bamboo-id').prop('required', true);
			$('#bamboo-div').show();
			$('#jira').checked = false;
			$('#jira-id').prop('required', false);
			$('#jira-div').hide();
			$('#bitbucket').checked = false;
			$('#bitbucket-id').prop('required', false);
			$('#bitbucket-div').hide();
			$('#fogbugz').checked = false;
			$('#fogbugz-id').prop('required', false);
			$('#fogbugz-div').hide();
		} else {
			$('#bamboo-div').hide();
			$('#bamboo-id').prop('required', false);
		}
	});
	$('#bitbucket').change(function() {
		if(this.checked) {
			$('#bitbucket-id').prop('required', true);
			$('#bitbucket-div').show();
			$('#bamboo').checked = false;
			$('#bamboo-id').prop('required', false);
			$('#bamboo-div').hide();
			$('#jira').checked = false;
			$('#jira-id').prop('required', false);
			$('#jira-div').hide();
			$('#fogbugz').checked = false;
			$('#fogbugz-id').prop('required', false);
			$('#fogbugz-div').hide();
		} else {
			$('#bitbucket-div').hide();
			$('#bitbucket-id').prop('required', false);
		}
	});
	$('#fogbugz').change(function() {
		if(this.checked) {
			$('#fogbugz-id').prop('required', true);
			$('#fogbugz-div').show();
			$('#bitbucket').checked = false;
			$('#bitbucket-id').prop('required', false);
			$('#bitbucket-div').hide();
			$('#bamboo').checked = false;
			$('#bamboo-id').prop('required', false);
			$('#bamboo-div').hide();
			$('#jira').checked = false;
			$('#jira-id').prop('required', false);
			$('#jira-div').hide();
		} else {
			$('#fogbugz-div').hide();
			$('#fogbugz-id').prop('required', false);
		}
	});

	$('#adapter-name').change(function() {

		var index = this.selectedIndex;
		var port = this.options[index].getAttribute("port");
		if(port == '5656') {
			$("#jira").prop("checked", true);
			$('#jira').prop("disabled", false);
			$("#jira-id").prop('required', true);
			$('#jira-div').show();
			$('#bamboo').checked = false;
			$('#bamboo').prop("disabled", true);
			$('#bamboo-id').prop('required', false);
			$('#bamboo-div').hide();
			$('#bitbucket').checked = false;
			$('#bitbucket').prop("disabled", true);
			$('#bitbucket-id').prop('required', false);
			$('#bitbucket-div').hide();
			$('#fogbugz').checked = false;
			$('#fogbugz').prop("disabled", true);
			$('#fogbugz-id').prop('required', false);
			$('#fogbugz-div').hide();
		}
		else if(port == '5657') {
			$("#bamboo").prop("checked", true);
			$('#bamboo').prop("disabled", false);
			$('#bamboo-id').prop('required', true);
			$('#bamboo-div').show();
			$('#jira').checked = false;
			$('#jira').prop("disabled", true);
			$('#jira-id').prop('required', false);
			$('#jira-div').hide();
			$('#bitbucket').checked = false;
			$('#bitbucket').prop("disabled", true);
			$('#bitbucket-id').prop('required', false);
			$('#bitbucket-div').hide();
			$('#fogbugz').checked = false;
			$('#fogbugz').prop("disabled", true);
			$('#fogbugz-id').prop('required', false);
			$('#fogbugz-div').hide();
		}
		else if(port == '5658') {
			$("#bitbucket").prop("checked", true);
			$('#bitbucket').prop("disabled", false);
			$('#bitbucket-id').prop('required', true);
			$('#bitbucket-div').show();
			$('#bamboo').checked = false;
			$('#bamboo').prop("disabled", true);
			$('#bamboo-id').prop('required', false);
			$('#bamboo-div').hide();
			$('#jira').checked = false;
			$('#jira').prop("disabled", true);
			$('#jira-id').prop('required', false);
			$('#jira-div').hide();
			$('#fogbugz').checked = false;
			$('#fogbugz').prop("disabled", true);
			$('#fogbugz-id').prop('required', false);
			$('#fogbugz-div').hide();
		}
		else if(port == '5659') {
			$("#fogbugz").prop("checked", true);
			$('#fogbugz').prop("disabled", false);
			$('#fogbugz-id').prop('required', true);
			$('#fogbugz-div').show();
			$('#bitbucket').checked = false;
			$('#bitbucket').prop("disabled", true);
			$('#bitbucket-id').prop('required', false);
			$('#bitbucket-div').hide();
			$('#bamboo').checked = false;
			$('#bamboo').prop("disabled", true);
			$('#bamboo-id').prop('required', false);
			$('#bamboo-div').hide();
			$('#jira').checked = false;
			$('#jira').prop("disabled", true);
			$('#jira-id').prop('required', false);
			$('#jira-div').hide();
		}

	});
});