$(':checkbox').each(function () {
    var status = localStorage.getItem(this.id) === "false" ? false : true;
    $(this).prop("checked", status);
});

var autorefresh_value = localStorage.getItem('autorefresh');
if (autorefresh_value === null || autorefresh_value === '') {
	localStorage.setItem('autorefresh', false);
	localStorage.setItem('interval', '');
	$('#autorefresh').prop("checked", false);

} else {
	$('#autorefresh').prop("checked", autorefresh_value);

}


var interval_value = localStorage.getItem('interval');
if (interval_value === null || interval_value === '') {
	$('#auto-refresh-group').hide();
} else {
	$('#auto-refresh-group').show();
	$('#interval').val(interval_value);

}

$('#autorefresh').change(function() {
	var $autoRefresh = $('#autorefresh').is(':checked');
	if ($autoRefresh) {
		$('#auto-refresh-group').show();
		var interval = $('#interval').val();
		if (interval === null || interval === '') {
			interval = 3;
		}
		localStorage.setItem('autorefresh', $autoRefresh);
		localStorage.setItem('interval', interval);
	} else {
		$('#auto-refresh-group').hide();
		localStorage.removeItem('interval');
		localStorage.removeItem('autorefresh');
	}
});

function loadPage() {
	var $autoRefresh = $('#autorefresh').is(':checked');
	if ($autoRefresh) {
		$('#auto-refresh-group').show();
		var interval = $('#interval').val();
		if (interval === null || interval === '') {
			interval = 3;
		}
		localStorage.setItem('autorefresh', $autoRefresh);
		localStorage.setItem('interval', interval);
		setTimeout("location.reload(true);", interval * 60000);
	} else {
		$('#auto-refresh-group').hide();
		localStorage.removeItem('interval');
		localStorage.removeItem('autorefresh');
	}
}

$('#auto-refresh-button').click(function() {
	loadPage();
});

setInterval(function() {
	loadPage();
}, 30000)





