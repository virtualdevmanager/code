import logging
import urllib

import couchdb
from django.conf import settings
from django.http import Http404

formatter = logging.Formatter(fmt='%(asctime)s-%(levelname)s-%(module)s-%(message)s')
handler = logging.FileHandler('/vagrant/vdm/main/logs/models.log')
handler.setFormatter(formatter)
logger = logging.getLogger('root')
logger.setLevel(logging.WARNING)
logger.addHandler(handler)


class Data:
    def __init__(self):
        self.main_data = []
        try:
            couch = couchdb.Server(settings.COUCH_DB_URL)
            self.main_data = couch['projects_data']
        except Exception as e:
            logger.error("main/models.py initialization - %s" % e)

    def get_data(self):
        """ get all documents in the database 'projects_data' in a list
        :rtype: list<couchdb.client.Document>
        :return: a list of documents (couchdb.client.Document)
        """
        try:
            return [self.main_data[x] for x in self.main_data]
        except Exception as e:
            logger.error("main/models.py get_data - %s" % e)


def get_project():
    """ return the database whose name is 'project_info'
        (stores project information configured in the "Add project" page)
    :rtype: couchdb.client.Database
    :return: return the database object
    """
    try:
        couch = couchdb.Server(settings.COUCH_DB_URL)
        db = couch['project_info']
        return [db[x] for x in db]
    except Exception as e:
        logger.error("main/models.py get_project - %s" % e)
    return []


def get_project_doc(project_name):
    """ get the document which stores the basic information of a project
    :param project_name: project id
    :rtype: couchdb.client.Document
    :return: return the document whose id is project_name in the project_info database
    """
    try:
        couch = couchdb.Server(settings.COUCH_DB_URL)
        db = couch['project_info']
        doc = db[project_name]

        if 'adapter' in doc:
            doc['adapter'] = urllib.unquote(doc['adapter']).decode('utf8')
        return doc
    except Exception as e:
        logger.error("main/models.py get_project_doc - %s" % e)
        raise Http404


def get_adapter_info():
    """ get a list of adapter names added to the system
    :rtype: a list
    :return: return  a list of adapter names added to the system (in the schedular database)
    """
    try:

        couch = couchdb.Server(settings.COUCH_DB_URL)
        db = couch['schedular']
        adapters = []
        for pname in db:
            doc = db[pname]
            if 'pname' in doc and 'host' in doc and 'port' in doc:
                port = int(doc['port'])
                connector_type = ''
                if port == settings.JIRA_PORT:
                    connector_type = 'JIRA'
                elif port == settings.BAMBOO_PORT:
                    connector_type = 'Bamboo'
                elif port == settings.BITBUCKET_PORT:
                    connector_type = 'Bitbucket'
                elif port == settings.FOGBUGZ_PORT:
                    connector_type = 'FogBugz'

                adapters.append({
                    'name': urllib.unquote(doc['pname']).decode('utf8'),
                    'host': doc['host'],
                    'type': connector_type
                })
        return adapters
    except Exception as e:
        logger.error("main/models.py get_adapter_names - %" % e)
        raise Http404
    return []


def save_project_info(project_info):
    """ save the project information to the project_info db
    :param project_info: a dict with keys: project_name, project_description, file_data, content_type
           project_name: the name of a project, set by user
           project_description: the description of a project, set by user
           file_data: the photo uploaded by a user as the project profile photo
           content_type: the type of the project profile photo
    :rtype: boolean
    :return: True, if saving succeeded. If not, return false.
    """
    try:
        couch = couchdb.Server(settings.COUCH_DB_URL)
        projectdb = couch['project_info']

        # remove space in the project_name
        project_name = project_info['project_name']
        projectdb[project_name] = {
            'name': project_name,
            'description': project_info['project_description'],
            'adapter': project_info['adapter']
        }
        doc = projectdb[project_name]
        if 'jira_id' in project_info.keys():
            # if the project already exists,
            # replace the old document which presents the project in the project_info database
            doc['jira_id'] = project_info['jira_id']
            projectdb.save(doc)
        if 'bamboo_id' in project_info.keys():
            # if the project does not exist, save the project information to the project_info database
            doc['bamboo_id'] = project_info['bamboo_id']
            projectdb.save(doc)
        if 'bitbucket_id' in project_info.keys():
            doc['bitbucket_id'] = project_info['bitbucket_id']
            projectdb.save(doc)
        if 'fogbugz_id' in project_info.keys():
            doc['fogbugz_id'] = project_info['fogbugz_id']
            projectdb.save(doc)
        # save the project profile photo to project_info database
        projectdb.put_attachment(projectdb[project_name], project_info['file_data'], filename='logo.png',
                                 content_type=project_info['content_type'])
        return True
    except couchdb.ResourceConflict:
        return False
    except Exception as e:
        logger.error("main/models.py save_project_info - %s" % e)
        raise Exception('Unable to save to database')
