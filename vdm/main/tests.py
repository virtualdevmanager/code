from django.test import TestCase, Client, override_settings
from django.conf.urls import include, url
import views as main
import datetime
import unittest
from issueTracker import views as issueTracker
from django.conf import settings
from django.http import Http404
from django.core.files.uploadedfile import SimpleUploadedFile
import couchdb
import os




class TestMainView(unittest.TestCase):
    def test_date_handle(self):
        main.date_handler(datetime.datetime.now().isoformat())
        self.assertEqual(True, True)

    @override_settings(COUCH_DB_URL='/invalid/url')
    def test_get_project(self):
        result = main.get_project()
        self.assertEqual(result, [])

    def test_get_project_2(self):
        result = main.get_project()
        self.assertNotEqual(result, [])

    def test_project_overview(self):
        client = Client()
        response = client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'project_overview.html')

    def test_organization(self):
        client = Client()
        response = client.get('/organization/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'organization.html')

    def test_project_doc(self):
        result = main.get_project_doc('VDM')
        self.assertNotEqual(result, {})

    def test_dashboard(self):
        client = Client()
        response = client.get('/dashboard/VDM/')
        self.assertEqual(response.status_code, 200)

    def test_dashboard_exception(self):
        client = Client()
        response = client.get('/dashboard/')
        self.assertEqual(response.status_code, 404)

    def test_page_not_found(self):
        client = Client()
        response = client.get('/dashboard/notfound/')
        self.assertEqual(response.status_code, 200)

    def test_project_configuration(self):
        client = Client()
        response = client.get('/project_configuration/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'add_project.html')

    def test_project_configuration(self):
        client = Client()
        response = client.get('/project_configuration/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'add_project.html')

    def test_add_project(self):
        client = Client()
        response = client.get('/add_project/')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], 'http://testserver/project_configuration/')

    def test_add_project_post_missing_info(self):
        client = Client()
        response = client.post('/add_project/', {'project_name': 'test', 'project_description': '', 'bamboo-id': 'devman'})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], 'http://testserver/project_configuration/')

    def test_add_project_post(self):
        client = Client()
        project_logo = SimpleUploadedFile("../static/main/images/add_project.png", "logo", content_type="image/png")
        response = client.post('/add_project/', {'project-name': 'test', 'project-description': '', 'project-logo': project_logo, 'jira-id': 'VDM', 'bamboo-id': 'devman'})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], 'http://testserver/')

    def test_add_project_post_without_tools_config(self):
        client = Client()
        project_logo = SimpleUploadedFile("../static/main/images/add_project.png", "logo", content_type="image/png")
        response = client.post('/add_project/', {'project-name': 'test2', 'project-description': '', 'project-logo': project_logo})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], 'http://testserver/')

    def test_dashboard_without_configs(self):
        client = Client()
        response = client.get('/dashboard/vdm%20jira/')
        self.assertEqual(response.status_code, 200)


    def test_add_project_post_already_exists(self):
        client = Client()
        project_logo = SimpleUploadedFile("../static/main/images/add_project.png", "logo", content_type="image/png")
        response = client.post('/add_project/', {'project-name': 'test', 'project-description': '', 'project-logo': project_logo})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], 'http://testserver/project_configuration/')

    @override_settings(COUCH_DB_URL='/invalid/url')
    def test_add_project_post_database_unavailable(self):
        client = Client()
        project_logo = SimpleUploadedFile("../static/main/images/add_project.png", "logo", content_type="image/png")
        response = client.post('/add_project/', {'project-name': 'test', 'project-description': '', 'project-logo': project_logo,'bamboo-id': 'devman'})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], 'http://testserver/project_configuration/')

    def test_delete_project(self):
        client = Client()
        response = client.put('/delete_project/test/')
        self.assertEqual(response['Location'], 'http://testserver/')

    def test_delete_project_test2(self):
        client = Client()
        response = client.put('/delete_project/test2/')
        self.assertEqual(response['Location'], 'http://testserver/')

    @override_settings(COUCH_DB_URL='/invalid/url')
    def test_delete_project_2(self):
        client = Client()
        response = client.put('/delete_project/test/')
        self.assertEqual(response.status_code, 404)





