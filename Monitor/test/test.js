
var express = require('express');
var bodyParser = require('body-parser');
var http = require('http');
var app = express();
var nano = require('nano')('http://localhost:5984');
var schedulardb = nano.db.use('schedular');
var connectordb =nano.db.use('connectors');
var sys = require('sys');
var exec = require('child_process').exec;
var child;

schedulardb.update = function(obj, key, callback) {
    var db = this;

    db.get(key, function (error, existing) {
        if(!error) {obj._rev = existing._rev;}
        db.insert(obj, key, callback);
    });
};

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

// key is connection name defined from configuration page, value is interval id
var connectionDict = {};

// key is port, value is an array, the first element is the connector name from the connector, the second element is the last time
//receive heartbeat
var health = {};

// key is port number, value is array of connection name
// this dictionary is used for removing connection for died connectors
var portConnectionDict = {};

function startFromDB(){
    schedulardb.view("schedular","by_pname",function(err,body){
        if(!err){
            body.rows.forEach(function(doc){

              
                var pname = doc.value.pname;
                var server = doc.value.host;
                var timeInterval = doc.value.timeInterval;
                var interval = immediateInterval(server, pname,timeInterval);
                connectionDict[pname]=interval;

            });

        }
    });
}

//initilize server
//1. set up health check loop
//2. read existing connection from database
function initServer(){
    connectordb.list({include_docs:true}, function(error,body) {
        body.rows.forEach(function(existing) {
        restartConnector(existing.key,existing.doc.port);
        });    
    });
    setInterval(healthCheck,30*1000); 
    startFromDB();
}

//start fetching process
//1. it will first to send authtification requests to associated connector
//2. it will then set interval to constantly fetch data
//output:
//1.please check your server. cause:if user provides wrong server name (return body is plain html rather than json)
//2.require username,password, server and name. cause: username,password, server, or name field is empty
//3. failed to record schedule into db. cause: cause by db issue
//4. scheduled. successfully scheduled
//5. unable to connect to connector. cause: port for connector is wrong
//6. already exists. cause: this project name is alreay used


function startProcess(req, res){
    var username =req.body.username;
    var password = req.body.password;
    var pname = req.body.pname;
    var server =req.body.host;
    var time =req.body.timeInterval;
    var port = req.body.port;

    if(typeof username === typeof undefined || typeof password === typeof undefined|| typeof pname === typeof undefined||typeof server ===typeof undefined || typeof port == typeof undefined){
        res.send("require username,password, server and name");

    }else{
        //connection does not exist in memory
        if(connectionDict[pname]=== typeof undefined){

            schedulardb.view('schedular', 'by_pname', {'key': pname, 'include_docs': true}, function(select_err, select_body){
                //we first check the we did not store the same project name twice (unique project name)
                if(!select_err&&select_body.rows.length!==0){
                    res.send("already exists");
                }else{
                    //default time is 3 minutes
                    var timeInterval = 3*60*1000;
                    //console.log(parseInt(time));
                    //check the time delay is between 1 minute and 30 minutes

                    if(typeof time !== 'undefined' && time.length !==0 && parseInt(time) >= 60*1000 && parseInt(time)<30*60*1000){
                        timeInterval = time;
                    }

                    var path="/auth?";
                    var input = [];
                    for(var v in req.body){
                        if(v!=='port' || v!=='timeInterval'){
                            input.push(v + "=" + req.body[v]);
                        }
                    }
                    path += input.join('&');
                    var options = {
                        //it may need to change later on allowing connector to run on different machines
                        host: "localhost",
                        port: port,
                        path: path,
                        method : 'GET'
                    };

                    var callback = function(response) {
                        var str = '';
                        response.on('data', function (chunk) {
                            str += chunk;
                        });

                        response.on('end', function () {
                            try{
                                var jsString = JSON.parse(str);

                                if(jsString['message']['status']==='success'){

                                    //store it into database
                                    var data=req.body;
                                    data["lastUpdate"] = "";
                                    data["timeInterval"] = timeInterval;

                                    schedulardb.insert(data,function(err){
                                        if(!err){
                                            //kick off fetching interval
                                            var interval = immediateInterval(server, pname,timeInterval);
                                            connectionDict[pname]=interval;
                                            res.send("scheduled");

                                        }else{
                                            res.send("fail");
                                        }

                                    });
                                }else{
                                    res.send("fail to autheticate");
                                }
                            }catch(e){
                                res.send("please check your server");
                            }
                            //if valid add to connectionDict and schedule
                        });
                    }
                    http.request(options, callback).on('error',function(error){res.end("unable to connect to connector" + error);
                    }).end();
                }

            })

        }else{
            res.send("alerady exists");
        }
    }
}

//run action first and kick off the interval
function immediateInterval(servers,pname, timeInterval){
    startInterval(servers,pname);
    return setInterval(startInterval, timeInterval, servers, pname);
}

//kick off interval, read information from db based on project name
//it will know the last update time for this project
function startInterval(servers,pname) {

    schedulardb.view('schedular', 'by_pname', {'key': pname, 'include_docs': true}, function (select_err, select_body) {

        if (!select_err && select_body.rows.length !== 0) {
            var doc_id = select_body.rows[0].id;
            var doc = select_body.rows[0].doc;
            var lastUpdate = doc.lastUpdate;
            pname = doc.pname;
            var username = doc.username;
            var password = doc.password;
            var server = doc.host;
            var path = '/updateData?pname=' + pname + '&lastUpdate=' + lastUpdate + '&password=' + password + '&username=' + username + '&host=' + server;
            var optionRequests = {
                host: "localhost",
                port: doc.port,
                path: encodeURI(path),
                method: 'GET' // do GET
            };
            var parseCallBack = function (response) {
                var out = '';

                response.on('data', function (chunk) {
                    out += chunk;
                });

                response.on('end', function () {
                    var jsString = JSON.parse(out);
                    if (jsString['message']['status'] === "success" && jsString['message'].hasOwnProperty('time')) {
                            doc.lastUpdate = jsString['message']['time']
                            schedulardb.update(doc, doc_id, function () {
                            });
                        }
                });
            }
            http.request(optionRequests, parseCallBack).on('error', function () {
            }).end();
            ;
        }
    });
}

//cancel scheduled task.
//input:  isRestart is a boolean variable to indicate whether we need to reschedule the task
//1. it will remove it from memory
//2. it will delete from db

function cancel(req,res,isRestart){
    var name = req.body.pname;
    var input=[];
    for(var v in req.body){
        input.push(v + "=" + req.body[v]);
    }
    //the project does not exist
    if(connectionDict[name]=== typeof undefined){
        res.send("can not find this project");
    }else{
        clearInterval(connectionDict[name]);
        var path="/cancel?";
      
        path += input.join('&');

        var options = {
            host: "localhost",
            port: req.body.port,
            path: path,
            method : 'GET' // do GET
        };

        var callback = function(response) {
            var str = '';

            //another chunk of data has been recieved, so append it to `str`
            response.on('data', function (chunk) {
                str += chunk;
            });

            response.on('end', function () {
                var jsString = JSON.parse(str);
                delete connectionDict[name];
                if(jsString['message']['status']==='success'){
                    schedulardb.view('schedular', 'by_pname', {'key': name, 'include_docs': true}, function(select_err, select_body){
                        if(!select_err && select_body.rows.length!==0){
                            var doc_id = select_body.rows[0].id;
                            var revision_id = select_body.rows[0].doc._rev;
                            schedulardb.destroy(doc_id, revision_id,function(err){
                                if(!err){
                                    if(isRestart){
                                        startProcess(req,res);
                                    }else{
                                        res.send("cancelled succssfully")
                                    }
                                }

                            });
                        }
                    })
                }else{

                    //to do when this is possible
                    res.send("fail to autheticate");
                }
                //if valid add to connectionDict and schedule
            });
        }

        var request = http.request(options, callback);
        request.on('error',function(error){res.end("fail to connect" + error);
        }).end();
    }
}

// it will check health dicionary to make every connector send heartbeat in last two minutes
function healthCheck(){
    var current = new Date();
    for(var key in health){
        var status ="alive";
        if(current-health[key][1] >= 2*60*1000){
            status = "die";
            restartConnector(health[key][0],key);
        }

        var path = "/healthcheck?port=" +key + "&connector=" + health[key][0] + "&status=" + status ;
        var options = {
            host: "localhost",
            port: 3000,
            path: path,
            method : 'GET' // do GET
        };
        var callback = function() {
            var str="";
            res.on('data', function (body) {
            str += body;
        });

        }
        http.request(options, callback).end();
    }
}


initServer();

app.set('port', process.env.PORT || 5689);
app.post('/init', function(req, res) {
    startProcess(req,res);
});

//cancel scheduled job, pname is required
app.post('/cancel', function(req, res) {
    if(req.body.pname===undefined||req.body.pname===""){
        res.send("project name required");
    }else{
        cancel(req,res,false);
    }
});


app.post('/healthcheck',function(req,res){
    var connectorName = req.body.connector;
    var port = req.body.port;
    var date = new Date();
    health[port]= [connectorName,date]
    res.send("");


});


//reschedule the job (modify credientials or intervals)
app.post('/reschedule',function(req,res){
    if(req.body.pname===undefined||req.body.pname===""){
        res.send("project name required");
    }else{
        var name = req.body.pname;
        if(connectionDict[name]===undefined){
            res.send("can not find this project");
        }else{
            cancel(req,res,true);
        }

    }

});

// custom 404 page
app.use(function(req, res) {
    res.type('text/plain');
    res.status(404);
    res.send('404 - Not Found');
});
// custom 500 page
app.use(function(err, req, res) {
    console.error(err.stack);
    res.type('text/plain');
    res.status(500);
    res.send('500 - Server Error');
});


app.listen(app.get('port'), function() {
});

//to do: restart connector
//
function restartConnector(connectorName,port){ 
                
                child = exec("service "  + connectorName+" restart" , function (error) {

                if (error === null){
                     var path = "/healthcheck?connector=" + connectorName+ "&status=alive"  + "&port="+port;
                    var options = {
                        host: "localhost",
                        port: 3000,
                        path: path,
                        method: 'GET' // do GET
                    };
                    var callback = function() {
                   
                    }  
                 
                    http.request(options, callback).end();
                }
        
    });
}


exports.app = app;

app

var test = require('unit.js')

describe('start from db', function() {
it('respond with json', function() { 
  test.string("sucess")
    
})
})

describe('POST /init without data', function() {
it('respond with json', function() {
  test.httpAgent(app)
    .post('/init')
    .send({
      username: "xbao@andrew.cmu.edu"
    })
    .expect("not data ")
    .expect(200)
    .end(function(err) {
      if (err) {
        test.fail(err.message);
      }
    });

})
})


describe('POST /init wrong data', function() {
    it('respond with json', function() {
      test.httpAgent(app)
        .post('/init')
        .send({username: "xbao@andrew.cmu.edu",password:"060218",pname:"test",port:5656,host:"https: //devman.atlassian.net"})
        .expect("fail to autheticate")
        .expect(200)
        .end(function(err) {
            if (err) {
               test.fail(err.message);
            }
        });

    })
})

describe('POST /init with right data no server connection', function() {
       it('respond with json', function() {
            test.httpAgent(app)
            .post('/init')
            .send({
                username: "xbao@andrew.cmu.edu",password:"060219",pname:"test",port:5656,host:"https: //devman.atlassian.net"})
            .expect("scheduled")
                  .expect(200)
                  .end(function(err) {
                    if (err) {
                      test.fail(err.message);
                    }
                  });

              })
          })

describe('POST /init with right data with server connection', function() {
    it('respond with json', function () {


        child = exec("python ../Jira/server.py", function () {
        });
        test.httpAgent(app)
            .post('/init')
            .send({
                username: "xbao@andrew.cmu.edu",
                password: "060219",
                pname: "test",
                port: 5656,
                host: "https: //devman.atlassian.net"
            })
            .expect("fail to autheticate")
            .expect(200)
            .end(function (err) {
                if (err) {
                    test.fail(err.message);
                }
            });

    })
})


describe('POST /init with duplicate name', function() {
      it('respond with json', function() {
         test.httpAgent(app)
        .post('/init')
        .send({username: "xbao@andrew.cmu.edu",password:"060219",pname:"test",port:5656,host:"https: //devman.atlassian.net"})
        .expect("fail to autheticate")
        .expect(200)
        .end(function(err) {
            if (err) {
               test.fail(err.message);
            }
        });

          })
      })



describe('POST /cancel', function() {
    it('respond with json', function() {
     test.httpAgent(app)
        .post('/init')
        .send({username: "xbao@andrew.cmu.edu",password:"060219",pname:"test",port:5656,host:"https: //devman.atlassian.net"})
        .expect("fail to autheticate")
        .expect(200)
        .end(function(err) {
            if (err) {
               test.fail(err.message);
            }
        });

        })
    })

describe('POST /reschdule without timeinterval' , function() {
      it('respond with json', function() {
         test.httpAgent(app)
        .post('/init')
        .send({username: "xbao@andrew.cmu.edu",password:"060219",pname:"test",port:5656,host:"https: //devman.atlassian.net"})
        .expect("fail to autheticate")
        .expect(200)
        .end(function(err) {
            if (err) {
               test.fail(err.message);
            }
        });

          })
      })
describe('POST /reschdule without lastupdate', function() {
      it('respond with json', function() {
         test.httpAgent(app)
        .post('/init')
        .send({username: "xbao@andrew.cmu.edu",password:"06029",pname:"test",port:5656,host:"https: //devman.atlassian.net",lastUpdate:""})
        .expect("fail to autheticate")
        .expect(200)
        .end(function(err) {
            if (err) {
               test.fail(err.message);
            }
        });

          })
      })


describe('POST /reschdule', function() {
      it('respond with json', function() {
         test.httpAgent(app)
        .post('/init')
        .send({username: "xbao@andrew.cmu.edu",password:"060219",pname:"test",port:5656,host:"https: //devman.atlassian.net",timeInterval:50000,lastUpdate:"2016-04-42 23:34"})
        .expect("success")
        .expect(200)
        .end(function(err) {
            if (err) {
               test.fail(err.message);
            }
        });

          })
      })


describe('POST /reschdule dup', function() {
      it('respond with json', function() {
         test.httpAgent(app)
        .post('/init')
        .send({username: "xbao@andrew.cmu.edu",password:"060219",pname:"test",port:5656,host:"https: //devman.atlassian.net",timeInterval:50000,lastUpdate:"2016-04-42 23:34"})
        .expect("already exists")
        .expect(200)
        .end(function(err) {
            if (err) {
               test.fail(err.message);
            }
        });

          })
      })


describe('POST /reschdule invalid dup', function() {
      it('respond with json', function() {
         test.httpAgent(app)
        .post('/init')
        .send({username: "xbao@andrew.cmu.edu",password:"060218",pname:"test",port:5656,host:"https: //devman.atlassian.net",timeInterval:50000,lastUpdate:"2016-04-42 23:34"})
        .expect("already exists")
        .expect(200)
        .end(function(err) {
            if (err) {
               test.fail(err.message);
            }
        });

          })
      })


describe('POST /healthcheck', function() {
      it('respond with json', function() {
         test.httpAgent(app)
        .post('/healthcheck')
        .send({port: "5689",name:"jira",status:"alive"})
        .expect(200)
        .end(function(err) {
            if (err) {
               test.fail(err.message);
            }
        });

          })
      })



