#!/bin/bash

curl -X PUT http://127.0.0.1:5984/schedular
curl -X PUT http://127.0.0.1:5984/connectors
curl -X PUT http://127.0.0.1:5984/issues
curl -X PUT http://127.0.0.1:5984/project_info
curl -X PUT http://127.0.0.1:5984/sprints
curl -X PUT http://127.0.0.1:5984/builds
curl -X PUT http://127.0.0.1:5984/build_data
curl -X PUT http://127.0.0.1:5984/projects_data
curl -X PUT http://127.0.0.1:5984/commits
curl -X PUT http://127.0.0.1:5984/schedular/_design/schedular --data-binary @mydesign.json
curl -d @connectors.json -H "Content-Type:application/json" -X POST http://localhost:5984/connectors/_bulk_docs
