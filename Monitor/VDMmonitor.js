var bunyan = require('bunyan');
var HashMap = require('hashmap');
var querystring = require('querystring');
var log = bunyan.createLogger({
    name: "monitor", 
    streams: [
    {
        level: 'debug',
        stream: process.stdout       // log INFO and above to stdout
    },
    {
        level: 'info',
        path: '/var/tmp/monitor-logs.json'  // log ERROR and above to a file
    }
  ]});

var express = require('express');
var bodyParser = require('body-parser');
var http = require('http');
var app = express();
var nano = require('nano')('http://localhost:5984');
var schedulardb = nano.db.use('schedular');
var connectordb =nano.db.use('connectors');
var sys = require('sys');
var exec = require('child_process').exec;
var child;
var connectorFirstTime = new HashMap();

var crypto = require('crypto');
var algorithm = 'aes-256-ctr';
var key = 'ly862xp555hw';


// Encryption and decryption functions
function encrypt(unencrypted_text){
  var cipher = crypto.createCipher(algorithm, key)
  var encrypted_text = cipher.update(unencrypted_text,'utf8','hex')
  encrypted_text += cipher.final('hex');
  return encrypted_text;
}

function decrypt(encrypted_text){
  var decipher = crypto.createDecipher(algorithm, key)
  var deciphered_text = decipher.update(encrypted_text,'hex','utf8')
  deciphered_text += decipher.final('utf8');
  return deciphered_text;
}

schedulardb.update = function(obj, key, callback) {
    log.info("Inside schedulardb.update function");
    var db = this;

    db.get(key, function (error, existing) {
        if (!error) {
            obj._rev = existing._rev;

            log.info("object is: " + obj + " key is: " + key);
            db.insert(obj, key, callback);
        } else {
            log.info("schedularedb.update.def error:" + error);
        }
    });
};

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

// key is connection name defined from configuration page, value is interval id
var connectionDict = {};

// key is port, value is an array, the first element is the connector name from the connector, the second element is the last time
//receive heartbeat
var health = {};

// key is port number, value is array of connection name
// this dictionary is used for removing connection for died connectors
var portConnectionDict = {};


/*
 * This function will load existing project set by user from database
 * pname is the project name which set up on the main page
 */
function startFromDB(){
    log.info("Start from DB");
    schedulardb.view("schedular","by_pname", function(err,body){
        if(!err){
            log.info("No error start from DB");
            body.rows.forEach(function(doc){
                var pname = doc.value.pname;
                var server = doc.value.host;
                var timeInterval = doc.value.timeInterval;
                var interval = immediateInterval(server, pname,timeInterval);

                //We load all the existing project in the connection diction list
                //We also set the interval for each project
                //The interval is the refresh interval for retrieving data from external website
                connectionDict[pname]=interval;
                log.info("Kicking off the job from startFromDB()");
            });
        }else{
            log.info("ERROR: Could not connect to db from startFromDB()");
        }
    });
}

//Initialize server
//1. set up health check loop
//2. read existing connection from database
/*
 * This function initialize the connector and load existing connected project from database to connection dictionary
 */
function initServer(){
    log.info("Initializing server");
    connectordb.list({include_docs:true}, function(error,body,headers) {
        if(error) {
            log.info("initServer error:" + error);
        }
        log.info("Iterating through connectors");
        body.rows.forEach(function(servicePort) {
            log.info(servicePort.key + "is on port: " + servicePort.doc.port);

            //we start each service port so that user can select different service on
            // (jira, bamboo, etc.) configuration page
            connectorFirstTime.set(servicePort.key, 1);
            restartConnector(servicePort.key, servicePort.doc.port);
        });
    });
    log.info("Done initializing server");

    //Set up health check loop
    setInterval(healthCheck,30*1000);
    startFromDB();
}

//start fetching process
//1. it will first to send authentication requests to associated connector
//2. it will then set interval to constantly fetch data
//output:
//1.please check your server. cause:if user provides wrong server name (return body is plain html rather than json)
//2.require username,password, server and name. cause: username,password, server, or name field is empty
//3. failed to record schedule into db. cause: cause by db issue
//4. scheduled. successfully scheduled
//5. unable to connect to connector. cause: port for connector is wrong
//6. already exists. cause: this project name is already used
function startProcess(req, res) {
    //User name of the configured adapter
    var username = req.body.username;

    //User password of the configured adapter
    var password = req.body.password;

    //Project name
    var pname = req.body.pname;

    //Server url of the configured adapter
    var server = req.body.host;

    //The refresh interval of the configured adapter
    var time = req.body.timeInterval;

    //The port (jira, bamboo, etc.) of the configured adapter
    var port = req.body.port;

    log.info("Start process");
    if (typeof username === 'undefined' || typeof password === 'undefined' ||
        typeof pname === 'undefined' || typeof server === 'undefined' || typeof port === 'undefined') {
        res.send("require username,password, server and name");
        log.info("require username,password, server and name");

    } else {
        //if there is no connection in the connection list
        // (This means when startFromDB() function did not load connected project into the dictionary)
        if (connectionDict[pname] === undefined) {
            schedulardb.view('schedular', 'by_pname', {
                'key': pname,
                'include_docs': true
            }, function (select_err, select_body) {
                //we first check the we did not store the same project name twice (unique project name)
                if (!select_err && select_body.rows.length !== 0) {
                    res.send("already exists");
                    log.info("Project " + pname + " already exists");
                } else {
                    //default time is 3 minutes
                    var timeInterval = 3 * 60 * 1000;
                    log.info(parseInt(time));
                    //check the time delay is between 1 minute and 30 minutes

                    if (typeof time !== 'undefined'
                        && time.length !== 0
                        && parseInt(time) >= 60 * 1000
                        && parseInt(time) < 30 * 60 * 1000) {
                        timeInterval = time;
                    }

                    //This section is building auth URL to make a call to python file
                    var post_data = querystring.stringify({
                        'pname': pname,
                        'username': username,
                        'password': password,
                        'host': server
                    });

                    var post_options = {
                        host: 'localhost',
                        port: port,
                        path: '/auth',
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                            'Content-Length': Buffer.byteLength(post_data)
                        }
                    };

                    var callback = function (response) {
                        var dataReceived = '';

                        //If there are data available, we aggregate them
                        response.on('data', function (chunk) {
                            dataReceived += chunk;
                        });

                        response.on('end', function () {
                            try {
                                var jsString = JSON.parse(dataReceived);

                                //if we have received a success response from adapter script
                                //We store the response data into database
                                if (jsString['message']['status'] === 'success') {

                                    //store it into database
                                    //The data object contains username, password, project name
                                    //Host URL, refresh time interval, port number
                                    //update time stamp
                                    var data = req.body;
                                    data["lastUpdate"] = "";
                                    data["timeInterval"] = timeInterval;
                                    var plaintext_pw = data["password"];
                                    var encrypted_pw = encrypt(plaintext_pw);
                                    data["password"] = encrypted_pw;
                                    schedulardb.insert(data, function (err, body) {
                                        if (!err) {
                                            //kick off fetching interval
                                            //The backend service will pull data from external server based on the time
                                            //interval set by the user from configuration page
                                            var interval = immediateInterval(server, pname, timeInterval);
                                            connectionDict[pname] = interval;
                                            res.send("scheduled");

                                        } else {
                                            log.info(err + "failed to record schedule into db");
                                            res.send("fail");
                                        }

                                    });
                                } else {
                                    log.info("Fail to authenticate")
                                    res.send("fail to autheticate");
                                }
                            } catch (e) {
                                res.send("please check your server");
                            }
                            //if valid add to connectionDict and schedule
                        });
                    };



                    //Making authentication request to adapter script
                    var post_req = http.request(post_options, callback).on('error', function (error) {
                        res.end("unable to connect to connector");
                        log.info("startProcess-unable to connect error:" + error);
                    });
                    post_req.write(post_data);
                    post_req.end();
                }

            })

        } else {
            res.send("alerady exists");
            log.info("Already exists");
        }
    }
}

//run action first and kick off the interval
/*
 * This function will set the back end refresh interval for individual adapter
 */
function immediateInterval(servers,pname, timeInterval){
    startInterval(servers,pname);
    log.info("Kicking off immediate interval");
    return setInterval(startInterval, timeInterval, servers, pname);
}

//kick off interval, read information from db based on project name
//it will know the last update time for this project
/*
 * This function read all available adapter from database and kick off the data pull activity
 * The updateData call will pull all project associate information from external server
 */
function startInterval(servers,pname,body) {
    log.info("Starting interval");
    schedulardb.view('schedular', 'by_pname', {'key': pname, 'include_docs': true}, function (select_err, select_body) {

        if (!select_err && select_body.rows.length !== 0) {
            var doc_id = select_body.rows[0].id;

            //The doc contains username, password, project name
            // Host URL, refresh time interval, port number
            //update time stamp
            var doc = select_body.rows[0].doc;
            log.info("We are logging doc info");
            log.info(doc);
            var lastUpdate = doc.lastUpdate;
            pname = doc.pname;
            var username = doc.username;
            var password = decrypt(doc.password);
            var server = doc.host;

            var post_data = querystring.stringify({
                'pname': pname,
                'lastUpdate': lastUpdate,
                'username': username,
                'password': password,
                'host': server
            });



            var post_options = {
                host: 'localhost',
                port: doc.port,
                path: '/updateData',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Content-Length': Buffer.byteLength(post_data)
                }
            };

            var parseCallBack = function (response) {
                var out = '';

                response.on('data', function (chunk) {
                    out += chunk;
                });

                response.on('end', function () {
                    log.info(out);
                    var jsString = JSON.parse(out);

                    //Once we receive success response from adapter script side, we update database with update time
                    if (jsString['message']['status'] === "success" && jsString['message'].hasOwnProperty('time')) {
                        log.info("update " + jsString['message']['time']);
                        doc.lastUpdate = jsString['message']['time']
                        log.info("doc.lastUpdate:" + doc.lastUpdate);

                        //Update the doc info
                        schedulardb.update(doc, doc_id, function (err, res) {

                            if (!err) {
                                log.info("succeed");
                            } else {
                                log.info(err);
                            }
                        });
                    }

                });
            }
            var post_req = http.request(post_options, parseCallBack).on('error', function (error) {
                log.info({pname: "start interval fail to connect:" + error});
            });
            post_req.write(post_data);
            post_req.end();
        } else {
            log.info(err);
        }
    });
}

//cancel scheduled task.
//input:  isRestart is a boolean variable to indicate whether we need to reschedule the task
//1. it will remove it from memory
//2. it will delete from db

/*
 * This function will remove the selected adapter from the database and from the memory
 * @param req: Incoming request
 * @param res: Response of the function
 * @isRestart: isRestart is a boolean variable to indicate whether we need to reschedule the task
 */
function cancel(req,res,isRestart) {
    log.info("Deleting scheduled task.");
    var name = req.body.pname;
    log.info("Deleting connector:" + name);

    //the project does not exist
    if (connectionDict[name] === undefined) {
        res.send("Could not find this project");
    } else {
        clearInterval(connectionDict[name]);


        var post_data = querystring.stringify({
            'pname': name
        });

        var post_options = {
            host: 'localhost',
            port: req.body.port,
            path: '/cancel',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': Buffer.byteLength(post_data)
            }
        };

        //Delete it from the memory
        delete connectionDict[name];


        schedulardb.view('schedular', 'by_pname', {
            'key': name,
            'include_docs': true
        }, function (select_err, select_body) {
            if (!select_err && select_body.rows.length !== 0) {
                var doc_id = select_body.rows[0].id;
                var revision_id = select_body.rows[0].doc._rev;

                //Deleting the adapter from database
                schedulardb.destroy(doc_id, revision_id, function (err, body) {
                    if (!err) {

                        //if we are rescheduling this process, we will start the process again
                        if (isRestart) {
                            //Start the new process
                            startProcess(req, res);
                        } else {
                            res.send("cancelled successfully")
                        }
                    } else {
                        log.info(err);
                    }

                });
            } else {
                log.info("no data");
            }
        })


        var callback = function (response) {

            var str = '';

            //another chunk of data has been recieved, so append it to `str`
            response.on('data', function (chunk) {
                str += chunk;
            });

            response.on('end', function () {
                var jsString = JSON.parse(str);
                if (jsString['message']['status'] === 'success') {
                    log.info("Success to authenticate");
                } else {

                    //to do when this is possible
                    res.send("fail to autheticate");
                    log.info("Failed to authenticate");
                }
                //if valid add to connectionDict and schedule
            });
        }

        var post_req = http.request(post_options, callback).on('error', function (error) {
            res.end("fail to connect");
            log.info("cancel error:" + error);
        });
        post_req.write(post_data);
        post_req.end();
    }
}

//

/*
 * This function  will check health dicionary to make every connector send heartbeat in last two minutes
 */
function healthCheck() {
    log.info("Doing health check");
    var current = new Date();
    
    //We iterate through all currently connected port to check health
    for (var key in health) {
        var status = "alive";

        //If the last connection is more then 2 min, we should restart the adapter
        if (current - health[key][1] >= 2 * 60 * 1000) {
            status = "die";
            log.info(key + "adaptor restarting inside healthCheck()");
            restartConnector(health[key][0], key);
        }

        //Initialize the call URL
        var path = "/healthcheck?port=" + key + "&connector=" + health[key][0] + "&status=" + status;
        log.info("GET path: /healthcheck?port=" + key + "&connector=" + health[key][0] + "&status=" + status);
        var options = {
            host: "localhost",
            port: 3000,
            path: path,
            method: 'GET' // do GET
        };
        var callback = function (response) {
            var str = ""
            response.on('data', function (body) {
                str += body;
            });

        }
        http.request(options, callback).end();
    }
}

log.info("======================== VDMMONITOR STARTED ========================");

//Initialize the monitor
initServer();

app.set('port', process.env.PORT || 5689);
app.post('/init', function(req, res) {
    startProcess(req,res);
    log.info("Starting process");
});

//removing scheduled adapter, project name is required
app.post('/cancel', function(req, res) {
    if(req.body.pname===undefined||req.body.pname===""){
        res.send("project name required");
        log.info("Project name required");
    }else{

        cancel(req,res,false);
        log.info("Canceling job for pname:" + req.body.pname);
    }
});


app.post('/healthcheck',function(req,res){
    var connectorName = req.body.connector;
    var port = req.body.port;
    var date = new Date();
    health[port]= [connectorName,date]
    log.info("Received heartbeat from " + health[port] );
    res.send("");
});


//reschedule the job (modify credientials or intervals)
app.post('/reschedule',function(req,res){
    if(req.body.pname===undefined||req.body.pname===""){
        res.send("project name required");
        log.info("Project name required to reschedule");
    }else{
        var name = req.body.pname;
        log.info("reshedule pname",name);
        if(connectionDict[name]===undefined){
            res.send("can not find this project");
            log.info("Cannot find this project");
        }else{
            log.info("Inside reschedule before making cancel request:" + req.body.password);
            req.body.password = decrypt(req.body.password)
            cancel(req,res,true);
            log.info("canceled");
        }

    }

});

// Checks if the project exists
app.post('/doesProjectExist',function(req,res) {
    log.info("VDMMonitor - received request for doesProjectExist: " + req.body);
    var adapter_name = req.body.adapter_name;   // pname parameter
    var projKey = req.body.project_key;       // ie jira_id, bitbucket_id
//    var project_info_param = req.body.project_info;
    schedulardb.view("schedular","by_pname", function(err,body) {
        if(!err) {
            body.rows.forEach(function(doc) {
                log.info('VDMMonitor iterating through every project doc');
                log.info('VDMMonitor - doesProjectExist: got pname from db: ' + doc.value.pname);
                log.info('VDMMonitor - doesProjectExist: got pname from req: ' + adapter_name);
                if (doc.value.pname && adapter_name === doc.value.pname) {
                    var port_number = doc.value.port;

                    var postData = querystring.stringify({
                        'pname': adapter_name,
                        'username': doc.value.username,
                        'password': decrypt(doc.value.password),
                        'host': doc.value.host,
                        'last_update': doc.value.last_update,
                        'key': projKey
                    });

                    var options = {
                        host: "localhost",
                        port: port_number,
                        path: "/checkIfExist",
                        method: 'POST',
                        headers: {  'Content-Type': 'application/x-www-form-urlencoded',
                                    'Content-Length': Buffer.byteLength(postData)
                        }
                    };

                    var post_req = http.request(options, function(response) {
                        log.info('Status: ' + response.statusCode);
                        response.setEncoding('utf8');
                        var str = ""
                        response.on('data', function (body) {
                            str += body;
                        });

                        response.on('end', function () {
                            var jsString = JSON.parse(str);
                            log.info('VDMMonitor - doesProjectExist: relaying following message: ' + str);
                            res.send(jsString);
                        });
                    });

                    post_req.on('error', function (error) {
                        res.end("project doesn't exist");
                        log.info("VDMMonitor - project doesn't exist:" + error);
                    });

                    post_req.write(postData);
                    post_req.end();
                }
            });
        }
        else {
            res.sendStatus(500);
            log.info("VDMMonitor - does project exist ERROR: Could not connect to db");
        }
    });
});

// Force update for the adapter information sent through the request body
app.post('/forceUpdate',function(req,res){
    log.info('VDMMonitor - force updating...')
    var adapter_name = req.body.pname;
    log.info('VDMMonitor - forceUpdate: adapter name: ' + adapter_name)
    schedulardb.view("schedular","by_pname", function(err,body) {
        if(!err) {
            body.rows.forEach(function(doc) {
                if (doc.value.pname && adapter_name === doc.value.pname) {
                    var port = doc.value.port;
                    var postData = querystring.stringify({
                        'pname': adapter_name,
                        'username': doc.value.username,
                        'password': decrypt(doc.value.password),
                        'host': doc.value.host,
                        'last_update': doc.value.last_update
                    });

                    var options = {
                        host: "localhost",
                        port: port,
                        path: "/updateData",
                        method: 'POST',
                        headers: {  'Content-Type': 'application/x-www-form-urlencoded',
                                    'Content-Length': Buffer.byteLength(postData)
                        }
                    };

                    var post_req = http.request(options, function(response) {
                        log.info('force update status: ' + response.statusCode);
                        response.setEncoding('utf8');
                        response.on('data', function (body) {
                            log.info('VDMMonitor - forceUpdate: response body: ' + body);
                            res.sendStatus(200);
                        });
                    });

                    post_req.on('error', function (error) {
                        res.end("project doesn't exist");
                        log.info("VDMMonitor - project doesn't exist:" + error);
                    });

                    post_req.write(postData);
                    post_req.end();
                }
            });
        }
        else {
            res.sendStatus(500);
            log.info("VDMMonitor - force update: Could not connect to db");
        }
    });
});


// custom 404 page
app.use(function(req, res) {
    log.info("error 404");
    res.type('text/plain');
    res.status(404);
    res.send('404 - Not Found');
});
// custom 500 page
app.use(function(err, req, res, next) {
    log.info("error 500");
    log.error(err.stack);
    res.type('text/plain');
    res.status(500);
    res.send('500 - Server Error');
});


app.listen(app.get('port'), function() {
    log.info('Express started on http://localhost:' +
        app.get('port') + '; press Ctrl-C to terminate.');
});

/*
 * This function start each inidividual adapter (jira, bamboo, etc.)
 */
function restartConnector(connectorName,port) {
    var statement;

    if(connectorFirstTime.get(connectorName) === undefined || connectorFirstTime.get(connectorName) === 1) {
        connectorFirstTime.set(connectorName, 0);
        statement = "sudo service " + connectorName + " start";
        log.info(connectorName + " starting");
    }
    else {
        statement = "sudo service " + connectorName + " restart";
        log.info(connectorName + " restarting");
    }

    child = exec(statement, function (error, stdout, stderr) {
        if (error != null) {
            log.info("exec error: " + error + stdout + stderr + port);
        }
        else {
            log.info("no execution error in restartConnector for: " + connectorName);
        }
    });
}


exports.app = app;
